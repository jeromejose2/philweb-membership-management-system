<?php

require_once("../../init.inc.php");

App::LoadModuleClass("MembershipTimor", "MTMemberInfo");
App::LoadModuleClass("MembershipTimor", "MTAuditTrail");

$mtMemberInfo = new MTMemberInfo();
$mtAuditTrail = new MTAuditTrail();

$separator = ": ";
$separator2 = " from";

$memberInfo = $mtMemberInfo->selectMemberByDateCreated();
for($memberInfoCount = 0;$memberInfoCount<count($memberInfo);$memberInfoCount++)
{
   $memberNameArray[] = $memberInfo[$memberInfoCount]["FirstName"]." ".$memberInfo[$memberInfoCount]["LastName"];
}

for($memberNameArrayCount = 0;$memberNameArrayCount<count($memberNameArray);$memberNameArrayCount++)
{
    $inAuditTrail = $mtAuditTrail->selectInactiveMemberBasedOnName($memberNameArray[$memberNameArrayCount]);
}

$inactiveMember = array_filter($mtMemberInfo->selectInactiveMembers());

foreach($inactiveMember as $singleInactiveMember)
{
    $memberLastTrans[] = $mtAuditTrail->selectInactiveMemberBasedOnLastTransaction($singleInactiveMember["FirstName"]." ".$singleInactiveMember["LastName"]);
}

function array_filter_recursive($input) 
{ 
    foreach ($input as &$value) 
    { 
      if (is_array($value)) 
      { 
        $value = array_filter_recursive($value); 
      } 
    } 
    
    return array_filter($input); 
} 
  
$memberLastTransArray = array_filter_recursive($memberLastTrans); 

foreach($memberLastTransArray as $singleMemberLastTrans)
{  
    $singleMemberLastTransArray[] = $mtAuditTrail->selectMemberInactive($singleMemberLastTrans[0]["TransDetails"]);  
}

$listOfMembersLastTransArray = array_filter_recursive($singleMemberLastTransArray); 

foreach ($listOfMembersLastTransArray as $singleListOfMembersLastTransArray)
{
    $transDateTime[] = $singleListOfMembersLastTransArray[0]["TransDateTime"];
}

foreach($transDateTime as $singleTransDateTime)
{
    $singleTransDateTimeArray[] = strtotime($singleTransDateTime);
}

foreach($singleTransDateTimeArray as $singleTransDateTimeArray2)
{
    $single2TransDateTimeArray2[] = strtotime("+60 days",$singleTransDateTimeArray2);
}

$dateNow = microtime(true);
$microTime = sprintf("%06d",($dateNow - floor($dateNow)) * 1000000);
$dateNow = new DateTime( date('Y-m-d H:i:s.'.$microTime,$dateNow) );
$dateNow = $dateNow->format("Y-m-d H:i:s.u");
$dateNow = strtotime($dateNow);

foreach($single2TransDateTimeArray2 as $transDateTime2)
{       
    if($transDateTime2 <= $dateNow)
    {
        $transDetails[] = $singleListOfMembersLastTransArray[0]["TransDetails"];
        foreach($transDetails as $singleTransDetails)
        {
            $array2[] = split($separator2, $singleTransDetails);
        }          
    }  
}

for ($o = 0; $o < count($array2); $o++)
{
    $array3[] = split(": ", $array2[$o][0]);
    $array4[] = split(" ", $array3[$o][1]);
    $memberId[]  = $mtMemberInfo->selectMemberByName($array4[$o][0],$array4[$o][1]);
}

if(count($memberInfo)!= 0)
{
    foreach($memberInfo as $singleMemberInfo)
    {
        $mtMemberInfo->StartTransaction();
        //update memberinfo table,change status to terminated
        $updateMemberStatus['MemberInfoID'] = $singleMemberInfo["MemberInfoID"];
        $updateMemberStatus['Status'] = 3;
        $mtMemberInfo->UpdateByArray($updateMemberStatus);
        if($mtMemberInfo->HasError)
        {
            $mtMemberInfo->RollBackTransaction();
            $errorMessage = "An error occured: " . $mtMemberInfo->getErrors();
        }
        else
        {
            $mtMemberInfo->CommitTransaction();
            
            //insert to audittrail update player status
            $mtAuditTrail->StartTransaction();
            $auditMemberStatus["SessionID"] = $_SESSION['sid'];
            $auditMemberStatus["AID"] = $_SESSION['aid'];
            $auditMemberStatus['AuditTrailFunctionID'] = '12';
            $auditMemberStatus["TransDetails"] = "Update Player Status:".$singleMemberInfo["FirstName"].' '.$singleMemberInfo["LastName"].' '."from Inactive to Terminated";
            $auditMemberStatus["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $auditMemberStatus["TransDateTime"] = "now_usec()";
            $mtAuditTrail->Insert($auditMemberStatus);
            if($mtAuditTrail->HasError)
            {
                $mtAuditTrail->RollBackTransaction();
                $errorMessage = "An error occured: " . $mtAuditTrail->getErrors();
            }
            else
            {
                $mtAuditTrail->CommitTransaction();
                app::pr("Inactive Account for 60 days detected. Deleted from catalog.");
            }    
        }
    }
}

if(count($memberId) != 0)
{
    foreach($memberId as $singleMemberId)
    {
        $mtMemberInfo->StartTransaction();
        //update memberinfo table,change status to terminated
        $updateMemberStatus['MemberInfoID'] = $singleMemberId[0]["MemberInfoID"];
        $updateMemberStatus['Status'] = 3;
        $mtMemberInfo->UpdateByArray($updateMemberStatus);
        if ($mtMemberInfo->HasError) {
            $mtMemberInfo->RollBackTransaction();
            $errorMessage = "An error occured: " . $mtMemberInfo->getErrors();
        } else {
            $mtMemberInfo->CommitTransaction();
            
            //insert to audittrail update player status
            $mtAuditTrail->StartTransaction();
            $auditMemberStatus["SessionID"] = $_SESSION['sid'];
            $auditMemberStatus["AID"] = $_SESSION['aid'];
            $auditMemberStatus['AuditTrailFunctionID'] = '12';
            $auditMemberStatus["TransDetails"] = "Update Player Status:".$singleMemberId[0]["FirstName"].' '.$singleMemberId[0]["LastName"].' '."from Inactive to Terminated";
            $auditMemberStatus["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $auditMemberStatus["TransDateTime"] = "now_usec()";
            $mtAuditTrail->Insert($auditMemberStatus);
            if ($mtAuditTrail->HasError) {
                $mtAuditTrail->RollBackTransaction();
                $errorMessage = "An error occured: " . $mtAuditTrail->getErrors();
            } else {
                $mtAuditTrail->CommitTransaction();
                app::pr("Inactive Account for 60 days detected. Deleted from catalog.");
            }    
        }
    }
}

?>
