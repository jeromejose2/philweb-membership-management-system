<?php

/*
 * Created by : Angelo Cubos
 * Date Created : April 01 2013
 * Purpose : controller useraccountprofileprocess
 */
require_once("../../init.inc.php");
error_reporting(E_ALL ^ E_NOTICE);

App::LoadControl("TextBox");
APP::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("Label");
App::LoadControl("ComboBox");

$moduleName = "MembershipTimor";
App::LoadModuleClass($moduleName, "MTAccounts");
App::LoadModuleClass($moduleName, "MTAccountDetails");
App::LoadModuleClass($moduleName, "MTAccountTypes");
App::LoadModuleClass($moduleName, "MTAccountStatusLogs");

$mtAccountStatusLogs = new MTAccountStatusLogs();
$mtAccounts = new MTAccounts();

$fProc = new FormsProcessor();

$ddlUserName = new ComboBox("ddlUsername", "ddlUsername");
$ddlGroupUserName = new ArrayList();
$ddlGroupUserName[] = new ListItem("", "0", true);
$getGroups = $mtAccounts->userList();
$ddlGroupUserName->AddArray($getGroups);
$ddlUserName->DataSource = $ddlGroupUserName;
$ddlUserName->DataSourceText = "UserName";
$ddlUserName->DataSourceValue = "AID";
$ddlUserName->DataBind();
$ddlUserName->Args = "onchange = 'javascript: onchange_username();'";
$ddlUserName->Style = "width: 280px";

$btnSearch = new Button("btnSearch", "btnSearch", "SEARCH");
$btnSearch->IsSubmit = true;
$btnSearch->Style = "width: 200px";
$btnSearch->CssClass = "labelbutton_black";
$btnSearch->Args = "onclick = 'javascript: return showData();'";

$btnEdit = new Button("btnEdit", "btnEdit", "EDIT");
$btnEdit->IsSubmit = true;
$btnEdit->CssClass = "labelbutton_black";
$btnEdit->Enabled = false;

$fProc->AddControl($ddlUserName);
$fProc->AddControl($btnSearch);
$fProc->AddControl($btnEdit);

$fProc->ProcessForms();

$userAccountID = "";
if (strlen($_SESSION['NewlyAddedAID']) > 0)
{
    $userAccountID = $_SESSION['NewlyAddedAID'];
    $ddlUserName->SetSelectedValue($userAccountID);
}
if (strlen($userAccountID) > 0)
{
    $arrUserInfo = $mtAccounts->userInfo($userAccountID);
    $userName = $arrUserInfo[0]['UserName'];
    $accountTypeID = $arrUserInfo[0]['AccountTypeID'];
    $dateCreated = $arrUserInfo[0]['DateCreated'];
    $status = $arrUserInfo[0]['Status'];

    $arrRemarks = $mtAccountStatusLogs->getRemarks($userAccountID);
    $txtRemarks->Text = $arrRemarks[0]['Remarks'];
}
if ($fProc->IsPostBack)
{
    if ($btnEdit->SubmittedValue == "EDIT")
    {
        if (strlen($_SESSION['NewlyAddedAID']) > 0)
        {
            $_SESSION['EditAcct'] = $_SESSION['NewlyAddedAID'];
        }
        app::pr("<script>window.location.href='manageuserupdate.php';</script>");
        $_SESSION['NewlyAddedAID'] = "";
    }
    if ($btnSearch->SubmittedValue == "SEARCH")
    {
        if ($ddlUserName->SubmittedValue == 0)
        {
            $errorTitle = "INVALID SEARCH";
            $errorMessage = "Please select username.";
        } else
        {

            $arrUserInfo = $mtAccounts->userInfo($ddlUserName->SubmittedValue);
            $userName = $arrUserInfo[0]['UserName'];
            $accountTypeID = $arrUserInfo[0]['AccountTypeID'];
            $dateCreated = $arrUserInfo[0]['DateCreated'];
            $status = $arrUserInfo[0]['Status'];

            $arrRemarks = $mtAccountStatusLogs->getRemarks($ddlUserName->SubmittedValue);
            $txtRemarks->Text = $arrRemarks[0]['Remarks'];

            $_SESSION['EditAcct'] = $ddlUserName->SubmittedValue;
            $ddlUserName->SetSelectedValue($ddlUserName->SubmittedValue);
            $_SESSION['NewlyAddedAID'] = "";
        }
    }
}

?>
