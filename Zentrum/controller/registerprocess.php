<?php

/**
 * The controller page. This module will be used for registration of e-Games members.
 * @author Noel Antonio 04-02-2013 <ndantonio@philweb.com.ph>
 * @copyright (c) 2013, PhilWeb Corporation
 */

require_once("../../init.inc.php");

// Load module classes
App::LoadModuleClass("MembershipTimor", "MTCountries");
App::LoadModuleClass("MembershipTimor", "MTAccounts");
App::LoadModuleClass("MembershipTimor", "MTMemberInfo");
App::LoadModuleClass("MembershipTimor", "MTAccountDetails");
App::LoadModuleClass("MembershipTimor", "MTRefIdentification");
App::LoadModuleClass("MembershipTimor", "MTCards");
App::LoadModuleClass("MembershipTimor", "MTMemberCards");
App::LoadModuleClass("MembershipTimor", "MTAuditTrail");

// Load controls
App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");

// Initialize module class objects
$mtCountries = new MTCountries();
$mtAccounts = new MTAccounts();
$mtMemberInfo = new MTMemberInfo();
$mtAccountDetails = new MTAccountDetails();
$mtRefIdentification = new MTRefIdentification();
$mtCards = new MTCards();
$mtMemberCards = new MTMemberCards();
$mtAuditTrail = new MTAuditTrail();

// Initialize control objects
$txtFirstName = new TextBox("txtFirstName", "txtFirstName", "First Name ");
$txtFirstName->Length = 30;
$txtFirstName->Args = "size='26%' autocomplete='off' onkeypress='javascript:"
        . "return AlphaAndSpaces(event);'";

$txtLastName = new TextBox("txtLastName", "txtLastName", "Last Name ");
$txtLastName->Length = 30;
$txtLastName->Args = "size='26%' autocomplete='off' onkeypress='javascript:" .
        "return AlphaAndSpaces(event);'";

$txtAddress = new TextBox("txtAddress", "txtAddress", "Address");
$txtAddress->Length = 300;
$txtAddress->Args = "size='87%' autocomplete='off' onkeypress='javascript:" . 
        "return AlphaNumericAndSpacesOnly(event);'";

$txtContactNo = new TextBox("txtContactNo", "txtContactNo", "Contact Number");
$txtContactNo->Length = 20;
$txtContactNo->Args = "size='87%' autocomplete='off' onkeypress='javascript:"
        . "return OnlyNumbers(event);'";

$txtEmail = new TextBox("txtEmail", "txtEmail", "E-mail Address");
$txtEmail->Args = "size='87%' autocomplete='off'";

$txtIDNo = new TextBox("txtIDNo", "txtIDNo", "ID Number");
$txtIDNo->Length = 20;
$txtIDNo->Args = "size='45%' autocomplete='off' onkeypress='javascript: return "
        . "AlphaNumericOnly(event);'";

$txtOthers = new TextBox("txtOthers", "txtOthers", "Others ");
$txtOthers->Length = 30;
$txtOthers->Args = "size='22%' autocomplete='off' onkeypress='javascript:" . 
        "return AlphaAndSpaces(event);'";

$ddlSex = new ComboBox("ddlSex", "ddlSex", "Sex");
$ddlSex->Style = "width: 175px";
$optionSex = null;
$optionSex[] = new ListItem("Please Select", "0", true);
$optionSex[] = new ListItem("Male", "1");
$optionSex[] = new ListItem("Female", "2");
$ddlSex->Items = $optionSex;

$ddlNationality = new ComboBox("ddlNationality", "ddlNationality", "Nationality");
$ddlNationality->Style = "width: 200px";
$optionCountry = null;
$optionCountry[] = new ListItem("Please Select", "0", true);
$ddlNationality->Items = $optionCountry;
$arrayCountries = $mtCountries->SelectCountriesByPriority();
$listCountries = new ArrayList();
$listCountries->AddArray($arrayCountries);
$ddlNationality->DataSource = $listCountries;
$ddlNationality->DataSourceText = "CountryName";
$ddlNationality->DataSourceValue = "CountryID";
$ddlNationality->DataBind();
$ddlNationality->SetSelectedValue("212"); // Set default to "Timor-Leste"

$ddlMaritalStat = new ComboBox("ddlMaritalStat", "ddlMaritalStat", "Marital Status");
$ddlMaritalStat->Style = "width: 200px";
$optionMaritalStatus = null;
$optionMaritalStatus[] = new ListItem("Please Select", "0", true);
$optionMaritalStatus[] = new ListItem("Single", "1");
$optionMaritalStatus[] = new ListItem("Married", "2");
$optionMaritalStatus[] = new ListItem("Widowed", "3");
$ddlMaritalStat->Items = $optionMaritalStatus;

$ddlIdPresented = new ComboBox("ddlIdPresented", "ddlIdPresented", "ID Presented");
$ddlIdPresented->Style = "width: 150px";
$optionIdPresent = null;
$optionIdPresent[] = new ListItem("Please Select", "0", true);
$ddlIdPresented->Items = $optionIdPresent;
$arrID = $mtRefIdentification->SelectAll();
$listID = new ArrayList();
$listID->AddArray($arrID);
$ddlIdPresented->DataSource = $listID;
$ddlIdPresented->DataSourceText = "IdentificationName";
$ddlIdPresented->DataSourceValue = "IdentificationID";
$ddlIdPresented->DataBind();

$ddlStatus = new ComboBox("ddlStatus", "ddlStatus", "Status");
$ddlStatus->Style = "width: 150px";
$optionStatus = null;
$optionStatus[] = new ListItem("Please Select", "", true);
$optionStatus[] = new ListItem("Active", "1");
$optionStatus[] = new ListItem("Inactive", "0");
$optionStatus[] = new ListItem("Suspended", "2");
$optionStatus[] = new ListItem("Banned", "3");
$ddlStatus->Items = $optionStatus;
$ddlStatus->SetSelectedValue(1);

$btnSubmit = new Button("btnSubmit", "btnSubmit", "SUBMIT");
$btnSubmit->CssClass = "labelbutton2";
$btnSubmit->Args = "onclick='javascript: return checkRegistrationInputs();'";

$btnOkay = new Button("btnOkay", "btnOkay", "OKAY");
$btnOkay->CssClass = "labelbutton_black";
$btnOkay->IsSubmit = true;

$btnBrowse = new Button("btnBrowse", "btnBrowse", "UPLOAD");
$btnBrowse->IsSubmit = true;

$btnBack = new Button("btnBack", "btnBack", "BACK");
$btnBack->IsSubmit = true;
$btnBack->CssClass = "labelbutton2";

$hidImgPath = new Hidden("hidImgPath", "hidImgPath", "Image Path");
$hidID = new Hidden("hidID", "hidID", "Membership ID");
$hidBday = new Hidden("hidBday", "hidBday", "Birthday");
$hidOrigin = new Hidden("hidOrigin", "hidOrigin");
$hidPage = new Hidden("hidPage", "hidPage");

// var $memberID came from the file - 'membershipcard.php'
$hidID->Text = $memberID;

$uploadSuccessful = false;

// Add controls to form
$formProcessor = new FormsProcessor();
$formProcessor->AddControl($txtFirstName);
$formProcessor->AddControl($txtLastName);
$formProcessor->AddControl($txtAddress);
$formProcessor->AddControl($txtContactNo);
$formProcessor->AddControl($txtEmail);
$formProcessor->AddControl($txtIDNo);
$formProcessor->AddControl($txtOthers);
$formProcessor->AddControl($ddlSex);
$formProcessor->AddControl($ddlNationality);
$formProcessor->AddControl($ddlMaritalStat);
$formProcessor->AddControl($ddlIdPresented);
$formProcessor->AddControl($ddlStatus);
$formProcessor->AddControl($btnSubmit);
$formProcessor->AddControl($btnOkay);
$formProcessor->AddControl($btnBack);
$formProcessor->AddControl($hidID);
$formProcessor->AddControl($hidImgPath);
$formProcessor->AddControl($btnBrowse);
$formProcessor->AddControl($hidBday);
$formProcessor->AddControl($hidOrigin);
$formProcessor->AddControl($hidPage);
$formProcessor->ProcessForms();

$year = '0000';
$month = '00';
$day = '00';
$myCalendar = new tc_calendar("hidBday",true);
$myCalendar->setIcon("images/iconCalendar.gif");
$myCalendar->setDate($day,$month,$year);
$myCalendar->setPath("./");
$myCalendar->setYearInterval(1900,(date('Y') - 17));
$myCalendar->dateAllow('1900-01-01','$ddlyear-$ddlmonth-$ddlday');
$myCalendar->disabledDay("Sat");
$myCalendar->disabledDay("sun");

if ($formProcessor->IsPostBack)
{
    $myCalendar->setDate($_POST['hidBday_day'], $_POST['hidBday_month'], $_POST['hidBday_year']);
    $hidBday->Text = $_POST['hidBday_year'] . '-' . $_POST['hidBday_month'] 
            . '-' . $_POST['hidBday_day'];
    
    if ($btnBrowse->SubmittedValue == "UPLOAD")
    {
        // Browse and upload photo
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $extension = end(explode(".", $_FILES["file"]["name"]));

        if ((($_FILES["file"]["type"] == "image/gif") || 
            ($_FILES["file"]["type"] == "image/jpeg") || 
            ($_FILES["file"]["type"] == "image/jpg") || 
            ($_FILES["file"]["type"] == "image/png")) || 
            ($_FILES["file"]["size"] < 1048576) && 
            in_array($extension, $allowedExts))
            {
                if ($_FILES["file"]["error"] > 0)
                {
                    $errorMsg = "Return Code: " . $_FILES["file"]["error"] . "<br>";
                }
                else
                {
                    $imageName = $hidID->Text . ".jpg";
                    $hidImgPath->Text = $imageName;
                    move_uploaded_file($_FILES["file"]["tmp_name"], 
                            "mywebcam/uploads/original/" . $imageName);

                    $msg = "Image successfully uploaded.";
                    $msgTitle = "SUCCESSFUL NOTIFICATION";
                    $uploadSuccessful = true;
                }
            }
            else
            {
                $errorMsg = "Invalid file";
            }
    }


    if ($btnOkay->SubmittedValue == "OKAY")
    {   
        // Check if first and last name of member already exist.
        /*$arrNames = $mtAccountDetails->SelectAccountNameExist(
                $txtFirstName->SubmittedValue, $txtLastName->SubmittedValue);
        $nameExist = $arrNames[0]['Exist'];
        if ($nameExist >= 1)
        {
            $errorMsg = "The name of the member already exist.";
        }
        else
        {*/
            // Check for existing email address.
            if (trim($txtEmail->SubmittedValue) != "")
            {
                $arrEmails = $mtAccountDetails->SelectEmailExist($txtEmail->SubmittedValue);
                $emailExist = $arrEmails[0]['Exist'];
            }
            else
            {
                $emailExist = 0;
            }

            if ($emailExist >= 1)
            {
                $errorMsg = "Email address already exist.";
            }
            else
            {
                $mtAccounts->StartTransaction();

                // insert into accounts
                $insertAccount['UserName'] = date("YmdHis");
                $insertAccount['Password'] = md5("password");
                $insertAccount['AccountTypeID'] = 5;
                $insertAccount['Status'] = $ddlStatus->SubmittedValue;
                $insertAccount['DateCreated'] = "now_usec()";
                $insertAccount['CreatedByAID'] = $_SESSION['aid'];
                $mtAccounts->Insert($insertAccount);
                if ($mtAccounts->HasError)
                {
                    $mtAccounts->RollBackTransaction();
                    $errorMsg = "An error occured: " . $mtAccounts->getErrors();
                }
                else
                {
                    $mtAccounts->CommitTransaction();
                    $lastInsertedID = $mtAccounts->LastInsertID;

                    $mtAccountDetails->StartTransaction();

                    // update account details
                    $updateDetails['AID'] = $lastInsertedID;
                    $updateDetails['FirstName'] = $txtFirstName->SubmittedValue;
                    $updateDetails['LastName'] = $txtLastName->SubmittedValue;
                    $updateDetails['Address'] = $txtAddress->SubmittedValue;
                    $updateDetails['MobileNumber'] = $txtContactNo->SubmittedValue;
                    $updateDetails['Email'] = $txtEmail->SubmittedValue;
                    $mtAccountDetails->UpdateByArray($updateDetails);
                    if ($mtAccountDetails->HasError)
                    {
                        $mtAccountDetails->RollBackTransaction();
                        $errorMsg = "An error occured: " . $mtAccountDetails->getErrors();
                    }
                    else
                    {
                        $mtAccountDetails->CommitTransaction();

                        $arr = $mtMemberInfo->SelectMemberInfoByAID($lastInsertedID);
                        $memberInfoId = $arr[0]['MemberInfoID'];

                        $mtMemberInfo->StartTransaction();                        

                        // update memberinfo
                        $updateInfo['MemberInfoID'] = $memberInfoId;
                        $updateInfo['FirstName'] = $txtFirstName->SubmittedValue;
                        $updateInfo['LastName'] = $txtLastName->SubmittedValue;
                        $updateInfo['Birthdate'] = $hidBday->Text;
                        $updateInfo['Address'] = $txtAddress->SubmittedValue;
                        $updateInfo['ContactNumber'] = $txtContactNo->SubmittedValue;
                        $updateInfo['Email'] = $txtEmail->SubmittedValue;
                        $updateInfo['IdentificationNumber'] = $txtIDNo->SubmittedValue;
                        $updateInfo['DateCreated'] = "now_usec()";
                        $updateInfo['CreatedByAID'] = $_SESSION['aid'];
                        $updateInfo['PhotoFileName'] = $hidImgPath->Text;
                        $updateInfo['Status'] = $ddlStatus->SubmittedValue;
                        $updateInfo['Nationality'] = $ddlNationality->SubmittedValue;
                        $updateInfo['Gender'] = $ddlSex->SubmittedValue;
                        $updateInfo['MaritalStatus'] = $ddlMaritalStat->SubmittedValue;
                        $updateInfo['RegistrationOrigin'] = 1; // Onsite registration
                        if ($ddlIdPresented->SubmittedValue == 4)
                        {
                            $updateInfo['Option1'] = $txtOthers->SubmittedValue;
                        }
                        
                        $updateInfo['IdentificationPresented'] = $ddlIdPresented->SubmittedValue;

                        $mtMemberInfo->UpdateByArray($updateInfo);
                        if ($mtMemberInfo->HasError)
                        {
                            $mtMemberInfo->RollBackTransaction();
                            $errorMsg = "An error occured: " . $mtMemberInfo->getErrors();
                        }
                        else
                        {
                            $mtMemberInfo->CommitTransaction();

                            if ($ddlStatus->SubmittedValue == 1)
                            {
                                $mtCards->StartTransaction();

                                // update cards table
                                // $cardID came from file 'membershipcard.php'
                                $mtCards->UpdateCardStatus($cardID); 
                                if ($mtCards->HasError)
                                {
                                    $mtCards->RollBackTransaction();
                                    $errorMsg = "An error occured: " . $mtCards->getErrors();
                                }
                                else
                                {
                                    $mtCards->CommitTransaction();
                                    
                                    $mtMemberCards->StartTransaction();

                                    // insert into membercards table
                                    $insertMemberCards['AID'] = $lastInsertedID;
                                    $insertMemberCards['CardID'] = $cardID;
                                    $insertMemberCards['DateCreated'] = "now_usec()";
                                    $insertMemberCards['CreatedByAID'] = $_SESSION['aid'];
                                    $insertMemberCards['Status'] = 1;
                                    $mtMemberCards->Insert($insertMemberCards);
                                    if ($mtMemberCards->HasError)
                                    {
                                        $mtMemberCards->RollBackTransaction();
                                        $errorMsg = "An error occured: " . $mtMemberCards->getErrors();
                                    }
                                    else
                                    {
                                        $mtMemberCards->CommitTransaction();
                                    }
                                }
                            }

                            // log to audit trail
                            $mtAuditTrail->StartTransaction();
                            $insertLog['SessionID'] = $_SESSION['sid'];
                            $insertLog['AID'] = $_SESSION['aid'];
                            $insertLog['AuditTrailFunctionID'] = '5';
                            $insertLog['TransDetails'] = "Register member: "
                                    . $txtFirstName->SubmittedValue . " "
                                    . $txtLastName->SubmittedValue;
                            $insertLog['RemoteIP'] = $_SERVER['REMOTE_ADDR'];
                            $insertLog['TransDateTime'] = "now_usec()";
                            $mtAuditTrail->Insert($insertLog);
                            if ($mtAuditTrail->HasError)
                            {
                                $mtAuditTrail->RollBackTransaction();
                                $errorMsg = "An error occured: " . 
                                        $mtAuditTrail->getErrors();
                            }
                            else
                            {
                                $mtAuditTrail->CommitTransaction();

                                $successMsg = "Congratulations! You have " . 
                                        "successfully created a new membership account.";
                                $hidImgPath->Text = "";
                                $hidBday->Text = "";
                                $myCalendar->setDate('00', '00', '0000');
                            }
                        }
                    }
                }
            }
        //}
    }

    if ($btnBack->SubmittedValue == "BACK")
    {
        URL::Redirect('register.php');
        $hidBday->Text = "";
        $myCalendar->setDate('00', '00', '0000');
    }
}
?>
