<?php

/*
 * Created by : Frances Ralph DL. Sison
 * Date Created : April 22 2013
 * Purpose : controller editmembersrecordsprocess
 */

require_once("../../init.inc.php");

$basePath = app::getParam('appdir');

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("Label");
App::LoadControl("ComboBox");

$modulename = "MembershipTimor";

App::LoadModuleClass($modulename, "MTRefCountries");
App::LoadModuleClass($modulename, "MTMemberInfo");
App::LoadModuleClass($modulename, "MTAccountDetails");
App::LoadModuleClass($modulename, "MTRefIdentification");
App::LoadModuleClass($modulename, "MTCards");
App::LoadModuleClass($modulename, "MTMemberCards");
App::LoadModuleClass($modulename, "MTAuditTrail");

$refCountries = new MTRefCountries();
$memberInfo = new MTMemberInfo();
$accountDetails = new MTAccountDetails();
$refIdentification = new MTRefIdentification();
$cards = new MTCards();
$memberCards = new MTMemberCards();
$auditTrail = new MTAuditTrail();

$txtFName = new TextBox("txtFName", "txtFName");
$txtFName->Style = "width:200px;";
$txtFName->Length = 30;
$txtFName->Args = "autocomplete='off' onkeypress ='javascript : return AlphaAndSpaces(event);'";

$txtLName = new TextBox("txtLName", "txtLName");
$txtLName->Style = "width:200px;";
$txtLName->Length = 30;
$txtLName->Args = "autocomplete='off' onkeypress ='javascript : return AlphaAndSpaces(event);'";

$ddlGender = new ComboBox("ddlGender", "ddlGender");
$ddlGroupGender = Null;
$ddlGroupGender[] = new ListItem("Please Select", "0", true);
$ddlGroupGender[] = new ListItem("Male", "1");
$ddlGroupGender[] = new ListItem("Female", "2");
$ddlGender->Items = $ddlGroupGender;
$ddlGender->Style = "width: 200px";

$ddlIdPresented = new ComboBox("ddlIdPresented", "ddlIdPresented");
$ddlGroupIdPresented = Null;
$ddlGroupIdPresented[] = new ListItem("Please Select", "0", true);
$ddlIdPresented->Items = $ddlGroupIdPresented;
//get list of ids
$arrayIds = $refIdentification->getIDPresentedList();
$listIds = new ArrayList();
$listIds->AddArray($arrayIds);
$ddlIdPresented->DataSource = $listIds;
$ddlIdPresented->DataSourceText = "IdentificationName";
$ddlIdPresented->DataSourceValue = "IdentificationID";
$ddlIdPresented->DataBind();
$ddlIdPresented->Style = "width: 200px";

$txtOthers = new TextBox("txtOthers", "txtOthers", "Others");
$txtOthers->Style = "width:195px;";
$txtOthers->Length = 30;
$txtOthers->Args = "autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";

$txtIdNumber = new TextBox("txtIdNumber", "txtIdNumber");
$txtIdNumber->Style = "width:200px;";
$txtIdNumber->Length = 20;
$txtIdNumber->Args = "autocomplete='off' onkeypress ='javascript : return AlphaNumericOnly(event);'";

$ddlNationality = new ComboBox("ddlNationality", "ddlNationality");
$ddlGroupNationality = Null;
$ddlGroupNationality[] = new ListItem("Please Select", "0");
$ddlNationality->Items = $ddlGroupNationality;
//get list of countires/nationalities sorted by priority
$nationalitiesList = $refCountries->selectCountriesByPriority();
$listNationality = new ArrayList();
$listNationality->AddArray($nationalitiesList);
$ddlNationality->DataSource = $listNationality;
$ddlNationality->DataSourceText = "CountryName";
$ddlNationality->DataSourceValue = "CountryID";
$ddlNationality->DataBind();
$ddlNationality->Style = "width: 200px; height: 20px;";

$ddlMaritalStat = new ComboBox("ddlMaritalStat", "ddlMaritalStat", "Marital Status");
$ddlMaritalStat->Style = "width: 200px";
$optionMaritalStatus = null;
$optionMaritalStatus[] = new ListItem("Please Select", "0", true);
$optionMaritalStatus[] = new ListItem("Single", "1");
$optionMaritalStatus[] = new ListItem("Married", "2");
$optionMaritalStatus[] = new ListItem("Widowed", "3");
$ddlMaritalStat->Items = $optionMaritalStatus;

$txtAddress = new TextBox("txtAddress", "txtAddress");
$txtAddress->Style = "width:200px;";
$txtAddress->Length = 300;
$txtAddress->Args = "autocomplete='off' onkeypress='javascript: return AlphaNumericAndSpacesOnly(event);'";

$txtContactNumber = new TextBox("txtContactNumber", "txtContactNumber");
$txtContactNumber->Style = "width:200px;";
$txtContactNumber->Length = 20;
$txtContactNumber->Args = "autocomplete='off' onkeypress ='javascript : return OnlyNumbers(event);'";

$ddlMonth = new ComboBox("ddlMonth", "ddlMonth", "Month");
$ddlGroupMonth = null;
$ddlGroupMonth[] = new ListItem("Month", "0", true);
for($i = 1; $i <= 12; $i++)
{
    $ddlGroupMonth[$i] = new ListItem(date("F", mktime(0, 0, 0, $i, 10)), $i);
}
$ddlMonth->Items = $ddlGroupMonth;

$ddlDay = new ComboBox("ddlDay", "ddlDay", "Day");
$ddlGroupDay = null;
$ddlGroupDay[] = new ListItem("Day", "0", true);
for($i = 1; $i <= 31; $i++)
{
    $ddlGroupDay[$i] = new ListItem($i, $i);
}
$ddlDay->Items = $ddlGroupDay;

$ddlYear = new ComboBox("ddlYear", "ddlYear", "Year");
$ddlGroupYear = null;
$ddlGroupYear[] = new ListItem("Year", "0", true);
$start = date("Y") - 16;
for ($i = 1; $i <= 101; $i++) {
    $start -= 1;
    $ddlGroupYear[$i] = new ListItem($start, $start);
}
$ddlYear->Items = $ddlGroupYear;

$txtEmail = new TextBox("txtEmail", "txtEmail");
$txtEmail->Style = "width:200px;";
$txtEmail->Args = "autocomplete='off'";

$ddlMemberStatus = new ComboBox("ddlMemberStatus", "ddlMemberStatus");
$statusOptions = null;
$statusOptions[] = new ListItem("Inactive", "0");
$statusOptions[] = new ListItem("Active", "1");
$statusOptions[] = new ListItem("Suspended", "2");
$statusOptions[] = new ListItem("Banned", "3");
$ddlMemberStatus->Items = $statusOptions;
$ddlMemberStatus->Style = "width: 90px";
$ddlMemberStatus->Enabled = true;

$txtRemarks = new TextBox("txtRemarks", "txtRemarks");
$txtRemarks->Style = "width:200px;";

$btnOkay = new Button("btnOkay", "btnOkay", "OKAY");
$btnOkay->CssClass = "labelbutton_black";
$btnOkay->IsSubmit = true;

$btnBrowse = new Button("btnBrowse", "btnBrowse", "UPLOAD");
$btnBrowse->IsSubmit = true;

$btnSubmit = new Button("btnSubmit", "btnSubmit", "SUBMIT");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "labelbutton_black";

$btnProceed = new Button("btnProceed", "btnProceed", "OKAY");
$btnProceed->IsSubmit = true;
$btnProceed->CssClass = "labelbutton_black";

$hidImgPath = new Hidden("hidImgPath", "hidImgPath", "Image Path");
$hidID = new Hidden("hidID", "hidID", "Membership ID");
$hidId2 = new Hidden("hidId2", "hidId2", "Membership ID Copy");
$hidMemberID = new Hidden("hidMemberID", "hidMemberID");
$hidOrigin = new Hidden("hidOrigin", "hidOrigin");
$hidPage = new Hidden("hidPage", "hidPage");
$hidLang = new Hidden("hidLang", "hidLang");

$hidPage->Text = "editmembersrecords";


$uploadSuccessful = false;

$fproc = new FormsProcessor();

$fproc->AddControl($txtFName);
$fproc->AddControl($txtLName);
$fproc->AddControl($ddlGender);
$fproc->AddControl($ddlIdPresented);
$fproc->AddControl($txtIdNumber);
$fproc->AddControl($ddlNationality);
$fproc->AddControl($ddlMaritalStat);
$fproc->AddControl($txtAddress);
$fproc->AddControl($txtContactNumber);
$fproc->AddControl($txtOthers);
$fproc->AddControl($ddlMonth);
$fproc->AddControl($ddlDay);
$fproc->AddControl($ddlYear);
$fproc->AddControl($txtEmail);
$fproc->AddControl($ddlMemberStatus);
$fproc->AddControl($txtRemarks);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($btnOkay);
$fproc->AddControl($btnProceed);
$fproc->AddControl($hidMemberID);
$fproc->AddControl($hidImgPath);
$fproc->AddControl($hidID);
$fproc->AddControl($hidId2);
$fproc->AddControl($hidOrigin);
$fproc->AddControl($btnBrowse);
$fproc->AddControl($hidLang);
$fproc->ProcessForms();

if ($fproc->GetPostVar("hiddenId"))
{
    $selectedMemberId = $fproc->GetPostVar("hiddenId");
    $hidMemberID->Text = $selectedMemberId;

    $id = $hidMemberID->Text;
    $_SESSION['hiddenMemberId'] = $id;

    //get member info given a specific member info id
    $getAll = $memberInfo->getMemberAcctDtlsWithID($id);

    $accountId = $getAll[0]["AID"];
    $hidMemberinfoID = $getAll[0]["MemberInfoID"];
    $membershipCardNumber = $getAll[0]["CardNumber"];
    $filename = $getAll[0]["PhotoFileName"];
    $_SESSION['filename2'] = $filename;

    //path for onsite registration
    $path = 'mywebcam/uploads/original/' . $hidMemberinfoID . '.jpg';

    //path for online registration
    $path2 = $basePath.'TegsRegistration/views/mywebcam/uploads/original/' . $hidMemberinfoID . '.jpg';

    $hidId2->Text = $hidMemberinfoID;
    $hidID->Text = $hidMemberinfoID;
   
    $firstName = $getAll[0]["FirstName"];
    $lastName = $getAll[0]["LastName"];
    $birthDate = $getAll[0]["Birthdate"];
    $dateTime = explode('-', $birthDate);
    $month = $dateTime[1][0] . $dateTime[1][1];
    $ddlMonth->SetSelectedValue($month);

    $day = $dateTime[2][0] . $dateTime[2][1];
    $ddlDay->SetSelectedValue($day);
    $year = $dateTime[0][0] . $dateTime[0][1] . $dateTime[0][2] . $dateTime[0][3];
    $ddlYear->SetSelectedValue($year);

    $gender = $getAll[0]["Gender"];
    $idPresented = $getAll[0]["IdentificationPresented"];
    if($idPresented  == 4)
    {
        $option1 = $getAll[0]["Option1"];
    }
    $idNumber = $getAll[0]["IdentificationNumber"];
    $nationality = $getAll[0]["Nationality"];
    $countryList = $refCountries->getCountriesList($nationality);
    $maritalStatus = $getAll[0]["MaritalStatus"];
    $address = $getAll[0]["Address"];
    $contactNumber = $getAll[0]["ContactNumber"];
    $email = $getAll[0]["Email"];
    $membershipDate = $getAll[0]["DateCreated"];
    $dateTime = new DateTime($membershipDate);
    $membershipDate = $dateTime->format("m/d/Y");
    $status = $getAll[0]["Status"];
    $remarks = $getAll[0]["Remarks"];
    $appTool = $getAll[0]["RegistrationOrigin"];
    if($appTool == 1)
    {
        $path = $path;
    }
    else
    {
        $path = $path2;
    }
    $fp = fopen($path, "r");
    if ($fp)
    {
        $hidImgPath->Text = $hidMemberinfoID . ".jpg";
    }
    else
    {
        $hidImgPath->Text = "";
    }
    
    $hidOrigin->Text = $appTool;
    $txtFName->Text = $firstName;
    $txtLName->Text = $lastName;
    $ddlGender->SetSelectedValue($gender);
    $ddlIdPresented->SetSelectedValue($idPresented);
    if($idPresented == 4)
    {
        $option1 = $getAll[0]["Option1"];
    }
    $txtOthers->Text = $option1;
    $txtIdNumber->Text = $idNumber;
    $ddlNationality->SetSelectedValue($nationality);
    $ddlMaritalStat->SetSelectedValue($maritalStatus);
    $txtAddress->Text = $address;
    $txtContactNumber->Text = $contactNumber;
    $txtEmail->Text = $email;
    $ddlMemberStatus->SetSelectedValue($status);
    $txtRemarks->Text = $remarks;
}


if($fproc->IsPostBack)
{
    //for photo taking and/or uploading
    if($btnBrowse->SubmittedValue == "UPLOAD")
    {
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $extension = end(explode(".", $_FILES["filebrowse"]["name"]));

        if((($_FILES["filebrowse"]["type"] == "image/gif")
                || ($_FILES["filebrowse"]["type"] == "image/jpeg")
                || ($_FILES["filebrowse"]["type"] == "image/jpg")
                || ($_FILES["filebrowse"]["type"] == "image/png"))
                || ($_FILES["filebrowse"]["size"] < 1048576)
                && in_array($extension, $allowedExts))
        {
            if($_FILES["filebrowse"]["error"] > 0)
            {
                $errorMessage = "Return Code: " . $_FILES["filebrowse"]["error"] . "<br>";
            }
            else
            {
                $imageName = $hidID->Text . ".jpg";
                $hidImgPath->Text = $imageName;

                if($hidOrigin->Text == 1)
                {
                    move_uploaded_file($_FILES["filebrowse"]["tmp_name"], "mywebcam/uploads/original/" . $imageName);
                }
                else if($hidOrigin->Text == 2)
                {
                    move_uploaded_file($_FILES["filebrowse"]["tmp_name"], $basePath. "TegsRegistration/views/mywebcam/uploads/original/" . $imageName);
                }
                
                $message = "Image successfully uploaded.";
                $messageTitle = "SUCCESSFUL NOTIFICATION";
                
                $_SESSION['btnbrowse'] = $btnBrowse->SubmittedValue;
                $uploadSuccessful = true;                
            }
        }
        else
        {
            $errorTitle = "ERROR!";
            $errorMessage = "Invalid file";
        }
        
        //unset($_SESSION['btnbrowse']);
        $getAll = $memberInfo->getMemberAcctDtlsWithID($_SESSION['hiddenMemberId']);
        $status = $getAll[0]["Status"];
        $appTool = $getAll[0]["RegistrationOrigin"];
        $membershipCardNumber = $getAll[0]["CardNumber"];
        $membershipDate = $getAll[0]["DateCreated"];
        $dateTime = new DateTime($membershipDate);
        $membershipDate = $dateTime->format("m/d/Y");
        $ddlMonth->SetSelectedValue($ddlMonth->SubmittedValue);
        $ddlDay->SetSelectedValue($ddlDay->SubmittedValue);
        $ddlYear->SetSelectedValue($ddlYear->SubmittedValue);
        $txtFName->Text = $txtFName->SubmittedValue;
        $txtLName->Text = $txtLName->SubmittedValue;
        $ddlGender->SetSelectedValue($ddlGender->SubmittedValue);
        $ddlIdPresented->SetSelectedValue($ddlIdPresented->SubmittedValue);
        if($ddlIdPresented == 4)
        {
            $txtOthers->Text = $txtOthers->SubmittedValue;
        }
        $txtIdNumber->Text = $txtIdNumber->SubmittedValue;
        $ddlNationality->SetSelectedValue($ddlNationality->SubmittedValue);
        $ddlMaritalStat->SetSelectedValue($ddlMaritalStat->SubmittedValue);
        $txtAddress->Text = $txtAddress->SubmittedValue;
        $txtContactNumber->Text = $txtContactNumber->SubmittedValue;
        $txtEmail->Text = $txtEmail->SubmittedValue;
        $ddlMemberStatus->SetSelectedValue($ddlMemberStatus->SubmittedValue);
        $txtRemarks->Text = $txtRemarks->SubmittedValue;
    }

    if($btnSubmit->SubmittedValue == "SUBMIT")
    {
        $id = $_SESSION['hiddenMemberId'];
        $getAll = $memberInfo->getMemberAcctDtlsWithID($id);

        $accountId = $getAll[0]["AID"];
        $hidMemberinfoID = $getAll[0]["MemberInfoID"];
        $membershipCardNumber = $getAll[0]["CardNumber"];
        $firstName = $getAll[0]["FirstName"];
        $lastName = $getAll[0]["LastName"];
        $birthDate = $getAll[0]["Birthdate"];
        $dateTime = new DateTime($birthDate);
        $birthDate = $dateTime->format("m-d-Y");
        $gender = $getAll[0]["Gender"];
        $idPresented = $getAll[0]["IdentificationPresented"];
        $idNumber = $getAll[0]["IdentificationNumber"];
        $nationality = $getAll[0]["Nationality"];
        $countryList = $refCountries->getCountriesList($nationality);
        $maritalStatus = $getAll[0]["MaritalStatus"];
        $address = $getAll[0]["Address"];
        $contactNumber = $getAll[0]["ContactNumber"];
        $email = $getAll[0]["Email"];
        $membershipDate = $getAll[0]["DateCreated"];
        $dateTime = new DateTime($membershipDate);
        $membershipDate = $dateTime->format("m/d/Y");
        $status = $getAll[0]["Status"];
        $remarks = $getAll[0]["Remarks"];
        $appTool = $getAll[0]["RegistrationOrigin"];

        $FName = $txtFName->SubmittedValue;
        $LName = $txtLName->SubmittedValue;
        $Email = $txtEmail->SubmittedValue;
        $Gender = $ddlGender->SubmittedValue;
        $Birthdate = $ddlYear->SubmittedValue . "-" . $ddlMonth->SubmittedValue . "-" . $ddlDay->SubmittedValue;
        $IdPresented = $ddlIdPresented->SubmittedValue;
        $IDnumber = $txtIdNumber->SubmittedValue;
        $Nationality = $ddlNationality->SubmittedValue;
        $Address = $txtAddress->SubmittedValue;
        $ContactNumber = $txtContactNumber->SubmittedValue;
        $Status = $ddlMemberStatus->SubmittedValue;
        $Remarks = $txtRemarks->SubmittedValue;
        
        $dateTimeNow = date('Ymd', time());
        $birthDateSubmitted = $ddlYear->SubmittedValue . str_pad($ddlMonth->SubmittedValue, 2, "0", STR_PAD_LEFT) . str_pad($ddlDay->SubmittedValue, 2, "0", STR_PAD_LEFT);

        if($ddlIdPresented->SubmittedValue == 4)
        {
            $IdPresented = $txtOthers->SubmittedValue;
        }
        else
        {
            $IdPresented = $ddlIdPresented->SubmittedValue;
        }

        $arrayEmail = $memberInfo->checkEmail($txtEmail->SubmittedValue);
        $pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$";
        
        if($txtFName->SubmittedValue == NULL || $txtFName->SubmittedValue == '' || strlen($txtFName->SubmittedValue) == 0 || ($txtLName->SubmittedValue == NULL || $txtLName->SubmittedValue == '' || strlen($txtLName->SubmittedValue) == 0)
          || $ddlMonth->SubmittedValue == 0 || $ddlDay->SubmittedValue == 0 || $ddlYear->SubmittedValue == 0 || $ddlGender->SubmittedValue == 0 || $ddlIdPresented->SubmittedValue == 0 || $ddlIdPresented->SubmittedValue ==4 && $txtOthers == ""
          || $txtIdNumber->SubmittedValue == NULL || $txtIdNumber->SubmittedValue == '' || strlen($txtIdNumber->SubmittedValue) == 0)
        {
            $errorTitle = "INCOMPLETE INFORMATION";
            $errorMessage = "Kindly fill out all required fields.";
        }
        
        //checks if submitted email is a valid email
        else if($txtEmail->SubmittedValue != "" && (!filter_var($txtEmail->SubmittedValue, FILTER_VALIDATE_EMAIL) || !eregi($pattern, $txtEmail->SubmittedValue)))
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Please supply valid Email Address.";
        }
        //checks if submitted age is less than 17
        else if($ddlMonth->SubmittedValue != 0 && $ddlDay->SubmittedValue != 0 && $ddlYear->SubmittedValue != 0)
        {
            if(($dateTimeNow - $birthDateSubmitted) < 170000)
            {
                $errorTitle = "INCOMPLETE INFORMATION";
                $errorMessage = "Age below 17 years old is prohibited.";
            }
            else
            {
                $errorTitle = "CONFIRMATION";
                $errorMessage = "You are about to change details in the membership account. Do you wish to continue?";
                $confirm = true;
            }
        }
        else
        {
            $errorTitle = "CONFIRMATION";
            $errorMessage = "You are about to change details in the membership account. Do you wish to continue?";
            $confirm = true;
        }
    }

    if($btnProceed->SubmittedValue == "OKAY")
    {
        $id = $_SESSION['hiddenMemberId'];
        $getAll = $memberInfo->getMemberAcctDtlsWithID($id);
        
        $accountId = $getAll[0]["AID"];
        $_SESSION['accountId'] = $accountId;
        $hidMemberinfoID = $getAll[0]["MemberInfoID"];
        $membershipCardNumber = $getAll[0]["CardNumber"];
        $filename = $getAll[0]["PhotoFileName"];
        $firstName = $getAll[0]["FirstName"];
        $lastName = $getAll[0]["LastName"];
        $birthDate = $getAll[0]["Birthdate"];
        $dateTime = new DateTime($birthDate);
        $birthDate = $dateTime->format("m-d-Y");
        $gender = $getAll[0]["Gender"];
        $idPresented = $getAll[0]["IdentificationPresented"];
        $idNumber = $getAll[0]["IdentificationNumber"];
        $nationality = $getAll[0]["Nationality"];
        $countryList = $refCountries->getCountriesList($nationality);
        $maritalStatus = $getAll[0]["MaritalStatus"];
        $address = $getAll[0]["Address"];
        $contactNumber = $getAll[0]["ContactNumber"];
        $email = $getAll[0]["Email"];
        $membershipDate = $getAll[0]["DateCreated"];
        $dateTime = new DateTime($membershipDate);
        $membershipDate = $dateTime->format("m/d/Y");
        $status = $getAll[0]["Status"];
        $remarks = $getAll[0]["Remarks"];
        $appTool = $getAll[0]["RegistrationOrigin"];
        $memberInfo->StartTransaction();
        //update memberinfo table
        $updateInfo["MemberInfoID"] = $hidMemberinfoID;
        $updateInfo["FirstName"] = $txtFName->SubmittedValue;
        $updateInfo["LastName"] = $txtLName->SubmittedValue;
        $updateInfo["Email"] = $txtEmail->SubmittedValue;
        if($appTool == 2 && $ddlMemberStatus->SubmittedValue == 1)
        {
            $updateInfo["DateCreated"] = "now_usec()";
        }
        $updateInfo["Gender"] = $ddlGender->SubmittedValue;
        $updateInfo["Birthdate"] = $ddlYear->SubmittedValue . "-" . $ddlMonth->SubmittedValue . "-" . $ddlDay->SubmittedValue;
        if($ddlIdPresented->SubmittedValue == 4)
        {
            $updateInfo['Option1'] = $txtOthers->SubmittedValue;
        }                
        $updateInfo['IdentificationPresented'] = $ddlIdPresented->SubmittedValue;
        $updateInfo["IdentificationNumber"] = $txtIdNumber->SubmittedValue;
        $updateInfo["Nationality"] = $ddlNationality->SubmittedValue;
        $updateInfo["MaritalStatus"] = $ddlMaritalStat->SubmittedValue;
        $updateInfo["Address"] = $txtAddress->SubmittedValue;
        $updateInfo["ContactNumber"] = $txtContactNumber->SubmittedValue;
        $updateInfo["Status"] = $ddlMemberStatus->SubmittedValue;
        if($filename != '')
        {
            $hidImgPath->Text = $hidMemberinfoID . ".jpg";
        }
        else
        {
            $hidImgPath->Text = "";
        }
        $updateInfo["PhotoFileName"] = $hidImgPath->SubmittedValue;
        $memberInfo->UpdateByArray($updateInfo);

        if($memberInfo->HasError)
        {
            $memberInfo->RollBackTransaction();
            $errorMessage = "An error occured: " . $memberInfo->getErrors();
        }
        else
        {
            $memberInfo->CommitTransaction();
            
            if($firstName != $txtFName->SubmittedValue || $lastName != $txtLName->SubmittedValue || $birthDate != (STR_PAD($ddlMonth->SubmittedValue, 2, 0, STR_PAD_LEFT) . "-" . STR_PAD($ddlDay->SubmittedValue, 2, 0, STR_PAD_LEFT) . "-" . STR_PAD($ddlYear->SubmittedValue, 2, 0, STR_PAD_LEFT)) ||
                                        $gender != $ddlGender->SubmittedValue || $idPresented != $ddlIdPresented->SubmittedValue || $idNumber != $txtIdNumber->SubmittedValue || $nationality != $ddlNationality->SubmittedValue || $maritalStatus != $ddlMaritalStat->SubmittedValue ||
                                        $address != $txtAddress->SubmittedValue || $contactNumber != $txtContactNumber->SubmittedValue || $email != $txtEmail->SubmittedValue || $filename != $hidID->Text)
            {
                // Log to Audit Trail update member profile
                $auditTrail->StartTransaction();
                $auditMemberProfile["SessionID"] = $_SESSION['sid'];
                $auditMemberProfile["AID"] = $_SESSION['aid'];
                $auditMemberProfile['AuditTrailFunctionID'] = '11';
                $auditMemberProfile["TransDetails"] = "Update Player Profile: " . $txtFName->SubmittedValue . " " . $txtLName->SubmittedValue;
                $auditMemberProfile["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $auditMemberProfile["TransDateTime"] = "now_usec()";

                $auditTrail->Insert($auditMemberProfile);
                if($auditTrail->HasError)
                {
                    $errorMessage = $auditTrail->getError();
                    $auditTrail->RollBackTransaction();
                }
                else
                {
                    $auditTrail->CommitTransaction();
                }
            }
       
            $accountDetails->StartTransaction();
            //update accountdetails table
            $updateDetails["AID"] = $accountId;
            $updateDetails["Remarks"] = $txtRemarks->SubmittedValue;
            $accountDetails->UpdateByArray($updateDetails);
            if($accountDetails->HasError)
            {
                $accountDetails->RollBackTransaction();
                $errorMessage = "An error occured: " . $accountDetails->getErrors();
            }
            else
            {
                $accountDetails->CommitTransaction();
                $statusText = '';
                if($remarks != $txtRemarks->SubmittedValue || $status != $ddlMemberStatus->SubmittedValue)
                {
                    if($status == 0)
                    {
                        $statusText = "Inactive";
                    }
                    else if($status == 1)
                    {
                        $statusText = "Active";
                    }
                    else if($status == 2)
                    {
                        $statusText = "Suspended";
                    }
                    else if($status == 3)
                    {
                        $statusText = "Banned";
                    }
                                    
                    // Log to Audit Trail update member status
                    $auditTrail->StartTransaction();
                    $auditMemberStatus["SessionID"] = $_SESSION['sid'];
                    $auditMemberStatus["AID"] = $_SESSION['aid'];
                    $auditMemberStatus['AuditTrailFunctionID'] = '12';
                    $auditMemberStatus["TransDetails"] = "Update Player Status: " . $txtFName->SubmittedValue . ' ' . $txtLName->SubmittedValue . ' ' . "from $statusText to $ddlMemberStatus->SelectedText";
                    $auditMemberStatus["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                    $auditMemberStatus["TransDateTime"] = "now_usec()";

                    $auditTrail->Insert($auditMemberStatus);
                    if($auditTrail->HasError)
                    {
                        $auditTrail->RollBackTransaction();
                        $errorMessage = "An error occured: " . $auditTrail->getErrors();
                    }
                    else
                    {
                        $auditTrail->CommitTransaction();   
                    }
                }
                
                $where = " Where AID = $accountId";
                $isExisting = $memberCards->SelectByWhere($where);
                if (count($isExisting) > 0)
                {
                    $memberInfo->StartTransaction();
                    $updateInfo["MemberInfoID"] = $hidMemberinfoID;
                    $updateInfo["Status"] = $ddlMemberStatus->SubmittedValue;
                    $memberInfo->UpdateByArray($updateInfo);

                    if($memberInfo->HasError)
                    {
                        $memberInfo->RollBackTransaction();
                        $errorMessage = "An error occured: " . $memberInfo->getErrors();
                    }
                    else
                    {
                        $memberInfo->CommitTransaction();
                    }
                }
                else
                {
                    $getStatus = $memberInfo->getMemberStatus($hidMemberinfoID);
                    
                    // if the member will be ACTIVATED
                    if($getStatus[0]['Status'] == 1)
                    {
                        $cardStatus = $cards->SelectMemberCardNo();
                        $cardId = $cardStatus[0]['CardID'];
                        $cards->StartTransaction();
                        //update cards table
                        $updateCards["CardID"] = $cardId;
                        $updateCards["Status"] = 1;
                        $cards->UpdateByArray($updateCards);
                        if($cards->HasError)
                        {
                            $cards->RollBackTransaction();
                            $errorMessage = "An error occured: " . $cards->getErrors();
                        }
                        else
                        {
                            $cards->CommitTransaction();
                            $member = $memberInfo->selectMemberByID($hidMemberinfoID);

                            $memberCardStatus = $memberCards->selectMemberCardID($hidMemberinfoID);

                            if(count($memberCardStatus) == 0)
                            {
                                $memberCards->StartTransaction();
                                //insert in membercards table
                                $insertMemberCards["AID"] = $member[0]["AID"];
                                $insertMemberCards["CardID"] = $cardStatus[0]["CardID"];
                                $insertMemberCards["DateCreated"] = "now_usec()";
                                $insertMemberCards["CreatedByAID"] = $_SESSION['aid'];
                                $insertMemberCards["Status"] = 1;
                                $memberCards->Insert($insertMemberCards);
                                if($memberCards->HasError)
                                {
                                    $memberCards->RollBackTransaction();
                                    $errorMessage = "An error occured: " . $memberCards->getErrors();
                                }
                                else
                                {
                                    $memberCards->CommitTransaction();
                                }
                            }
                            else
                            {
                                $memberCards->StartTransaction();
                                //update memberCards table
                                $updateMemberCards["MemberCardID"] = $memberCardStatus[0]['MemberCardID'];
                                $updateMemberCards["CardID"] = $memberCardStatus[0]['CardID'];
                                $updateMemberCards["DateCreated"] = "now_usec()";
                                $updateMemberCards["CreatedByAID"] = $_SESSION['aid'];
                                $updateMemberCards["Status"] = 1;
                                $memberCards->UpdateByArray($updateMemberCards);
                                if($memberCards->HasError)
                                {
                                    $memberCards->RollBackTransaction();
                                    $errorMessage = "An error occured: " . $memberCards->getErrors();
                                }
                                else
                                {
                                    $memberCards->CommitTransaction();                    
                                }
                            }
                        }
                    }                    
                }
            }
            
        }

        $successMessage = "The member's profile is successfully updated.";
        $success = true;
        
    }
}
?>
