<?php
/*
 * Added By : Jerome F. Jose
 * Added On : June 21, 2012
 * Purpose : Change Password Admin
 */

require_once("../../init.inc.php");
$modulename = "MembershipTimor";
App::LoadModuleClass($modulename,"MTAccounts");
App::LoadModuleClass($modulename,"MTAuditTrail");
App::LoadModuleClass($modulename,"MTAccountSessions");
App::LoadModuleClass($modulename,"MTAccountDetails");
App::LoadModuleClass($modulename, "MTPasswordUpdateRequests");
$accounts = new MTAccounts();
$accountdetails = new MTAccountDetails();
$mtaudittrail = new MTAuditTrail();
$accountsessions = new MTAccountSessions();
$passwordupdaterequest = new MTPasswordUpdateRequests();

App::LoadCore("PHPMailer.class.php");

APP::LoadControl("TextBox");
App::LoadControl("Button");

$fproc = new FormsProcessor();

$txtPassword = new TextBox("txtPassword","txtPassword");
$txtPassword->Style = "width:400px;";
$txtPassword->Password = true;
$txtPassword->Length = 12;
$txtPassword->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtNewPassword = new TextBox("txtNewPassword", "txtNewPassword");
$txtNewPassword->Style = "width:400px;";
$txtNewPassword->Password = true;
$txtNewPassword->Length = 12;
$txtNewPassword->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtConfPassword = new TextBox("txtConfPassword", "txtConfPassword");
$txtConfPassword->Style = "width:400px;";
$txtConfPassword->Password = true;
$txtConfPassword->Length = 12;
$txtConfPassword->Args="autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$btnOk = new Button("btnOK","btnOK","SUBMIT");
$btnOk->CssClass = "labelbutton_black";
//$btnOk->IsSubmit = true;
$btnOk->Args = "onclick='javascript: return checkchangepassword();' ";

$passSubmit = new Button("passSubmit","passSubmit", "OKAY");
$passSubmit->CssClass = "labelbutton_black";
$passSubmit->Args = "onclick=document.getElementById('light11').style.display='none';document.getElementById('fade').style.display='none';";
$passSubmit->IsSubmit = true;


$btncancel = new Button("btncancel","btncancel","CANCEL");
$btncancel->CssClass = "labelbutton_black";
$btncancel->Args = "onclick='javascript: return cancellink();' ";

$fproc->AddControl($txtPassword);
$fproc->AddControl($txtNewPassword);
$fproc->AddControl($txtConfPassword);
$fproc->AddControl($btnOk);
$fproc->AddControl($passSubmit);
$fproc->AddControl($btncancel);


$fproc->ProcessForms();

$rcode = $_GET['Uname'];
$where = " WHERE pr.RequestCode = '" . $rcode . "' AND pr.Status = 0";
$acctdtls = $passwordupdaterequest->SelectByHash($where);
if(count($acctdtls) > 0)
{
    $current_password = $acctdtls[0]["Password"];
    $_SESSION['currentPassword'] = $current_password;
    $username = $acctdtls[0]["UserName"];
    $AcctID = $acctdtls[0]["AID"];
    $requestid = $acctdtls[0]["PasswordUpdateRequestID"];
}

if($fproc->IsPostBack)
{
    $current_password = (isset($_SESSION['currentPassword']) && $_SESSION['currentPassword'] != NULL ) ?  $_SESSION['currentPassword'] : NULL;
    if($passSubmit->SubmittedValue == "OKAY")
    {
           $oldpass = $txtPassword->SubmittedValue;
           $newpass = $txtNewPassword->SubmittedValue;
           $newpassforemail = $newpass;
           $remoteip = $_SERVER['REMOTE_ADDR'];
    if ($current_password == MD5($txtPassword->Text))
        {
                    $accounts->StartTransaction();
                    $updatePass = $accounts->UpdateChangeResetPassword($newpass, $AcctID);
                    if($accounts->HasError)
                    {
                        $accounts->RollBackTransaction();
                    }else
                    {
                         $accounts->CommitTransaction();
                        $passwordupdaterequest->StartTransaction();
                        $passwordupdaterequest->UpdateStatus($rcode);
                        if($passwordupdaterequest->HasError)
                    {
                        $passwordupdaterequest->RollBackTransaction();
                    }else
                    {
                        $passwordupdaterequest->CommitTransaction();
                    }
                       
                        
//                        -- insert in audit trail
                        $mtaudittrail->StartTransaction();
                        $arrAudittrail['SessionID'] = '';
                        $arrAudittrail['AuditTrailFunctionID'] = '3';
                        $arrAudittrail['AID'] = $AcctID;
                        $arrAudittrail['TransDetails'] = "Change Reset password for  AID :".$AcctID." ";
                        $arrAudittrail['RemoteIP'] = $remoteip;
                        $arrAudittrail['TransDateTime'] = "now_usec()";
                        $mtaudittrail->Insert($arrAudittrail);
                        if($mtaudittrail->HasError)
                        {
                            $returnmsg = " Error inserting in audit trail";
                        }  else {
                            $mtaudittrail->CommitTransaction();
                            $successmsg = "success";
                            session_destroy();
                            session_unset();
//                            $returnmsg = "You have successfully changed your password. An email notification was sent to the user account. ";
                                }

                   }
//
//        $emailaddr = $Email;
//        $where = " WHERE Email = '$emailaddr' "; 
//        $userdetails = $accountdetails->SelectByWhere($where);
//        $id = $userdetails[0]["AID"];
//        $acctname = $userdetails[0]["FirstName"].' '.$userdetails[0]["MiddleName"].' '.$userdetails[0]["LastName"];
//              
//        $where2 = " WHERE AID = '$id'";
//        $userdetails2 = $accounts->SelectByWhere($where2);
//        $name = $userdetails2[0]["UserName"];
//      
//        $newpass = substr(session_id(),0,8);
//          
//        // Sending of Email
//        $pm = new PHPMailer();
//        $pm->AddAddress($emailaddr, $acctname);
//          
//        $pageURL = 'http';
//        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
//        $pageURL .= "://";
//        $folder = $_SERVER["REQUEST_URI"];
//        $folder = substr($folder,0,strrpos($folder,'/') + 1);
//        if ($_SERVER["SERVER_PORT"] != "80") 
//        {
//          $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
//        } 
//        else 
//        {
//          $pageURL .= $_SERVER["SERVER_NAME"].$folder;
//        }
//        
//        $pm->IsHTML(true);
//
//        $pm->Body = "Dear ".$acctname."<br/><br/>
//            This is to inform you that your password has been changed on this date ".date("m/d/Y")." and time ".date("H:i:s").". Here are your new Account Details:<br/><br/>
//            Username: <b>".$name."</b><br/>
//            Password: <b>".$newpassforemail."</b><br/><br/>".
//        "If you didn’t perform this procedure, email us at________________."."<br/><br/>
//         Regards,<br/>e-Games Management";
//        $pm->FromName = "no-reply";
//        $pm->From = "no-reply@ticketmanagementsystem.com";
//        $pm->Host = "localhost";
//        $pm->Subject = "ZENTRUM E-GAMES CATALOG CHANGE PASSWORD";
//        $email_sent = $pm->Send();
//        
//        if($email_sent)
//        {
//            $successmsg = "success";
//            
//        }
         }
        else {
            $errormsg = "Current Password did not match";
            }
       }
}
?>
