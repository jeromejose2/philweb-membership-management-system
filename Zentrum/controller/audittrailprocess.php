<?php
/*
 * @author Jerome Jose 04-18-2013
 * Purpose   : controller for audittrail
 * @modified Mark Kenneth Esguerra 06-19-2013
 * Add Date Range
 */

require_once("../../init.inc.php");

$moduleName = "MembershipTimor";
App::LoadModuleClass($moduleName,"MTAuditTrail");
App::LoadModuleClass($moduleName,"MTAuditFunction");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("TextBox");
App::LoadControl("PagingControl2");

$fProc         = new FormsProcessor();
$auditTrail    = new MTAuditTrail();
$auditFunction = new MTAuditFunction();

$txtDateFrom           = new TextBox("txtDateFrom","txtDateFrom");
$txtDateFrom->Length   = "10";
$txtDateFrom->Style    = "text-align:center";
$txtDateFrom->ReadOnly = true;
$txtDateFrom->Text     = date("m/d/Y");

$txtDateTo           = new TextBox("txtDateTo","txtDateTo");
$txtDateTo->Length   = "10";
$txtDateTo->Style    = "text-align:center";
$txtDateTo->ReadOnly = true;
$txtDateTo->Text     = date("m/d/Y");

$ddlFunction                  = new ComboBox("ddlFunction","ddlFunction","Audit Trail Function: ");
$ddlFunction->ShowCaption     = true;
$arralFunctions               = $auditFunction->SelectAllWithOrder();
$allFunctionList              = new ArrayList();
$allFunctionList->AddArray($arralFunctions);
$ddlFunction->ClearItems();
$liItem                       = null;
$liItem[]                     = new ListItem("ALL","0",true);
$ddlFunction->Items           = $liItem;
$ddlFunction->DataSource      = $allFunctionList;
$ddlFunction->DataSourceText  = "AuditFunctionName";
$ddlFunction->DataSourceValue = "AuditTrailFunctionID";
$ddlFunction->DataBind();

$btnSubmit           = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->Args     = "onclick='javascript: return checkaudittrail();'";
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "labelbutton_black";
$btnSubmit->Style    = "padding:2px;width:130px;";

//pagination
$itemsPerPage               = 20;
$pgCon                      = new PagingControl2($itemsPerPage,1);
$pgCon->URL                 = "javascript:ChangePage(%currentpage);";
$pgCon->ShowMoveToFirstPage = true;
$pgCon->ShowMoveToLastPage  = true;
$pgCon->PageGroup           = 5;


$fProc->AddControl($txtDateFrom);
$fProc->AddControl($txtDateTo);
$fProc->AddControl($ddlFunction);
$fProc->AddControl($btnSubmit);

$fProc->ProcessForms();
if ( $fProc->IsPostBack )
{
    if ( $btnSubmit->SubmittedValue == "Submit" )
    {
        $pgCon->SelectedPage = 1;
    }
    $DateFrom         = $txtDateFrom->SubmittedValue;
    $DateTo           = $txtDateTo->SubmittedValue;
    $functionId       = $ddlFunction->SubmittedValue;
    $dateTime         = new DateTime($DateFrom);
    $DateFrom         = $dateTime->format('Y-m-d');
    $dateTime2        = new DateTime($DateTo);
    $DateTo           = $dateTime2->format('Y-m-d');
    $transactionCount = $auditTrail->SelectCount($DateFrom,$DateTo,$functionId);

    $rowCount      = $transactionCount[0][0];
    $pgCon->Initialize($itemsPerPage,$rowCount);
    $pageAudit     = $pgCon->PreRender();
    $page          = ($pgCon->SelectedItemFrom - 1);
    $arrayAud      = $auditTrail->SelectWithLimit($DateFrom,$DateTo,$functionId,$page,$itemsPerPage);
    $tableDataList = new ArrayList();
    $tableDataList->AddArray($arrayAud);
}
?>