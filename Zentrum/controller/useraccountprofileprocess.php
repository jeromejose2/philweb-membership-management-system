<?php

/*
 * Created by : Angelo Cubos
 * Date Created : April 01 2013
 * Purpose : controller useraccountprofileprocess
 */
require_once("../../init.inc.php");

App::LoadControl("TextBox");
APP::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("Label");
App::LoadControl("ComboBox");

$moduleName = "MembershipTimor";
App::LoadModuleClass($moduleName, "MTAccounts");
App::LoadModuleClass($moduleName, "MTAccountDetails");
App::LoadModuleClass($moduleName, "MTAccountTypes");
App::LoadModuleClass($moduleName, "MTAccessRights");

$mtAccounts = new MTAccounts();
$mtAccountDetails = new MTAccountDetails();
$mtAccountTypes = new MTAccountTypes();

$fProc = new FormsProcessor();

$ddlUserName = new ComboBox("ddlUsername", "ddlUsername");
$ddlGroupUserName[] = new ListItem("", "0", true);
$ddlUserName->Items = $ddlGroupUserName;
$ddlGroupUserName = new ArrayList();
$getgroups = $mtAccounts->userList();
$ddlGroupUserName->AddArray($getgroups);
$ddlUserName->DataSource = $ddlGroupUserName;
$ddlUserName->DataSourceText = "UserName";
$ddlUserName->DataSourceValue = "AID";
$ddlUserName->DataBind();
$ddlUserName->Args = "onchange = 'javascript: onchange_username();'";
$ddlUserName->Style = "width: 280px";

$btnSearch = new Button("btnSearch", "btnSearch", "SEARCH");
$btnSearch->IsSubmit = true;
$btnSearch->Style = "width: 200px";
$btnSearch->CssClass = "labelbutton_black";
$btnSearch->Args = "onclick = 'javascript: return showData();'";

$btnEdit = new Button("btnEdit", "btnEdit", "EDIT");
$btnEdit->IsSubmit = true;
$btnEdit->CssClass = "labelbutton_black";
$btnEdit->Enabled = false;

$txtFirstName = new TextBox("txtFirstName", "txtFirstName");
$txtFirstName->Style = "width:200px;text-align:center;";
$txtFirstName->Enabled = false;

$txtMiddleName = new TextBox("txtMiddleName", "txtMiddleName");
$txtMiddleName->Style = "width:200px;text-align:center;";
$txtMiddleName->Enabled = false;

$txtLastName = new TextBox("txtLastName", "txtLastName");
$txtLastName->Style = "width:200px;text-align:center;";
$txtLastName->Enabled = false;

$txtEmail = new TextBox("txtEmail", "txtEmail");
$txtEmail->Style = "width:200px;text-align:center;";
$txtEmail->Length = "50";
$txtEmail->Enabled = false;

$txtPosition = new TextBox("txtPosition", "txtPosition");
$txtPosition->Style = "width:200px;text-align:center;";
$txtPosition->Length = "50";
$txtPosition->Enabled = false;

$ddlAccType = new ComboBox("ddlAccType", "ddlAccType");
$groupList[] = new ListItem("", "0", true);
$ddlAccType->Items = $groupList;
$getall = $mtAccountTypes->getAcctTypes();
$groupList = new ArrayList();
$groupList->AddArray($getall);
$ddlAccType->DataSource = $groupList;
$ddlAccType->DataSourceText = "Name";
$ddlAccType->DataSourceValue = "AccountTypeID";
$ddlAccType->DataBind();
$ddlAccType->Style = "width: 200px; height: 20px; text-align:center;";
$ddlAccType->Enabled = false;

$fProc->AddControl($txtFirstName);
$fProc->AddControl($txtMiddleName);
$fProc->AddControl($txtLastName);
$fProc->AddControl($txtEmail);
$fProc->AddControl($txtPosition);
$fProc->AddControl($ddlUserName);
$fProc->AddControl($btnSearch);
$fProc->AddControl($btnEdit);
$fProc->AddControl($ddlAccType);


$fProc->ProcessForms();

$userAccountID = "";
if (strlen($_SESSION['NewlyAddedAID']) > 0)
{
    $userAccountID = $_SESSION['NewlyAddedAID'];
    $ddlUserName->SetSelectedValue($userAccountID);
}
if (strlen($userAccountID) > 0)
{
    $arrUserInfo = $mtAccounts->userInfo($userAccountID);
    $userName = $arrUserInfo[0]['UserName'];
    $accountTypeID = $arrUserInfo[0]['AccountTypeID'];
    $dateCreated = $arrUserInfo[0]['DateCreated'];
    $status = $arrUserInfo[0]['Status'];

    $arrUserDetl = $mtAccountDetails->userDetl($userAccountID);
    $txtFirstName->Text = $arrUserDetl[0]['FirstName'];
    $txtMiddleName->Text = $arrUserDetl[0]['MiddleName'];
    $txtLastName->Text = $arrUserDetl[0]['LastName'];
    $txtEmail->Text = $arrUserDetl[0]['Email'];
    $txtPosition->Text = $arrUserDetl[0]['Position'];
    $ddlAccType->SetSelectedValue($accountTypeID);
}

if ($fProc->IsPostBack)
{
    if ($btnEdit->SubmittedValue == "EDIT")
    {
        if (strlen($_SESSION['NewlyAddedAID']) > 0)
        {
            $_SESSION['EditAcct'] = $_SESSION['NewlyAddedAID'];
        }
        app::pr("<script>window.location.href='updateacct.php';</script>");
        $_SESSION['NewlyAddedAID'] = "";
    }
    if ($btnSearch->SubmittedValue == "SEARCH")
    {
        if ($ddlUserName->SubmittedValue == 0)
        {
            $errorTitle = "ERROR";
            $errorMessage = "Please select username.";
        } else
        {

            $arrUserInfo = $mtAccounts->userInfo($ddlUserName->SubmittedValue);
            $userName = $arrUserInfo[0]['UserName'];
            $accountTypeID = $arrUserInfo[0]['AccountTypeID'];
            $dateCreated = $arrUserInfo[0]['DateCreated'];
            $status = $arrUserInfo[0]['Status'];

            $arrUserDetl = $mtAccountDetails->userDetl($ddlUserName->SubmittedValue);
            $txtFirstName->Text = $arrUserDetl[0]['FirstName'];
            $txtMiddleName->Text = $arrUserDetl[0]['MiddleName'];
            $txtLastName->Text = $arrUserDetl[0]['LastName'];
            $txtEmail->Text = $arrUserDetl[0]['Email'];
            $txtPosition->Text = $arrUserDetl[0]['Position'];
            $ddlAccType->SetSelectedValue($accountTypeID);

            $_SESSION['EditAcct'] = $ddlUserName->SubmittedValue;
            $ddlUserName->SetSelectedValue($ddlUserName->SubmittedValue);
            $_SESSION['NewlyAddedAID'] = "";
        }
    }
}

?>
