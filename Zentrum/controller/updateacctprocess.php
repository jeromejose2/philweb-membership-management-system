<?php

/*
 * Created by : Angelo Cubos
 * Date Created : April 01 2013
 * Purpose : controller updateacctprocess
 */
require_once("../../init.inc.php");

App::LoadControl("TextBox");
APP::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("Label");
App::LoadControl("ComboBox");

$moduleName = "MembershipTimor";
App::LoadModuleClass($moduleName, "MTAccounts");
App::LoadModuleClass($moduleName, "MTAccountDetails");
App::LoadModuleClass($moduleName, "MTAccountTypes");
App::LoadModuleClass($moduleName, "MTAccessRights");
App::LoadModuleClass($moduleName, "MTAuditTrail");

$mtAuditTrail = new MTAuditTrail();
$mtAccounts = new MTAccounts();
$mtAccountDetails = new MTAccountDetails();
$mtAccountTypes = new MTAccountTypes();

$fProc = new FormsProcessor();

$ddlUserName = new ComboBox("ddlUsername", "ddlUsername");
$ddlGroupUserName[] = new ListItem("", "0", true);
$ddlUserName->Items = $ddlGroupUserName;
$ddlGroupUserName = new ArrayList();
$getgroups = $mtAccounts->userList();
$ddlGroupUserName->AddArray($getgroups);
$ddlUserName->DataSource = $ddlGroupUserName;
$ddlUserName->DataSourceText = "UserName";
$ddlUserName->DataSourceValue = "AID";
$ddlUserName->DataBind();
$ddlUserName->Args = "onchange = 'javascript: onchange_username();'";
$ddlUserName->Style = "width: 280px";
$ddlUserName->Enabled = false;

$txtFirstName = new TextBox("txtFirstName", "txtFirstName");
$txtFirstName->Style = "width:200px;text-align:center;";
$txtFirstName->AutoComplete = false;
$txtFirstName->Args = "onkeypress ='javascript : return AlphaOnlyForFirstName(event);'";

$txtMiddleName = new TextBox("txtMiddleName", "txtMiddleName");
$txtMiddleName->Style = "width:200px;text-align:center;";
$txtMiddleName->AutoComplete = false;
$txtMiddleName->Args = "onkeypress ='javascript : return AlphaOnlyForMiddleName(event);'";

$txtLastName = new TextBox("txtLastName", "txtLastName");
$txtLastName->Style = "width:200px;text-align:center;";
$txtLastName->AutoComplete = false;
$txtLastName->Args = "onkeypress ='javascript : return AlphaOnlyForLastName(event);'";

$txtEmail = new TextBox("txtEmail", "txtEmail");
$txtEmail->Style = "width:200px;text-align:center;";
$txtEmail->AutoComplete = false;
$txtEmail->Args = "onkeypress='javascript: return verifyEmail(event);'";
$txtEmail->Length = "50";

$txtPosition = new TextBox("txtPosition", "txtPosition");
$txtPosition->Style = "width:200px;text-align:center;";
$txtPosition->AutoComplete = false;
$txtPosition->Args = "onkeypress ='javascript : return AlphaOnly(event);'";
$txtPosition->Length = "50";

$ddlAccType = new ComboBox("ddlAccType", "ddlAccType");
$groupList[] = new ListItem("", "0", true);
$ddlAccType->Items = $groupList;
$getall = $mtAccountTypes->getAcctTypes();
$groupList = new ArrayList();
$groupList->AddArray($getall);
$ddlAccType->DataSource = $groupList;
$ddlAccType->DataSourceText = "Name";
$ddlAccType->DataSourceValue = "AccountTypeID";
$ddlAccType->DataBind();
$ddlAccType->Style = "width: 200px; height: 20px; text-align:center; ";

$btnSave = new Button("btnSave", "btnSave", "SAVE");
$btnSave->IsSubmit = true;
$btnSave->Style = "width: 150px";
$btnSave->CssClass = "labelbutton_black";
$btnSave->Args = "onclick = 'javascript: return showData();'";

$btnBack = new Button("btnBack", "btnBack", "BACK");
$btnBack->IsSubmit = true;
$btnBack->Style = "width: 150px";
$btnBack->CssClass = "labelbutton_black";
$btnBack->Enabled = true;

$btnProceed = new Button("btnProceed", "btnProceed", "PROCEED");
$btnProceed->IsSubmit = true;
$btnProceed->Style = "width: 150px";
$btnProceed->CssClass = "labelbutton_black";

$fProc->AddControl($txtFirstName);
$fProc->AddControl($txtMiddleName);
$fProc->AddControl($txtLastName);
$fProc->AddControl($txtEmail);
$fProc->AddControl($txtPosition);
$fProc->AddControl($ddlUserName);
$fProc->AddControl($ddlAccType);
$fProc->AddControl($btnSave);
$fProc->AddControl($btnBack);
$fProc->AddControl($btnProceed);

$fProc->ProcessForms();

$success = false;
$successUpdate = false;
$confirm = false;
if ($btnSave->SubmittedValue != "SAVE")
{
    $userAccountID = "";
    if (strlen($_SESSION['EditAcct']) > 0)
    {
        $userAccountID = $_SESSION['EditAcct'];
        $ddlUserName->SetSelectedValue($userAccountID);
    }
    if (strlen($userAccountID) > 0)
    {
        $arrUserInfo = $mtAccounts->userInfo($userAccountID);
        $userName = $arrUserInfo[0]['UserName'];
        $accountTypeID = $arrUserInfo[0]['AccountTypeID'];
        $dateCreated = $arrUserInfo[0]['DateCreated'];
        $status = $arrUserInfo[0]['Status'];

        $arruserDetl = $mtAccountDetails->userDetl($userAccountID);
        $txtFirstName->Text = $arruserDetl[0]['FirstName'];
        $txtMiddleName->Text = $arruserDetl[0]['MiddleName'];
        $txtLastName->Text = $arruserDetl[0]['LastName'];
        $txtEmail->Text = $arruserDetl[0]['Email'];
        $txtPosition->Text = $arruserDetl[0]['Position'];
        $ddlAccType->SetSelectedValue($accountTypeID);
    }
}
if ($fProc->IsPostBack)
{
    if ($btnBack->SubmittedValue == "BACK")
    {
        $_SESSION['NewlyAddedAID'] = $_SESSION['EditAcct'];
        app::pr("<script>window.location.href='useraccountprofile.php';</script>");
        $_SESSION['EditAcct'] = "";
    }
    if ($btnSave->SubmittedValue == "SAVE")
    {
        $userAccountID = $_SESSION['EditAcct'];
        $ddlUserName->SetSelectedValue($userAccountID);
        $arrUserInfo = $mtAccounts->userInfo($userAccountID);
        $userName = $arrUserInfo[0]['UserName'];
        $accountTypeID = $arrUserInfo[0]['AccountTypeID'];
        $dateCreated = $arrUserInfo[0]['DateCreated'];

        $arruserDetl = $mtAccountDetails->userDetl($userAccountID);


        $firstName = ucwords($txtFirstName->SubmittedValue);
        $middleName = ucwords($txtMiddleName->SubmittedValue);
        $lastName = ucwords($txtLastName->SubmittedValue);
        $eMail = $txtEmail->SubmittedValue;
        $position = ucwords($txtPosition->SubmittedValue);
        $accessRights = $ddlAccType->SubmittedValue;

        $arrEmail = $mtAccountDetails->checkemail($eMail);
        $pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$";

        if ($firstName == NULL || $firstName == '' || strlen($firstName) == 0)
        {
            $errorTitle = "ERROR";
            $errorMessage = "Please supply the First Name.";
        } else if ($middleName == NULL || $middleName == '' || strlen($middleName) == 0)
        {
            $errorTitle = "ERROR";
            $errorMessage = "Please supply the Middle Name.";
        } else if ($lastName == NULL || $lastName == '' || strlen($lastName) == 0)
        {
            $errorTitle = "ERROR";
            $errorMessage = "Please supply the Last Name.";
        } else if ($eMail == NULL || $eMail == '' || strlen($eMail) == 0)
        {
            $errorTitle = "ERROR";
            $errorMessage = "Please supply the E-mail Address.";
        } else if (!filter_var($eMail, FILTER_VALIDATE_EMAIL) || !eregi($pattern, $eMail))
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Please supply valid Email Address.";
        } else if (count($arrEmail) > 0 && $arruserDetl[0]['Email'] != $eMail)
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "E-mail already exists.";
        } else if ($position == NULL || $position == '' || strlen($position) == 0)
        {
            $errorTitle = "ERROR";
            $errorMessage = "Please supply the Position.";
        } else if ($ddlAccType->SubmittedValue == 0)
        {
            $errorTitle = "ERROR";
            $errorMessage = "Please supply the Access Rights.";
        } else
        {
            $errorTitle = "CONFIRMATION";
            $errorMessage = "Continue to update?";
            $confirm = true;
        }
    }
    if ($btnProceed->SubmittedValue == "PROCEED")
    {
        $userAccountID = $_SESSION['EditAcct'];
        $ddlUserName->SetSelectedValue($userAccountID);
        $arrUserInfo = $mtAccounts->userInfo($userAccountID);
        $userName = $arrUserInfo[0]['UserName'];
        $accountTypeID = $arrUserInfo[0]['AccountTypeID'];
        $dateCreated = $arrUserInfo[0]['DateCreated'];

        $arruserDetl = $mtAccountDetails->userDetl($userAccountID);

        $firstName = ucwords($txtFirstName->SubmittedValue);
        $middleName = ucwords($txtMiddleName->SubmittedValue);
        $lastName = ucwords($txtLastName->SubmittedValue);
        $eMail = $txtEmail->SubmittedValue;
        $position = ucwords($txtPosition->SubmittedValue);
        $accessRights = $ddlAccType->SubmittedValue;
        $mtAccounts->StartTransaction();
        $mtAccounts->updateStatusAndAccTypeID($userAccountID, $accessRights);
        if ($mtAccounts->HasError)
        {
            $errorMsg = $mtAccounts->getError();
            $mtAccounts->RollBackTransaction();
        } else
        {
            $mtAccounts->CommitTransaction();

            $mtAccountDetails->StartTransaction();
            $mtAccountDetails->updateUserDetl($userAccountID, $firstName, 
                                $middleName, $lastName, $eMail, $position);
            if ($mtAccountDetails->HasError)
            {
                $errorMsg = $mtAccountDetails->getError();
                $mtAccountDetails->RollBackTransaction();
            } else
            {
                $mtAccountDetails->CommitTransaction();
                $successUpdate = true;
            }
        }
        if ($successUpdate == true)
        {
            $arrUserInfo = $mtAccounts->userInfo($userAccountID);
            $userName = $arrUserInfo[0]['UserName'];
            $accountTypeID = $arrUserInfo[0]['AccountTypeID'];
            $dateCreated = $arrUserInfo[0]['DateCreated'];
            $status = $arrUserInfo[0]['Status'];

            $arruserDetl = $mtAccountDetails->userDetl($userAccountID);
            $txtFirstName->Text = $arruserDetl[0]['FirstName'];
            $txtMiddleName->Text = $arruserDetl[0]['MiddleName'];
            $txtLastName->Text = $arruserDetl[0]['LastName'];
            $txtEmail->Text = $arruserDetl[0]['Email'];
            $txtPosition->Text = $arruserDetl[0]['Position'];
            $ddlAccType->SetSelectedValue($accountTypeID);

            $errorTitle = "SUCCESSFUL NOTIFICATION";
            $errorMessage = "User profile successfully updated.";
            $success = true;
            $_SESSION['NewlyAddedAID'] = $userAccountID;

            // Log to Audit Trail
            $mtAuditTrail->StartTransaction();
            $mtAuditLogParam['SessionID'] = $_SESSION['sid'];
            $mtAuditLogParam['AID'] = $_SESSION['aid'];
            $mtAuditLogParam['AuditTrailFunctionID'] = '8';
            $mtAuditLogParam['TransDetails'] = "Update Account Profile: " . $userName;
            $mtAuditLogParam['RemoteIP'] = $_SERVER['REMOTE_ADDR'];
            $mtAuditLogParam['TransDateTime'] = "now_usec()";

            $mtAuditTrail->Insert($mtAuditLogParam);
            if ($mtAuditTrail->HasError)
            {
                $errorMsg = $mtAuditTrail->getError();
                $mtAuditTrail->RollBackTransaction();
            } else
            {
                $mtAuditTrail->CommitTransaction();
            }
        }
    }
}

?>
