<?php

/*
 * Added By : Frances Ralph DL. Sison
 * Added On : April 01, 2013
 * Purpose : Filtering and Viewing of Membership Accounts
 */

require_once("../../init.inc.php");

$pagesubmenuid = 15;

App::LoadControl("TextBox");
APP::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("Label");
App::LoadControl("ComboBox");
App::LoadControl("PagingControl2");

$modulename = "MembershipTimor";

App::LoadModuleClass($modulename, "MTMemberInfo");
App::LoadModuleClass($modulename, "MTAuditTrail");

$mtMemberInfo = new MTMemberInfo();
$fproc = new FormsProcessor();

$filterSearch = new ComboBox("filterSearch", "filterSearch", "FILTER SEARCH");
$filterSearchGroup[] = new ListItem("Select One", "0", true);
$filterSearchGroup[] = new ListItem("Name", "1");
$filterSearchGroup[] = new ListItem("MembershipCardNo", "2");
//$filterSearchGroup[] = new ListItem("Status", "Status");
$filterSearch->Items = $filterSearchGroup;
$filterSearchGroup = new ArrayList();
$getGroups = $mtMemberInfo->getFilterSearchGroup();
$filterSearchGroup->AddArray($getGroups);
$filterSearch->DataSource = $filterSearchGroup;
//$filterSearch->Args = "onchange='javascript: acceptSpace();'";

$searchField = new TextBox("searchField", "searchField");
$searchField->Length = 30;
$searchField->Args = "autocomplete='off'";

$btnSearch = new Button("btnSearch", "btnSearch", "SEARCH");
$btnSearch->IsSubmit = true;
$btnSearch->CssClass = "labelbutton_black";
$btnSearch->Args = "onclick='javascript: return checkvalidation();'";

$itemsPerPage = 50;
$pgcon = new PagingControl2($itemsPerPage, 1);
$pgcon->URL = "javascript:ChangePage(%currentpage);";
$pgcon->ShowMoveToFirstPage = true;
$pgcon->ShowMoveToLastPage = true;

$btnDownload = new Button("btnDownload", "btnDownload", "Export to CSV");
$btnDownload->IsSubmit = true;
$btnDownload->CssClass = "labelbutton_black";

$hidMemberInfoID = new Hidden("hidMemberInfoID", "hidMemberInfoID");

$ddlMemberStatus = new ComboBox("ddlMemberStatus", "ddlMemberStatus");
$ddlMemberStatusGroup[] = new ListItem("Inactive", "0");
$ddlMemberStatusGroup[] = new ListItem("Active", "1");
$ddlMemberStatusGroup[] = new ListItem("Suspended", "2");
$ddlMemberStatusGroup[] = new ListItem("Banned", "3");
$ddlMemberStatusGroup[] = new ListItem("All", "4", true);
$ddlMemberStatus->Items = $ddlMemberStatusGroup;
$ddlMemberStatusGroup = new ArrayList();
$ddlMemberStatus->DataSource = $ddlMemberStatusGroup;

$display = true;

$fproc->AddControl($filterSearch);
$fproc->AddControl($searchField);
$fproc->AddControl($btnSearch);
$fproc->AddControl($btnDownload);
$fproc->AddControl($hidMemberInfoID);
$fproc->AddControl($ddlMemberStatus);

$userType = $_SESSION['accttype'];

$fproc->ProcessForms();

//display member info for the initial loading of the caralog page
$getAll = $mtMemberInfo->getMembershipAcctDtls();
$getAllCount = count($getAll);
$pgcon->Initialize($itemsPerPage, $getAllCount);
$arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsWithLimit(($pgcon->SelectedItemFrom - 1), $itemsPerPage);
$filterAndViewList = new ArrayList();
$filterAndViewList->AddArray($arrayGetAll);

if($fproc->IsPostBack)
{
    
    if ($btnSearch->SubmittedValue == "SEARCH" || $fproc->GetPostVar('pgSelectedPage') != '')
    {
        if ($btnSearch->SubmittedValue == "SEARCH")
        {
            $pgcon->SelectedItemFrom = 1;
            $pgcon->SelectedPage = 1;
        }
        
//        if ($ddlMemberStatus->SubmittedValue == 4 && $filterSearch->SubmittedValue == 0)
//        {
//            $display = false;
//            $errorTitle = "ERROR";
//            $errorMessage = "No search by filter selected.";
//            
////            if ($searchField->SubmittedValue == "")
////            {                
////                //same as that of the initial loading of the catalog page
////                $getAll = $mtMemberInfo->getMembershipAcctDtls();
////                $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsWithLimit(($pgcon->SelectedItemFrom - 1), $itemsPerPage);
////                $searchFieldData = $_POST['searchField'];
////                $filterSearchData = $_POST['filterSearch'];
////                
////            }
////            else
////            {
////                $selection = $searchField->SubmittedValue;
////                if(is_numeric($selection))
////                {
////                    $selection = ltrim(strtolower($selection),"%a");
////                    $selection = rtrim(strtolower($selection),"?");
////                    if(strlen($selection) == 5)
////                    {
////                        //get member info based on either given name or member card number
////                        $getAll = $mtMemberInfo->getMembershipAcctDtlsWithLastNameorMCN($selection);
////                        $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsWithLastNameorMCNWithLimit($selection, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);
////                    }
////                    else
////                    {
////                        $display = false;
////                        $errorTitle = "ERROR";
////                        $errorMessage = "Invalid Card Number.";
////                    }
////                }
////                else
////                {
////                    //get member info based on either given name or member card number
////                    $getAll = $mtMemberInfo->getMembershipAcctDtlsWithLastNameorMCN($selection);
////                    $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsWithLastNameorMCNWithLimit($selection, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);
////                }
////               
////            }
//        }
        if($ddlMemberStatus->SubmittedValue == 4 && $filterSearch->SubmittedValue == 1)
        {
            if($searchField->SubmittedValue != "")
            {
                $name = $searchField->SubmittedValue;
                $name = strtolower($name);
                //display member info based on given name
                $getAll = $mtMemberInfo->getMembershipAcctDtlsWithGivenName($name);
                $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsWithGivenNameWithLimit($name, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);                                
            }
//            else
//            {
//                $display = false;
//                $errorTitle = "ERROR";
//                $errorMessage = "No filter inputted.";
//            }
        }
        else if($ddlMemberStatus->SubmittedValue == 4 && $filterSearch->SubmittedValue == 2)
        {
            if($searchField->SubmittedValue != "")
            {
                $membershipCardNo = $searchField->SubmittedValue;
                if(strlen($membershipCardNo) == 6 && ($membershipCardNo[0] == 'A' || $membershipCardNo[0] == 'a'))
                {
                    $membershipCardNo = ltrim(strtolower($membershipCardNo),"%a");
                    $membershipCardNo = rtrim(strtolower($membershipCardNo),"?");
                    //display member info based on given member card number
                    $getAll = $mtMemberInfo->getMembershipAcctDtlsWithGivenMCN($membershipCardNo);
                    $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsWithGivenMCNWithLimit($membershipCardNo, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);
                }
//                else
//                {
//                    $display = false;
//                    $errorTitle = "ERROR";
//                    $errorMessage = "Invalid Card Number.";
//                }
                
                
                
            }
//            else
//            {
//                $display = false;
//                $errorTitle = "ERROR";
//                $errorMessage = "No filter inputted.";
//            }
        }
//        else if($ddlMemberStatus->SubmittedValue == 0 && $filterSearch->SubmittedValue == 0)
//        {
//            $display = false;
//            $errorTitle = "ERROR";
//            $errorMessage = "No search by filter selected.";
////            if($searchField->SubmittedValue == "")
////            {
////                //same as that of the initial loading of the catalog page
////                $getAll = $mtMemberInfo->getMembershipAcctDtlsOfInactive();
////                $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfInactiveWithLimit(($pgcon->SelectedItemFrom - 1), $itemsPerPage);
////
////                $searchFieldData = $_POST['searchField'];
////                $filterSearchData = $_POST['filterSearch'];
////                
////            }
////            else
////            {
////                $selection = $searchField->SubmittedValue;
////                if(is_numeric($selection))
////                {
////                    $selection = ltrim(strtolower($selection),"%a");
////                    $selection = rtrim(strtolower($selection),"?");
////                
////                    if(strlen($selection) == 5)
////                    {
////                        //get member info based on either given name or member card number
////                        $getAll = $mtMemberInfo->getMembershipAcctDtlsOfInactiveWithLastNameorMCN($selection);
////                        $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfInactiveWithLastNameorMCNWithLimit($selection, ($pgcon->SelectedItemFrom - 1), $itemsPerPage); 
////                    }
////                    else
////                    {
////                        $display = false;
////                        $errorTitle = "ERROR";
////                        $errorMessage = "Invalid Card Number.";
////                    }
////                }
////                else
////                {
////                    //get member info based on either given name or member card number
////                    $getAll = $mtMemberInfo->getMembershipAcctDtlsOfInactiveWithLastNameorMCN($selection);
////                    $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfInactiveWithLastNameorMCNWithLimit($selection, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);
////                }
////                
////                                
////            }
//        }
        else if($ddlMemberStatus->SubmittedValue == 0 && $filterSearch->SubmittedValue == 1)
        {
            if($searchField->SubmittedValue != "")
            {
                $name = $searchField->SubmittedValue;
                $name = strtolower($name);
                //display member info based on given name
                $getAll = $mtMemberInfo->getMembershipAcctDtlsOfInactiveWithGivenName($name);
                $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfInactiveWithGivenNameWithLimit($name, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);         
            }
//            else
//            {
//                $display = false;
//                $errorTitle = "ERROR";
//                $errorMessage = "No filter inputted.";
//            }
        }
        else if($ddlMemberStatus->SubmittedValue == 0 && $filterSearch->SubmittedValue == 2)
        {
            if($searchField->SubmittedValue != "")
            {
                $membershipCardNo = $searchField->SubmittedValue;
                if(strlen($membershipCardNo) == 6 && ($membershipCardNo[0] == 'A' || $membershipCardNo[0] == 'a'))
                {
                    $membershipCardNo = ltrim(strtolower($membershipCardNo),"%a");
                    $membershipCardNo = rtrim(strtolower($membershipCardNo),"?");
                    //display member info based on given member card number
                    $getAll = $mtMemberInfo->getMembershipAcctDtlsOfInactiveWithGivenMCN($membershipCardNo);
                    $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfInactiveWithGivenMCNWithLimit($membershipCardNo, ($pgcon->SelectedItemFrom - 1), $itemsPerPage); 
                }
//                else
//                {
//                    $display = false;
//                    $errorTitle = "ERROR";
//                    $errorMessage = "Invalid Card Number.";
//                }             
                        
            }
//            else
//            {
//                $display = false;
//                $errorTitle = "ERROR";
//                $errorMessage = "No filter inputted.";
//            }
        }
//        else if($ddlMemberStatus->SubmittedValue == 1 && $filterSearch->SubmittedValue == 0)
//        {
//            $display = false;
//            $errorTitle = "ERROR";
//            $errorMessage = "No search by filter selected.";
////            if($searchField->SubmittedValue == "")
////            {
////                //same as that of the initial loading of the catalog page
////                $getAll = $mtMemberInfo->getMembershipAcctDtlsOfActive();
////                $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfActiveWithLimit(($pgcon->SelectedItemFrom - 1), $itemsPerPage);
////
////                $searchFieldData = $_POST['searchField'];
////                $filterSearchData = $_POST['filterSearch'];
////                
////            }
////            else
////            {
////                $selection = $searchField->SubmittedValue;
////                $selection = ltrim(strtolower($selection),"%a");
////                $selection = rtrim(strtolower($selection),"?");
////                
////                if(strlen($selection) == 5)
////                {
////                    //get member info based on either given name or member card number
////                    $getAll = $mtMemberInfo->getMembershipAcctDtlsOfActiveWithLastNameorMCN($selection);
////                    $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfActiveWithLastNameorMCNWithLimit($selection, ($pgcon->SelectedItemFrom - 1), $itemsPerPage); 
////                }
////                else
////                {
////                    $display = false;
////                    $errorTitle = "ERROR";
////                    $errorMessage = "Invalid Card Number.";
////                }
////                               
////            }
//        }
        else if($ddlMemberStatus->SubmittedValue == 1 && $filterSearch->SubmittedValue == 1)
        {
            if($searchField->SubmittedValue != "")
            {
                $name = $searchField->SubmittedValue;
                $name = strtolower($name);
                //display member info based on given name
                $getAll = $mtMemberInfo->getMembershipAcctDtlsOfActiveWithGivenName($name);
                $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfActiveWithGivenNameWithLimit($name, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);         
            }
//            else
//            {
//                $display = false;
//                $errorTitle = "ERROR";
//                $errorMessage = "No filter inputted.";
//            }
        }
        else if($ddlMemberStatus->SubmittedValue == 1 && $filterSearch->SubmittedValue == 2)
        {
            if($searchField->SubmittedValue != "")
            {
                $membershipCardNo = $searchField->SubmittedValue;
                if(strlen($membershipCardNo) == 6 && ($membershipCardNo[0] == 'A' || $membershipCardNo[0] == 'a'))
                {
                    $membershipCardNo = ltrim(strtolower($membershipCardNo),"%a");
                    $membershipCardNo = rtrim(strtolower($membershipCardNo),"?");
                    //display member info based on given member card number
                    $getAll = $mtMemberInfo->getMembershipAcctDtlsOfActiveWithGivenMCN($membershipCardNo);
                    $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfActiveWithGivenMCNWithLimit($membershipCardNo, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);
                }
//                else
//                {
//                    $display = false;
//                    $errorTitle = "ERROR";
//                    $errorMessage = "Invalid Card Number.";
//                }
                
                
                
                         
            }
//            else
//            {
//                $display = false;
//                $errorTitle = "ERROR";
//                $errorMessage = "No filter inputted.";
//            }
        }
//        else if($ddlMemberStatus->SubmittedValue == 2 && $filterSearch->SubmittedValue == 0)
//        {
//            $display = false;
//            $errorTitle = "ERROR";
//            $errorMessage = "No search by filter selected.";
////            if($searchField->SubmittedValue == "")
////            {
////                //same as that of the initial loading of the catalog page
////                $getAll = $mtMemberInfo->getMembershipAcctDtlsOfSuspended();
////                $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfSuspendedWithLimit(($pgcon->SelectedItemFrom - 1), $itemsPerPage);
////
////                $searchFieldData = $_POST['searchField'];
////                $filterSearchData = $_POST['filterSearch'];
////                
////            }
////            else
////            {
////                $selection = $searchField->SubmittedValue;
////                $selection = ltrim(strtolower($selection),"%a");
////                $selection = rtrim(strtolower($selection),"?a");
////                
////                if(strlen($selection) == 5)
////                {
////                    //get member info based on either given name or member card number
////                    $getAll = $mtMemberInfo->getMembershipAcctDtlsOfSuspendedWithLastNameorMCN($selection);
////                    $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfSuspendedWithLastNameorMCNWithLimit($selection, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);
////                }
////                else
////                {
////                    $display = false;
////                    $errorTitle = "ERROR";
////                    $errorMessage = "Invalid Card Number.";
////                }
////                                
////            }
//        }
        else if($ddlMemberStatus->SubmittedValue == 2 && $filterSearch->SubmittedValue == 1)
        {
            if($searchField->SubmittedValue != "")
            {
                $name = $searchField->SubmittedValue;
                $name = strtolower($name);
                //display member info based on given name
                $getAll = $mtMemberInfo->getMembershipAcctDtlsOfSuspendedWithGivenName($name);
                $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfSuspendedWithGivenNameWithLimit($name, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);         
            }
//            else
//            {
//                $display = false;
//                $errorTitle = "ERROR";
//                $errorMessage = "No filter inputted.";
//            }
        }
        else if($ddlMemberStatus->SubmittedValue == 2 && $filterSearch->SubmittedValue == 2)
        {
            if($searchField->SubmittedValue != "")
            {
                $membershipCardNo = $searchField->SubmittedValue;
                if(strlen($membershipCardNo) == 6 && ($membershipCardNo[0] == 'A' || $membershipCardNo[0] == 'a'))
                {
                    $membershipCardNo = ltrim(strtolower($membershipCardNo),"%a");
                    $membershipCardNo = rtrim(strtolower($membershipCardNo),"?");
                    //display member info based on given member card number
                    $getAll = $mtMemberInfo->getMembershipAcctDtlsOfSuspendedWithGivenMCN($membershipCardNo);
                    $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfSuspendedWithGivenMCNWithLimit($membershipCardNo, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);
                }
//                else
//                {
//                    $display = false;
//                    $errorTitle = "ERROR";
//                    $errorMessage = "Invalid Card Number.";
//                }        
                         
            }
//            else
//            {
//                $display = false;
//                $errorTitle = "ERROR";
//                $errorMessage = "No filter inputted.";
//            }
        }
//        else if($ddlMemberStatus->SubmittedValue == 3 && $filterSearch->SubmittedValue == 0)
//        {
//            $display = false;
//            $errorTitle = "ERROR";
//            $errorMessage = "No search by filter selected.";
////            if($searchField->SubmittedValue == "")
////            {
////                //same as that of the initial loading of the catalog page
////                $getAll = $mtMemberInfo->getMembershipAcctDtlsOfBanned();
////                $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfBannedWithLimit(($pgcon->SelectedItemFrom - 1), $itemsPerPage);
////
////                $searchFieldData = $_POST['searchField'];
////                $filterSearchData = $_POST['filterSearch'];
////                
////            }
////            else
////            {
////                $selection = $searchField->SubmittedValue;
////                $selection = ltrim(strtolower($selection),"%a");
////                $selection = rtrim(strtolower($selection),"?");
////                
////                if(strlen($selection) == 5)
////                {
////                    //get member info based on either given name or member card number
////                    $getAll = $mtMemberInfo->getMembershipAcctDtlsOfBannedWithLastNameorMCN($selection);
////                    $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfBannedWithLastNameorMCNWithLimit($selection, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);
////                }
////                else
////                {
////                    $display = false;
////                    $errorTitle = "ERROR";
////                    $errorMessage = "Invalid Card Number.";
////                }
////                                
////            }
//        }
        else if($ddlMemberStatus->SubmittedValue == 3 && $filterSearch->SubmittedValue == 1)
        {
            if($searchField->SubmittedValue != "")
            {
                $name = $searchField->SubmittedValue;
                $name = strtolower($name);
                //display member info based on given name
                $getAll = $mtMemberInfo->getMembershipAcctDtlsOfBannedWithGivenName($name);
                $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfBannedWithGivenNameWithLimit($name, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);         
            }
//            else
//            {
//                $display = false;
//                $errorTitle = "ERROR";
//                $errorMessage = "No filter inputted.";
//            }
        }
        else if($ddlMemberStatus->SubmittedValue == 3 && $filterSearch->SubmittedValue == 2)
        {
            if($searchField->SubmittedValue != "")
            {
                $membershipCardNo = $searchField->SubmittedValue;
                if(strlen($membershipCardNo) == 6 && ($membershipCardNo[0] == 'A' || $membershipCardNo[0] == 'a'))
                {
                    $membershipCardNo = ltrim(strtolower($membershipCardNo),"%a");
                    $membershipCardNo = rtrim(strtolower($membershipCardNo),"?");
                    //display member info based on given member card number
                    $getAll = $mtMemberInfo->getMembershipAcctDtlsOfBannedWithGivenMCN($membershipCardNo);
                    $arrayGetAll = $mtMemberInfo->getMembershipAcctDtlsOfBannedWithGivenMCNWithLimit($membershipCardNo, ($pgcon->SelectedItemFrom - 1), $itemsPerPage);
                }
//                else
//                {
//                    $display = false;
//                    $errorTitle = "ERROR";
//                    $errorMessage = "Invalid Card Number.";
//                }         
                         
            }
//            else
//            {
//                $display = false;
//                $errorTitle = "ERROR";
//                $errorMessage = "No filter inputted.";
//            }
        }
    }

    if($btnDownload->SubmittedValue == "Export to CSV")
    {       
            header('Content-type:text/csv');
            header("Content-Disposition: attachment; filename="."Membership_Catalog.csv");
            header('Pragma:public');
            readfile('../csv/Membership_Catalog.csv');
            exit;
    }
}
        
$getAllCount = count($getAll);
$pgcon->Initialize($itemsPerPage, $getAllCount);
$filterAndViewList = new ArrayList();
$filterAndViewList->AddArray($arrayGetAll);
$pgTransactionHistory = $pgcon->PreRender();

$csvData = array();
for($i = 0;$i < count($getAll);$i++)
{
        $membershipCardNo = $getAll[$i]["CardNumber"];
        if($getAll[$i]["Status"] == 1)
        {
            $membershipCardNo = sprintf("%05d", $getAll[$i]["CardNumber"]);
            $membershipCardNo = 'A'.$membershipCardNo;
        }
        else
        {
            $membershipCardNo = $getAll[$i]["MemberInfoID"];
        }
        $lastName = $getAll[$i]["LastName"];
        $firstName = $getAll[$i]["FirstName"];
        $dateTime = new DateTime($getAll[$i]["Birthdate"]);
        $birthDate = $dateTime->format('m-d-Y');
        $sex = $getAll[$i]['Gender'];

        if($sex == 1)
        {
            $sex = "Male";
        }
        else if($sex == 2)
        {
            $sex = "Female";
        }

        $dateTime2 = new DateTime($getAll[$i]["DateCreated"]);
        $membershipDate = $dateTime2->format('m-d-Y');
        $status = $getAll[$i]["Status"];

        if($status == 0)
        {
            $status = "Inactive";
        }
        else if($status == 1)
        {
            $status = "Active";
        }
        else if($status == 2)
        {
            $status = "Suspended";
        }
        else if($status == 3)
        {
            $status = "Banned";
        }

        $appTool = $getAll[$i]["RegistrationOrigin"];

        if($appTool == 2)
        {
            $appTool = "Online";
        }
        else if($appTool == 1)
        {
            $appTool = "Onsite";
        }
        
        $address = $getAll[$i]["Address"];
        
        $contactnumber = $getAll[$i]["ContactNumber"];
        
        $email = $getAll[$i]["Email"];
        
        $idpresented = $getAll[$i]["IdentificationPresented"];
        
        if($idpresented == 1)
        {
            $idpresented = "Driver's License";
        }
        else if($idpresented == 2)
        {
            $idpresented = "Electoral ID";
        }
        else if($idpresented == 3)
        {
            $idpresented = "Passport";
        }
        else if($idpresented == 4)
        {
            $idpresented = "Others";
        }
        
        $idnumber = $getAll[$i]["IdentificationNumber"];

        $csvData[] = $membershipCardNo.",".$lastName.",".$firstName.",".$birthDate.",".$sex.",".$membershipDate.",".$status.",".$appTool.",".$address.",".$contactnumber.",".$email.",".$idpresented.",".$idnumber."\r\n";
    }
    
    $fp = fopen("../csv/Membership_Catalog.csv", "w");
    if ($fp)
    {
        $header = "Membership Card No,Last Name,First Name,Birthdate,Gender,Membership Date,Status,Application Tool,Address,Contact Number,E-mail Address,ID Presented,ID Number" . "\r\n";
        fwrite($fp, $header);
        if($csvData)
        {
            
            foreach($csvData as $rc)
            {
                if(count($rc) > 0)
                {
                    fwrite($fp, $rc);
                }
            }
        }
        else
        {
            $rc = "\nNo Records Found;\n";
            fwrite($fp, $rc);
        }

        fclose($fp);
    }
    else
    {
        echo "<script>alert('Cannot write file.');</script>";
    }
?>
