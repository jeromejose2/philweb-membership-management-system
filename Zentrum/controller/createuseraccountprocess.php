<?php

/*
 * Created by : Angelo Cubos
 * Date Created : April 01 2013
 * Purpose : controller create user account
 */
require_once("../../init.inc.php");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadCore("PHPMailer.class.php");
App::LoadCore("DateSelector.class.php");

$moduleName = "MembershipTimor";

App::LoadModuleClass($moduleName, "MTAccounts");
App::LoadModuleClass($moduleName, "MTAccountDetails");
App::LoadModuleClass($moduleName, "MTAccountTypes");
App::LoadModuleClass($moduleName, "MTAuditTrail");

$mtAccounts = new MTAccounts();
$mtAccountDetails = new MTAccountDetails();
$mtAccountTypes = new MTAccountTypes();
$mtAuditTrail = new MTAuditTrail();

$fProc = new FormsProcessor();

$txtUserName = new TextBox("txtUserName", "txtUserName", "Username");
$txtUserName->Length = 12;
$txtUserName->Enabled = false;
$txtUserName->AutoComplete = false;
$txtUserName->Args = "onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtFirstName = new TextBox("txtFirstName", "txtFirstName");
$txtFirstName->Length = 30;
$txtFirstName->AutoComplete = false;
$txtFirstName->Args = "onblur='filluname();' onkeypress ='javascript : return AlphaOnlyForFirstName(event);'";

$txtMiddleName = new TextBox("txtMiddleName", "txtMiddleName");
$txtMiddleName->Length = 30;
$txtMiddleName->AutoComplete = false;
$txtMiddleName->Args = "onblur='filluname();' onkeypress ='javascript : return AlphaOnlyForMiddleName(event);'";

$txtLastName = new TextBox("txtLastName", "txtLastName");
$txtLastName->Length = 30;
$txtLastName->AutoComplete = false;
$txtLastName->Args = "onblur='filluname();' onkeypress ='javascript : return AlphaOnlyForLastName(event);'";

$txtEmail = new TextBox("txtEmail", "txtEmail");
$txtEmail->Length = 50;
$txtEmail->AutoComplete = false;
$txtEmail->Args = "onkeypress='javascript: return verifyEmail(event);'";

$txtPosition = new TextBox("txtPosition", "txtPosition");
$txtPosition->Length = "50";
$txtPosition->AutoComplete = false;
$txtPosition->Args = "onkeypress ='javascript : return AlphaOnly(event);'";

$ddlAccType = new ComboBox("ddlAccType", "ddlAccType");
$getAll = $mtAccountTypes->getAcctTypes();
$groupList = new ArrayList();
$groupList[] = new ListItem("", "0", true);
$groupList->AddArray($getAll);
$ddlAccType->DataSource = $groupList;
$ddlAccType->DataSourceText = "Name";
$ddlAccType->DataSourceValue = "AccountTypeID";
$ddlAccType->DataBind();
$ddlAccType->Style = "width: 300px";

$btnCancel = new Button("btnCancel", "btnCancel", "CANCEL");
$btnCancel->IsSubmit = true;
$btnCancel->CssClass = "labelbutton_black";

$btnSubmit = new Button("btnSubmit", "btnSubmit", "SUBMIT");
$btnSubmit->IsSubmit = true;
$btnSubmit->CssClass = "labelbutton_black";
$btnSubmit->Args = "onclick='javascript: return checkadduseracct();'";

$btnProceed = new Button("btnProceed", "btnProceed", "PROCEED");
$btnProceed->IsSubmit = true;
$btnProceed->CssClass = "labelbutton_black";

$argsHide = "onclick=document.getElementById('light1').style.display='none';";
$argsHide.= "document.getElementById('fade').style.display='none';";
$btnHideMsg = new Button("btnHideMsg", "btnHideMsg", "OKAY");
$btnHideMsg->Args = "$argsHide";
$btnHideMsg->CssClass = "labelbutton_black";

$hiddenUname = new Hidden("hiddenUname","hiddenUname");
        
$fProc->AddControl($hiddenUname);
$fProc->AddControl($txtUserName);
$fProc->AddControl($txtFirstName);
$fProc->AddControl($txtMiddleName);
$fProc->AddControl($txtLastName);
$fProc->AddControl($txtEmail);
$fProc->AddControl($txtPosition);
$fProc->AddControl($ddlAccType);
$fProc->AddControl($btnSubmit);
$fProc->AddControl($btnCancel);
$fProc->AddControl($btnProceed);
$fProc->AddControl($btnHideMsg);

$fProc->ProcessForms(); 

if ($fProc->IsPostBack)
{
    $txtUserName->Text = $hiddenUname->SubmittedValue;
    $firstName = ucwords($txtFirstName->SubmittedValue);
    $middleName = ucwords($txtMiddleName->SubmittedValue);
    $lastName = ucwords($txtLastName->SubmittedValue);
    $eMail = $txtEmail->SubmittedValue;
    $accountPosition = ucwords($txtPosition->SubmittedValue);
    $accountType = $ddlAccType->SubmittedValue;

    $sendEmail = false;
    $_SESSION['NewlyAddedAID'] = "";
    if ($btnCancel->SubmittedValue == 'CANCEL')
    {
        app::pr("<script>window.location.href='createuseraccount.php';</script>");
    }
    if ($btnSubmit->SubmittedValue == 'SUBMIT')
    {
        $tmpUser = substr($firstName, 0, 1) . substr($middleName, 0, 1) . $lastName;
        $tmpUserName = str_replace(' ','',strtolower($tmpUser));
        $userArray = $mtAccounts->getUserName($tmpUserName);
        $pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$";
        $_SESSION['tmpusername'] = $tmpUserName;
        if (($firstName == NULL || $firstName == '' || strlen($firstName) == 0) &&
            ($middleName == NULL || $middleName == '' || strlen($middleName) == 0) &&
            ($lastName == NULL || $lastName == '' || strlen($lastName) == 0) &&
            ($eMail == NULL || $eMail == '' || strlen($eMail) == 0) &&
            ($accountPosition == NULL || $accountPosition == '' 
            || strlen($accountPosition) == 0) && ($accountType == 0))
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Please fill in all fields.";
        }
        else if ($firstName == NULL || $firstName == '' || strlen($firstName) == 0)
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Please supply the First Name.";
        }
        else if ($middleName == NULL || $middleName == '' || strlen($middleName) == 0)
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Please supply the Middle Name.";
        }
        else if ($lastName == NULL || $lastName == '' || strlen($lastName) == 0)
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Please supply the Last Name.";
        }
        else if ($eMail == NULL || $eMail == '' || strlen($eMail) == 0)
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Please supply the Email Address.";
        }
        else if (!filter_var($eMail, FILTER_VALIDATE_EMAIL)||!eregi($pattern, $eMail))
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Please supply valid Email Address.";
        }
        else if ($accountPosition == NULL || $accountPosition == '' 
                || strlen($accountPosition) == 0)
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Please supply the Position.";
        }
        else if ($accountType == 0)
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Please supply the Account Type.";
        }
        else if (count($userArray) > 0)
        {
            $errorTitle = "INVALID INFORMATION";
            $errorMessage = "Username already exists.";
        }
        else
        {
            $arremail = $mtAccountDetails->checkEmail($eMail);
            if (count($arremail) == 0)
            {
                $confirmTitle = "CONFIRMATION";
                $confirmMessage = "Proceed with the account creation?";
            }
            else
            {
                $errorTitle = "INVALID INFORMATION";
                $errorMessage = "E-mail already exists.";
            }
        }
    }
    if ($btnProceed->SubmittedValue == 'PROCEED')
    {
        $tmpPassword = substr(session_id(), 0, 8);
        $mtAccounts->StartTransaction();
        $arrAccount['UserName'] = $_SESSION['tmpusername'];
        $arrAccount['Password'] = md5($tmpPassword);
        $arrAccount['AccountTypeID'] = $accountType;
        $arrAccount['DateCreated'] = "now_usec()";
        $arrAccount['Status'] = 1;
        $mtAccounts->Insert($arrAccount);
        if ($mtAccounts->HasError)
        {
            $returnMsg = $mtAccounts->getError();
            $mtAccounts->RollBackTransaction();
        }
        else
        {
            $mtAccounts->CommitTransaction();

            $arrLastID = $mtAccounts->LastAID();
            $lastID = $arrLastID[0]['LastID'];
            $mtAccountDetails->StartTransaction();
            $mtAccountDetails->updateByAID($lastID, $firstName, $middleName, 
                                           $lastName, $eMail, $accountPosition);
            if ($mtAccountDetails->HasError)
            {
                $returnMsg = $mtAccountDetails->getError();
                $mtAccountDetails->RollBackTransaction();
            }
            else
            {
                $mtAccountDetails->CommitTransaction();
                $sendEmail = true;
            }
            // Log to Audit Trail
            $tmpUname = $_SESSION['tmpusername'];
            $mtAuditTrail->StartTransaction();
            $mtAuditLogParam['SessionID'] = $_SESSION['sid'];
            $mtAuditLogParam['AID'] = $_SESSION['aid'];
            $mtAuditLogParam['AuditTrailFunctionID'] = '7';
            $mtAuditLogParam['TransDetails'] = "Created new account: " . $tmpUname;
            $mtAuditLogParam['RemoteIP'] = $_SERVER['REMOTE_ADDR'];
            $mtAuditLogParam['TransDateTime'] = "now_usec()";

            $mtAuditTrail->Insert($mtAuditLogParam);
            if ($mtAuditTrail->HasError)
            {
                $returnMsg = $mtAuditTrail->getError();
                $mtAuditTrail->RollBackTransaction();
            }
            else
            {
                $mtAuditTrail->CommitTransaction();
            }
        }
    }

    if ($sendEmail == true)
    {
        // add emailing
        $pm = new PHPMailer();
        $pm->AddAddress($eMail, $firstName);
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on")
        {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        $folder = $_SERVER["REQUEST_URI"];
        $folder = substr($folder, 0, strrpos($folder, '/') + 1);
        if ($_SERVER["SERVER_PORT"] != "80")
        {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $folder;
        }
        else
        {
            $pageURL .= $_SERVER["SERVER_NAME"] . $folder;
        }
        $tmpUserName = $_SESSION['tmpusername'];
//        $date = date('m/d/Y');
//        $time = date('h:i:s');
        $ds = new DateSelector();
        $ds->SetTimeZone("Asia/Dili");
        $date = Date('m/d/Y',strtotime($ds->CurrentDate));
        $time = $ds->GetCurrentDateFormat("H:i:s");        
        
        $pm->IsHTML(true);
        $pm->Body = "<HTML><BODY>						
                  Dear $firstName $middleName $lastName,
				  <br /><br />
				  This is to inform you that your User Account has been successfully 
                  created on this date $date and time $time.
				  Here are your Account Details:
					<br /><br />
					Username: <b>$tmpUserName</b><br />
					Password: <b>$tmpPassword</b>	
			      <br /><br />
			      For security purposes, please change this assigned password 
                  upon log in at $pageURL.																		
				  <br /><br />
				  For inquiries on your account, please contact our 
                  Customer Service email at membership@egames.com.tl.
				  <br /><br />
				  Regards,
				  <br /><br />
				  e-Games Management
				</BODY></HTML>";
        $pm->From = "membership@egames.com.tl";
        $pm->FromName = "e-Games Management";
        $pm->Host = "localhost";
        $pm->Subject = "ZENTRUM MEMBERSHIP CATALOG NOTIFICATION FOR NEW USER ACCOUNT";
        $email_sent = $pm->Send();
        $_SESSION['title'] = "Successful Account Creation";
        if ($email_sent)
        {
        $errorTitle = "SUCCESSFUL ACCOUNT CREATION";
        $errorMessage = "New user account has been successfully created. Log in ";
        $errorMessage.= "credentials have been sent to the registered e-mail address.";
        $_SESSION['NewlyAddedAID'] = $lastID;
        }
        else
        {
        $errorTitle = "ERROR";
        $errorMessage = "An error occurred while sending the email to your email address.";
        }
    }
}
?>