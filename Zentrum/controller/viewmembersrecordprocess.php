<?php

/*
 * Added By : Frances Ralph DL. Sison
 * Added On : April 01, 2013
 * Purpose : Edit Member's Profile Controller
 */
require_once("../../init.inc.php");

App::LoadControl("TextBox");
App::LoadControl("Button");
App::LoadControl("ComboBox");
App::LoadControl("Hidden");

$modulename = "MembershipTimor";

App::LoadModuleClass($modulename,"MTMemberInfo");
App::LoadModuleClass($modulename,"MTRefCountries");
App::LoadModuleClass($modulename,"MTRefIdentification");

$memberInfo = new MTMemberInfo();
$refCountries = new MTRefCountries();
$refIdentification = new MTRefIdentification();
$fproc = new FormsProcessor();


$btnEdit = new Button("btnEdit","btnEdit","EDIT");
$btnEdit->IsSubmit = true;
$btnEdit->CssClass = "labelbutton_black";

$hiddenPostedId = new Hidden("hiddenPostedId","hiddenPostedId","Hidden Posted Id");
$hiddenId = new Hidden("hiddenId","hiddenId");

$fproc->AddControl($btnEdit);
$fproc->AddControl($hiddenPostedId);
$fproc->AddControl($hiddenId);

$fproc->ProcessForms();

if($fproc->IsPostBack)
{
    if($btnEdit->SubmittedValue == "EDIT")
    {
        $page = 'editmembersrecords.php';
        App::pr("<script> window.location = '$page'; </script>");
    }
    
    if($fproc->GetPostVar("hidMemberInfoID"))
    {
        $selectedMemberId = $fproc->GetPostVar("hidMemberInfoID");
        $hiddenPostedId->Text = $selectedMemberId;
    }
    
    $id = $hiddenPostedId->Text;
    $_SESSION['hiddenid'] = $id;
    //display member info based on the given id (from the catalog page)
    $getAll = $memberInfo->getMemberAcctDtlsWithID($id);
    $filename = $getAll[0]["PhotoFileName"];
    $_SESSION['filename'] = $filename;
    $hidMember = $getAll[0]["MemberInfoID"];
    $membershipCardNumber = $getAll[0]["CardNumber"];
    $firstName = $getAll[0]["FirstName"];
    $lastName = $getAll[0]["LastName"];
    $birthDate = $getAll[0]["Birthdate"];
    $maritalStatus = $getAll[0]["MaritalStatus"];
    $dateTime = new DateTime($birthDate);
    $birthDate = $dateTime->format("m/d/Y");
    $gender = $getAll[0]["Gender"];
    $idPresented = $getAll[0]["IdentificationPresented"];
    $idList = $refIdentification->getIDList($idPresented);
    if($idList[0]["IdentificationID"] == 4)
    {
        $option1 = $getAll[0]["Option1"];
    }
    $idNumber = $getAll[0]["IdentificationNumber"];
    $nationality = $getAll[0]["Nationality"];
    $countryList = $refCountries->getCountriesList($nationality);
    $address = $getAll[0]["Address"];
    $contactNumber = $getAll[0]["ContactNumber"];
    $email = $getAll[0]["Email"];
    $date = $getAll[0]["DateCreated"];
    $dateTime = new DateTime($date);
    $membershipDate = $dateTime->format("m/d/Y");
    $status = $getAll[0]["Status"];
    $remarks = $getAll[0]["Remarks"];
    $appTool = $getAll[0]["RegistrationOrigin"];
    $_SESSION['apptool'] = $appTool;
    $userType = $_SESSION['accttype'];
    
    if(count($hidMember) > 0)
    {
        $display = true;
    }
    else
    {
        $display = false; 
    }
    unset($_SESSION['btnbrowse']);
}
?>
