<?php
/*
 * Created by : Angelo Cubos
 * Date Created : April 01 2013
 * Purpose : controller updateacctprocess
 */
require_once("../../init.inc.php");

App::LoadControl("TextBox");
APP::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadControl("Label");
App::LoadControl("ComboBox");

$moduleName = "MembershipTimor";
App::LoadModuleClass($moduleName, "MTAccounts");
App::LoadModuleClass($moduleName, "MTAccountDetails");
App::LoadModuleClass($moduleName, "MTAccountTypes");
App::LoadModuleClass($moduleName, "MTAccountStatusLogs");
App::LoadCore("PHPMailer.class.php");
App::LoadModuleClass($moduleName, "MTAuditTrail");
App::LoadCore("DateSelector.class.php");

$mtAuditTrail = new MTAuditTrail();
$mtAccounts = new MTAccounts();
$mtAccountDetails = new MTAccountDetails();
$mtAccountStatusLogs = new MTAccountStatusLogs();

$fProc = new FormsProcessor();

$ddlUserName = new ComboBox("ddlUsername","ddlUsername");
$ddlgroupusername[] = new ListItem("", "0", true);
$ddlUserName->Items = $ddlgroupusername;
$ddlgroupusername = new ArrayList();
$getgroups = $mtAccounts->userList();
$ddlgroupusername->AddArray($getgroups);
$ddlUserName->DataSource = $ddlgroupusername;
$ddlUserName->DataSourceText = "UserName";
$ddlUserName->DataSourceValue = "AID";
$ddlUserName->DataBind();
$ddlUserName->Args = "onchange = 'javascript: onchange_username();'";
$ddlUserName->Style = "width: 280px";
$ddlUserName->Enabled = false;

$ddlAccountStatus = new ComboBox("ddlAccountStatus","ddlAccountStatus");
$opt_status          = null;
$opt_status[]        = new ListItem("Please Select","0",true);
$opt_status[]        = new ListItem("ACTIVE","1");
$opt_status[]        = new ListItem("SUSPENDED","2");
$opt_status[]        = new ListItem("TERMINATED","5");
$ddlAccountStatus->Items    = $opt_status;
$ddlAccountStatus->Style = "width: 120px;text-align: center;";

$btnSave = new Button("btnSave","btnSave","APPLY");
$btnSave->IsSubmit = true;
$btnSave->Style = "width: 150px";
$btnSave->CssClass = "labelbutton_black";
$btnSave->Args = "onclick = 'javascript: return showData();'";

$btnBack = new Button("btnBack","btnBack","BACK");
$btnBack->IsSubmit = true;
$btnBack->Style = "width: 150px";
$btnBack->CssClass = "labelbutton_black";
$btnBack->Enabled = true;

$btnSearch = new Button("btnSearch","btnSearch","SEARCH");
$btnSearch->IsSubmit = true;
$btnSearch->Style = "width: 200px";
$btnSearch->CssClass = "labelbutton_black";
$btnSearch->Enabled = false;

$remarks = new Hidden("remarks","remarks");

$btnOkay = new Button("btnOkay","btnOkay","OKAY");
$btnOkay->IsSubmit = true;
$btnOkay->Style = "width: 150px";
$btnOkay->CssClass = "labelbutton_black";

  $fProc->AddControl($ddlUserName);
  $fProc->AddControl($ddlAccountStatus);
  $fProc->AddControl($btnSave);
  $fProc->AddControl($btnBack);
  $fProc->AddControl($btnSearch);
  $fProc->AddControl($remarks);
  $fProc->AddControl($btnOkay);  
  $fProc->ProcessForms();
  
$success = false;

$setRemarks = $remarks->SubmittedValue;
$_SESSION["remarks"] = $setRemarks;

    $userAccountID ="";
    if(strlen($_SESSION['EditAcct'])>0)
    {
        $userAccountID = $_SESSION['EditAcct'];
        $ddlUserName->SetSelectedValue($userAccountID);
    }
    if(strlen($userAccountID)>0)
    {
                $arrUserInfo    = $mtAccounts->userInfo($userAccountID);
                $UserName       = $arrUserInfo[0]['UserName'];
                $status         = $arrUserInfo[0]['Status'];
                
                $_SESSION["tmpstatus"] = $status;
                $opt_status          = null;
                $opt_status[]        = new ListItem("Please Select","0",true);
             if($status != 1)
             {
                $opt_status[]        = new ListItem("ACTIVE","1");
             }
             if($status != 2)
             {
                $opt_status[]        = new ListItem("SUSPENDED","2");
             }
             if($status != 5)
             {
                $opt_status[]        = new ListItem("TERMINATED","5");
             }
                $ddlAccountStatus->Items    = $opt_status;
                $ddlAccountStatus->Style = "width: 120px;text-align: center;";                             
    }

if($fProc->IsPostBack)
{
        $newStatus  = $ddlAccountStatus->SubmittedValue;
        $ddlAccountStatus->SetSelectedValue($newStatus);
        
        if($status == 1){$cs = "ACTIVE";}
        if($status == 2){$cs = "SUSPENDED";}
        if($status == 5){$cs = "TERMINATED";}
        
        if($newStatus == 1){$ns = "ACTIVE";}
        if($newStatus == 2){$ns = "SUSPENDED";}
        if($newStatus == 5){$ns = "TERMINATED";}
        
    if($btnBack->SubmittedValue == "BACK")
    {
        $_SESSION['NewlyAddedAID'] = $_SESSION['EditAcct'];
        app::pr("<script>window.location.href='manageuser.php';</script>");    
        $_SESSION['EditAcct'] = "";    
    }
    if($btnSave->SubmittedValue == "APPLY")
    {

        if($newStatus == $_SESSION["tmpstatus"] || $newStatus == '0')
        {
                $errorTitle = "NOTIFICATION";
                $errorMessage = "Please enter new account status.";     
                
        }else if($setRemarks == "" || $setRemarks == NULL || strlen($setRemarks) == 0){
            
        $errorTitle = "NOTIFICATION";
        $errorMessage = "Please enter the reason for changing the account status.";            
                
        }else{
        $confirmTitle = "UPDATE ACCOUNT STATUS";
        $confirmMessage = "Are you sure you want to change $UserName\'s status from $cs to $ns?";
        $success = true;
        $_SESSION['NewlyAddedAID']=$userAccountID;
        }


    }
    if($btnOkay->SubmittedValue == 'OKAY')
    {
        $mtAccounts->StartTransaction();
        $mtAccounts->updateStatus($userAccountID,$ddlAccountStatus->SubmittedValue);
        if($mtAccounts->HasError)
        {
            $mtAccounts->RollBackTransaction();
            $errorTitle = "ERROR";
            $errorMessage = $mtAccounts->getError();
        }
        else
        {     
            $mtAccounts->CommitTransaction();
        
        $mtAccountStatusLogs->StartTransaction();
        $arrAccount['AID'] = $userAccountID;
        $arrAccount['DateCreated'] = "now_usec()";
        $arrAccount['OldStatus'] = $status;
        $arrAccount['NewStatus'] = $ddlAccountStatus->SubmittedValue;
        $arrAccount['Remarks'] = $setRemarks;
        $mtAccountStatusLogs->Insert($arrAccount);      
        if($mtAccountStatusLogs->HasError)
        {
            $errorTitle = "ERROR";
            $errorMessage = $mtAccountStatusLogs->getError();
            $mtAccountStatusLogs->RollBackTransaction();
        }
        else
        {
            $mtAccountStatusLogs->CommitTransaction();
            $result = $mtAccountDetails->userDetl($userAccountID);
            $eMail = $result[0]['Email'];
            $name = $result[0]['FirstName']." ".$result[0]['MiddleName']." ".$result[0]['LastName'];
            // Log to Audit Trail
            $mtAuditTrail->StartTransaction();
            $scAuditLogParam["SessionID"] = $_SESSION['sid'];
            $scAuditLogParam["AID"] = $_SESSION['aid'];
            $scAuditLogParam['AuditTrailFunctionID'] = '10';
            $scAuditLogParam["TransDetails"] = "Update Account Status: ".$UserName." from $cs to $ns";
            $scAuditLogParam["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
            $scAuditLogParam["TransDateTime"] = "now_usec()";

            $mtAuditTrail->Insert($scAuditLogParam);
            if($mtAuditTrail->HasError)
            {
                $errorMsg = $mtAuditTrail->getError();
                $mtAuditTrail->RollBackTransaction();
            }else{
                $mtAuditTrail->CommitTransaction();
            }
            // add emailing
            $pm = new PHPMailer();
            $pm->AddAddress($eMail,$name);
            $pageURL = 'http';
            if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
            $pageURL .= "://";
            $folder = $_SERVER["REQUEST_URI"];
            $folder = substr($folder,0,strrpos($folder,'/') + 1);
            if ($_SERVER["SERVER_PORT"] != "80") 
            {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$folder;
            } 
            else 
            {
            $pageURL .= $_SERVER["SERVER_NAME"].$folder;
            }

            $ds = new DateSelector();
            $ds->SetTimeZone("Asia/Dili");
            $date = Date('m/d/Y',strtotime($ds->CurrentDate));
            $time = $ds->GetCurrentDateFormat("H:i:s");                        
            $pm->IsHTML(true);
            $pm->Body = "<HTML><BODY>						
                                Dear $UserName,
                          <br /><br />
                          This is to inform you that your account status has been changed on this date $date and time $time
                          from $cs to $ns.
                          <br /><br />

                          For further inquiries on the status of your account, please contact our Customer Service email at membership@egames.com.tl.
                          <br /><br />
                          Regards,
                          <br /><br />
                          e-Games Management
                        </BODY></HTML>";
                        $pm->From = "membership@egames.com.tl";
                        $pm->FromName = "e-Games Management";
                        $pm->Host = "localhost";
                        $pm->Subject = "NOTIFICATION FOR USER ACCOUNT CHANGE OF STATUS";
                        $email_sent = $pm->Send();
                        if($email_sent)
                        {
                            $_SESSION['NewlyAddedAID'] = $_SESSION['EditAcct'];                      
                            $successTitle = "SUCCESSFUL NOTIFICATION";
                            $successMessage = "The user\'s account status has been updated.";
                            $_SESSION['EditAcct'] = "";
                        }
                        else
                        {
                            $successTitle = "ERROR";
                            $successMessage = "An error occurred while sending the email to your email address.";
                        }    
        }         
    }
   }
}

?>
