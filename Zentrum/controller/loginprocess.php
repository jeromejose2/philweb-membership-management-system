<?php
/*
 * Created By       :       Jerome Jose
 * Date Created     :       April 1, 2013
 * Purpose          :       Login Page Controller: Entry point of Timor Lester Zentrum .
 */
require_once("../../init.inc.php");
session_regenerate_id();
$moduleName = "MembershipTimor";
App::LoadModuleClass($moduleName,"MTAccountSessions");
App::LoadModuleClass($moduleName,"MTAuditTrail");
App::LoadModuleClass($moduleName,"MTAccounts");
App::LoadModuleClass($moduleName,"MTAccountDetails");
App::LoadModuleClass($moduleName,"MTPasswordUpdateRequests");
App::LoadModuleClass($moduleName,"MTAccessRights");
App::LoadCore("PHPMailer.class.php");
App::LoadCore("DateSelector.class.php");
App::LoadControl("TextBox");
App::LoadControl("Button");

$fProc = new FormsProcessor();

$accounts              = new MTAccounts();
$accountDetails        = new MTAccountDetails();
$accountSessions       = new MTAccountSessions();
$auditTrail            = new MTAuditTrail();
$passwordUpdateRequest = new MTPasswordUpdateRequests();

$splashPage = "index.php";

$txtUserName         = new TextBox("txtUsername","txtUsername","Username: ");
$txtUserName->Length = 20;
$txtUserName->Args   = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtPassword           = new TextBox("txtPassword","txtPassword","Password: ");
$txtPassword->Password = true;
$txtPassword->Length   = 20;
$txtPassword->Args     = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtEmail        = new TextBox("txtEmail","txtEmail","Email");
$txtEmail->Style = "height: 35px;width: 320px;font-size:1.0em;";
$txtEmail->Args  = "onkeypress='javascript: return verifyEmail(event);'";

$btnSubmit           = new Button("btnSubmit","btnSubmit","LOG-IN");
$btnSubmit->CssClass = "labelbold2";
$btnSubmit->Args     = "onclick='javascript: return checkLogin_forloginphp();'";
$btnSubmit->IsSubmit = true;

$btnReset           = new Button("btnReset","btnReset","RESET");
$btnReset->Args     = "onclick='javascript: return redirect();'";
$btnReset->CssClass = "labelbutton_black";
$btnReset->IsSubmit = true;


$fProc->AddControl($txtUserName);
$fProc->AddControl($txtPassword);
$fProc->AddControl($txtEmail);
$fProc->AddControl($btnSubmit);
$fProc->AddControl($btnReset);


$fProc->ProcessForms();


if ( $fProc->IsPostBack )
{
    if ( $btnSubmit->SubmittedValue == "LOG-IN" )
    {
        $userName = mysql_escape_string(trim($txtUserName->Text));
        $password = mysql_escape_string(trim($txtPassword->Text));

        $arrayPasswordUpdateAccts = $accounts->CheckAccount(MD5($userName),$password);
        if ( count($arrayPasswordUpdateAccts) == 1 )
        {

            $arrayPasswordUpdateDetails = $arrayPasswordUpdateAccts[0];
            $aid                        = $arrayPasswordUpdateDetails["AID"];
            $accountType                = $arrayPasswordUpdateDetails["AccountTypeID"];
            $sessionPassword            = $arrayPasswordUpdateDetails["Password"];
            if ( $arrayPasswordUpdateDetails["ForChangePassword"] == 0 )
            {
                $_SESSION['sid']            = session_id();
                $_SESSION['aid']            = $aid;
                $_SESSION['accttype']       = $accountType;
                $_SESSION['password']       = $sessionPassword;
                $_SESSION['uname']          = $userName;
                $_SESSION['acctid']         = $arrayPasswordUpdateDetails['AccountTypeID'];
                $_SESSION['actualpassword'] = $password;
                $boolRollBack               = false;

                // Check for Existing Session
                $accountSessions->StartTransaction();
                $auditTrail->StartTransaction();

                $arrayPasswordUpdateSession = $accountSessions->DoesAccountHasSession($aid);

                if ( count($arrayPasswordUpdateSession) == 1 )
                {
                    //update session id     
                    $sessionDetails                                    = $arrayPasswordUpdateSession[0];
                    $aid                                               = $sessionDetails["AID"];
                    $arrayPasswordUpdateayAccountSessions['SessionID'] = $_SESSION['sid'];
                    $arrayPasswordUpdateayAccountSessions['TransDate'] = "now_usec()";
                    $arrayPasswordUpdateayAccountSessions['DateStart'] = "now_usec()";
                    $arrayPasswordUpdateayAccountSessions['DateEnd']   = "0";
                    $arrayPasswordUpdateayAccountSessions['AID']       = $aid;

                    $accountSessions->UpdateByArray($arrayPasswordUpdateayAccountSessions);
                    if ( $accountSessions->HasError )
                    {
                        $errorMessage      = $accountSessions->getError();
                        $errorMessageTitle = "ERROR!";
                    }
                    else
                    {
                        $boolRollBack = false;
                    }
                }
                else
                {
                    // Insert into Account Session
                    $accountSessionParameter['SessionID'] = $_SESSION['sid'];
                    $accountSessionParameter['AID']       = $_SESSION['aid'];
                    $accountSessionParameter['RemoteIP']  = $_SERVER['REMOTE_ADDR'];
                    $accountSessionParameter['TransDate'] = "now_usec()";
                    $accountSessionParameter['DateStart'] = "now_usec()";
                    $accountSessionParameter['DateEnd']   = "0";

                    $accountSessions->Insert($accountSessionParameter);
                    if ( $accountSessions->HasError )
                    {
                        $errorMessage      = $accountSessions->getError();
                        $errorMessageTitle = "ERROR!";
                        $boolRollBack      = true;
                    }
                }

                $mtAccessRights        = new MTAccessRights();
                $dMenus                = $mtAccessRights->GetMenusByAccountTypeID($accountType);
                $_SESSION["usermenus"] = $dMenus;
                $landingPage           = $mtAccessRights->GetDefaultPage($accountType);
                $splashPage            = $landingPage[0]["Link"];

                // Log to Audit Trail
                $auditLogParameter['SessionID']            = $_SESSION['sid'];
                $auditLogParameter['AID']                  = $_SESSION['aid'];
                $auditLogParameter['AuditTrailFunctionID'] = "1";
                $auditLogParameter['TransDetails']         = "Login: " . $userName;
                $auditLogParameter['RemoteIP']             = $_SERVER['REMOTE_ADDR'];
                $auditLogParameter['TransDateTime']        = "now_usec()";

                $auditTrail->Insert($auditLogParameter);
                if ( $auditTrail->HasError )
                {
                    $errorMessage      = $auditTrail->getError();
                    $errorMessageTitle = "ERROR!";
                    $boolRollBack      = true;
                }

                if ( $boolRollBack )
                {
                    $accountSessions->RollBackTransaction();
                    $auditTrail->RollBackTransaction();
                }
                else
                {
                    $accountSessions->CommitTransaction();
                    $auditTrail->CommitTransaction();
                    URL::Redirect($splashPage);
                }
            }
            else
            {
                $errorMessage      = "You cannot login at the moment. Please check your email for the link to change your password.";
                $errorMessageTitle = "ERROR";
            }
        }
        else
        {
            $errorMessage      = "You have entered an incorrect account information.";
            $errorMessageTitle = "INVALID LOG-IN";
        }
    }


    if ( $btnReset->SubmittedValue == "RESET" )
    {
        $emailAddress = $txtEmail->SubmittedValue;

        $accounts->StartTransaction();
        $where        = "WHERE Email = '$emailAddress'";
        $userDetails  = $accountDetails->SelectByWhere($where);
        $id           = $userDetails[0]["AID"];
        $where2       = " WHERE AID = '$id'";
        $userDetails2 = $accounts->SelectByWhere($where2);
        if ( $userDetails2[0]['AccountTypeID'] != 5 )
        {
            $name = $userDetails2[0]["UserName"];
            if ( $userDetails )
            {
                $passwordUpdateRequest->StartTransaction();
                $id          = $userDetails[0]["AID"];
                $accountName = $userDetails[0]["FirstName"] . " " . $userDetails[0]["MiddleName"] . " " . $userDetails[0]["LastName"];
                $newpass     = substr(session_id(),0,8);
                //get request code
                $requestCode = MD5($emailAddress . $id . date("Y-m-d H:i:s"));


                $hasdata = $passwordUpdateRequest->DoesAccountHasData($id);
                if ( $hasdata )
                {
                    //update session id     
                    $arrayPasswordUpdate['DateRequested'] = "now_usec()";
                    $arrayPasswordUpdate['RequestCode']   = $requestCode;
                    $arrayPasswordUpdate['Status']        = "0";
                    $arrayPasswordUpdate['AID']           = $id;
                    $passwordUpdateRequest->UpdateByArray($arrayPasswordUpdate);
                }
                else
                {
                    $passwordRequest['AID']           = $id;
                    $passwordRequest['RequestCode']   = $requestCode;
                    $passwordRequest['DateRequested'] = "now_usec()";
                    $passwordRequest['Status']        = '0';
                    $passwordUpdateRequest->Insert($passwordRequest);
                }
                if ( $passwordUpdateRequest->HasError )
                {
                    $errorMessage      = "Error has occurred: " . $passwordUpdateRequest->getError();
                    $errorMessageTitle = "ERROR!";
                    $passwordUpdateRequest->RollBackTransaction();
                }
                else
                {

                    $passwordUpdateRequest->CommitTransaction();
                }

                // Update password
                $accounts->UpdateChangePassword($newpass,$id);
                if ( $accounts->HasError )
                {
                    $errorMessage      = $accounts->getErrors();
                    $errorMessageTitle = "ERROR!";
                    $accounts->RollBackTransaction();
                }
                $accounts->CommitTransaction();
                $pm                = new PHPMailer();
                $pm->AddAddress($emailAddress,$name);

                $pageURL = 'http';
                if ( $_SERVER["HTTPS"] == "on" )
                {
                    $pageURL .= "s";
                }
                $pageURL .= "://";
                $folder = $_SERVER["REQUEST_URI"];
                $folder = substr($folder,0,strrpos($folder,'/') + 1);
                if ( $_SERVER["SERVER_PORT"] != "80" )
                {
                    $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $folder;
                }
                else
                {
                    $pageURL .= $_SERVER["SERVER_NAME"] . $folder;
                }
                $pageURL = $pageURL . 'changeresetpassword.php?Uname=' . $requestCode;
                $pm->IsHTML(true);
                $ds      = new DateSelector();
                $ds->SetTimeZone("Asia/Dili");
                $date    = Date('m/d/Y',strtotime($ds->CurrentDate));
                $time    = $ds->GetCurrentDateFormat("H:i:s");

                $pm->Body = "Dear " . $accountName . "<br/><br/>
                            This is to inform you that your password has been reset on this date " . $date . " and time " . $time . ". Here are your new Account Details:<br/><br/>
                            Username: <b>" . $name . "</b><br/>
                            Password: <b>" . $newpass . "</b><br/><br/>" .
                          "For security purposes, please change this system-generated password upon log in or by clicking this link
                <a href=" . $pageURL . ">" . $pageURL . "</a><br/><br/>" .
                          "If you didn’t perform this procedure, please e-mail us at membership@egames.com.tl." . "<br/><br/>
                            Regards,<br/>e-Games Management<br/>";

                $pm->FromName = "no-reply";
                $pm->From     = "membership@egames.com.tl";
                $pm->Host     = "localhost";
                $pm->Subject  = "ZENTRUM E-GAMES CATALOG NEW PASSWORD";
                $email_sent   = $pm->Send();
                if ( $email_sent )
                {
                    $successmsg      = "Your new password has been sent to your e-mail account. For security purpose, please change this system-generated password upon log in.";
                    $successmsgtitle = "RESET PASSWORD";
                }
                else
                {
                    $errorMessage      = "An error occurred while sending the email to your email address";
                    $errorMessageTitle = "ERROR!";
                }
            }
            else
            {
                $errorMessage      = "You have entered an invalid e-mail address. Please try again.";
                $errorMessageTitle = "ERROR!";
            }
        }
        else
        {
            $errorMessage      = "You have entered an invalid e-mail address. Please try again.";
            $errorMessageTitle = "ERROR!";
        }
    }
}
?>
