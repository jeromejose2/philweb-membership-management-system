<?php
/*
 * Added By : Jerome F. Jose
 * Added On : June 21, 2012
 * Purpose : Change Password Admin
 */

require_once("../../init.inc.php");
$moduleName = "MembershipTimor";
App::LoadModuleClass($moduleName,"MTAccounts");
App::LoadModuleClass($moduleName,"MTAuditTrail");
App::LoadModuleClass($moduleName,"MTAccountSessions");
App::LoadModuleClass($moduleName,"MTAccountDetails");

$accounts        = new MTAccounts();
$accountDetails  = new MTAccountDetails();
$mtAuditTrail    = new MTAuditTrail();
$accountSessions = new MTAccountSessions();


App::LoadCore("PHPMailer.class.php");
App::LoadCore("DateSelector.class.php");

APP::LoadControl("TextBox");
App::LoadControl("Button");

$fProc = new FormsProcessor();

$txtPassword           = new TextBox("txtPassword","txtPassword");
$txtPassword->Style    = "width:400px;";
$txtPassword->Password = true;
$txtPassword->Length   = 12;
$txtPassword->Args     = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtNewPassword           = new TextBox("txtNewPassword","txtNewPassword");
$txtNewPassword->Style    = "width:400px;";
$txtNewPassword->Password = true;
$txtNewPassword->Length   = 12;
$txtNewPassword->Args     = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$txtConfPassword           = new TextBox("txtConfPassword","txtConfPassword");
$txtConfPassword->Style    = "width:400px;";
$txtConfPassword->Password = true;
$txtConfPassword->Length   = 12;
$txtConfPassword->Args     = "autocomplete='off' onkeypress='javascript: return AlphaNumericOnly(event);'";

$btnOk           = new Button("btnOK","btnOK","SUBMIT");
$btnOk->CssClass = "labelbutton_black";
$btnOk->IsSubmit = true;
$btnOk->Args     = "onclick='javascript: return checkchangepassword();' ";

$passSubmit           = new Button("passSubmit","passSubmit","OKAY");
$passSubmit->CssClass = "labelbutton_black";
$passSubmit->Args     = "onclick=document.getElementById('light11').style.display='none';document.getElementById('fade').style.display='none';";
$passSubmit->IsSubmit = true;


$btnCancel           = new Button("btncancel","btncancel","CANCEL");
$btnCancel->CssClass = "labelbutton_black";
$btnCancel->Args     = "onclick='javascript: return cancellink();' ";

$fProc->AddControl($txtPassword);
$fProc->AddControl($txtNewPassword);
$fProc->AddControl($txtConfPassword);
$fProc->AddControl($btnOk);
$fProc->AddControl($passSubmit);
$fProc->AddControl($btnCancel);


$fProc->ProcessForms();

if ( $fProc->IsPostBack )
{
    $sessionID = $_SESSION['sid'];


    if ( $passSubmit->SubmittedValue == "OKAY" )
    {

        $_SESSION['acctpword'] = $txtNewPassword->SubmittedValue;
        $_SESSION['newpw']     = $txtNewPassword->SubmittedValue;
        $_SESSION['oldpw']     = $txtPassword->SubmittedValue;
        $oldPass               = $txtPassword->SubmittedValue;
        $newPass               = $txtNewPassword->SubmittedValue;
        $newPassforemail       = $newPass;
        //change password

        $getSessionId = $accountSessions->GetSessionID($sessionID);
        if ( count($getSessionId) == 0 )
        {
            $errorMessage = "User Session does not Exist.";
        }
        else
        {

            $getDetail = $accounts->getCashierDetails($sessionID);

            $AcctID   = $getDetail[0]['AID'];
            $Email    = $getDetail[0]['Email'];
            $password = $getDetail[0]['Password'];
            $userName = $getDetail[0]['UserName'];
            $remoteIp = $getDetail[0]['RemoteIP'];

            $accounts->StartTransaction();

            $updatePass = $accounts->UpdateChangePassword($newPass,$AcctID);

            if ( $accounts->HasError )
            {
                $accounts->RollBackTransaction();
            }
            else
            {
                $accounts->CommitTransaction();

//                        -- insert in audit trail
                $mtAuditTrail->StartTransaction();
                $arrAudittrail['SessionID']            = $sessionID;
                $arrAudittrail['AuditTrailFunctionID'] = '9';
                $arrAudittrail['AID']                  = $AcctID;
                $arrAudittrail['TransDetails']         = "Change password for  AID :" . $AcctID . " ";
                $arrAudittrail['RemoteIP']             = $remoteIp;
                $arrAudittrail['TransDateTime']        = "now_usec()";
                $mtAuditTrail->Insert($arrAudittrail);
                if ( $mtAuditTrail->HasError )
                {
                    $errorMessage = " Error inserting in audit trail";
                }
                else
                {
                    $mtAuditTrail->CommitTransaction();
                }
            }

            $emailAdd    = $Email;
            $where       = " WHERE Email = '$emailAdd' ";
            $userDetails = $accountDetails->SelectByWhere($where);
            $id          = $userDetails[0]["AID"];
            $acctname    = $userDetails[0]["FirstName"] . ' ' . $userDetails[0]["MiddleName"] . ' ' . $userDetails[0]["LastName"];

            $where2       = " WHERE AID = '$id'";
            $userDetails2 = $accounts->SelectByWhere($where2);
            $name         = $userDetails2[0]["UserName"];

            $newPass = substr(session_id(),0,8);

            // Sending of Email
            $pm = new PHPMailer();
            $pm->AddAddress($emailAdd,$acctname);

            $pageURL = 'http';
            if ( $_SERVER["https"] == "on" )
            {
                $pageURL .= "s";
            }
            $pageURL .= "://";
            $folder = $_SERVER["REQUEST_URL"];
            $folder = substr($folder,0,strrpos($folder,'/') + 1);
            if ( $_SERVER["SERVER_PORT"] != "80" )
            {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $folder;
            }
            else
            {
                $pageURL .= $_SERVER["SERVER_NAME"] . $folder;
            }

            $ds   = new DateSelector();
            $ds->SetTimeZone("Asia/Dili");
            $date = Date('m/d/Y',strtotime($ds->CurrentDate));
            $time = $ds->GetCurrentDateFormat("H:i:s");
            $pm->IsHTML(true);

            $pm->Body     = "Dear " . $acctname . "<br/><br/>
            This is to inform you that your password has been changed on this date " . $date . " and time " . $time . ". Here are your new Account Details:<br/><br/>
            Username: <b>" . $name . "</b><br/>
            Password: <b>" . $newPassforemail . "</b><br/><br/>" .
                      "If you didn't perform this procedure, email us at membership@egames.com.tl." . "<br/><br/>
            Regards,<br/>e-Games Management";
            $pm->FromName = "e-Games Management";
            $pm->From     = "membership@egames.com.tl";
            $pm->Host     = "localhost";
            $pm->Subject  = "ZENTRUM E-GAMES CATALOG CHANGE PASSWORD";
            $emailSent    = $pm->Send();

            if ( $emailSent )
            {
                $successMsg = "success";
            }
        }
    }
}
?>
