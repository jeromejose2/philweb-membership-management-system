<?php

/*
 * Created by : Frances Ralph DL. Sison
 * Date Created : April 22 2013
 * Purpose :  Edit Member's Profile
 */
header("Pragma: no-cache");
header("cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

include_once("membershipcard.php");
require_once("../controller/editmembersrecordsprocess.php");
include("header.php");
?>
<link rel="stylesheet" type="text/css" href="mywebcam/assets/css/styles.css" />
<script src="jscripts/jquery-1.5.2.min.js"></script>
<script src="mywebcam/assets/fancybox/jquery.easing-1.3.pack.js"></script>
<script src="mywebcam/assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script src="mywebcam/assets/webcam/webcam.js"></script>
<script src="mywebcam/assets/js/script.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<style type="text/css">
    
    .overlay{
        opacity: 0;
        filter: alpha(opacity = 0);     
        position: absolute;
        top: 0; bottom: 0; left: 0; right: 0;
        display: block;
        z-index: 2;
        background: transparent;
        width: 240px;
        height: 190px;
    }
    
    .labelcls {
        color: blue;
        cursor: pointer;
    }

    .filecls {
        opacity: 0;
        width: 0px;
        height: 0px;
        display: inline-block;
        padding: 0;
        margin: 0;
        border: 0;
    }
    
</style>
<script type="text/javascript">
    $(document).ready(function(){
        
//        $('#uploadButton').live('click', function(){
//           $.ajax
//                ({
//                    url: 'photo.php?click=1',
//                    type : 'post',
//                    success : function(data)
//                    {
//                        $('#hidLang').val('15');
//                    },
//                    error: function(e)
//                    {
//                        alert("Error in getting photo.php");
//                    }
//                });
//        });
        
        $('input[type=text]').bind('cut copy paste', function (e) {
                    e.preventDefault();
        });
        
        var $body = $(this.ie6 ? document.body : document);
        $('#fade').css({
            width:  $body.width(),
            height:  $body.height()
        });
                
        var src = '';
        if($('#hidOrigin').val() == 2)
        {
            src = '/TegsRegistration/views/mywebcam/uploads/original/';
        }
        else if($('#hidOrigin').val() == 1)
        {           
            src = '/Zentrum/views/mywebcam/uploads/original/';
        }
        
        var hidID = $('#hidID');
        var hidId2 = $('#hidId2');
            

        var isOpera = !!window.opera || navigator.userAgent.indexOf('Opera') >= 0;
        // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
        var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        // At least Safari 3+: "[object HTMLElementConstructor]"
        var isChrome = !!window.chrome;                          // Chrome 1+
        var isIE = /*@cc_on!@*/false;                            // At least IE6
                
        if(isIE)
        {
            document.frames['frameId'].document;
        }
        else if(isChrome || isFirefox)
        {
            document.getElementById("frameId").contentDocument.location.reload(true);
        }
        $('#frameId').attr('src', 'photo.php');

        var val = $('#ddlIdPresented').val();
        if(val == 4)
        {
            $('#txtOthers').show();
        }
        else
        {
            $('#txtOthers').hide();
        }
        $('#btnBrowse').hide();
        $('#ddlIdPresented').change(function(){
           var val = $('#ddlIdPresented').val();
           if(val == 4)
           {
               $('#txtOthers').show();
           }
           else
           {
               $('#txtOthers').hide();
           }
        });
        

        $('#lnktake').click(function(){
            document.getElementById('lightphoto').style.display='block';
            document.getElementById('fade').style.display='block';           
            return false;
        });
        
        $('#filebrowse').change(function(){
           
            var isOpera = !!window.opera || navigator.userAgent.indexOf('Opera') >= 0;
            // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
            var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
            var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
            // At least Safari 3+: "[object HTMLElementConstructor]"
            var isChrome = !!window.chrome;                          // Chrome 1+
            var isIE = /*@cc_on!@*/false;                            // At least IE6
                
            if(isIE)
            {
                document.frames['frameId'].document;
            }
            else if(isChrome || isFirefox)
            {
                document.getElementById("frameId").contentDocument.location.reload(true)
            }
            
            $('#btnBrowse').trigger('click');
        });
            
        $('#btnOkayUpload').click(function(){
            $('#frameId').attr('src', 'photo.php');
            
            var isOpera = !!window.opera || navigator.userAgent.indexOf('Opera') >= 0;
            // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
            var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
            var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
            // At least Safari 3+: "[object HTMLElementConstructor]"
            var isChrome = !!window.chrome;                          // Chrome 1+
            var isIE = /*@cc_on!@*/false;                            // At least IE6
                
            if(isIE)
            {
                document.frames['frameId'].document;
            }
            else if(isChrome || isFirefox)
            {
                document.getElementById("frameId").contentDocument.location.reload(true)
            }
            
            $('#light1').css("display", "none");
            $('#fade').css("display", "none");            
	});
        
        $('#uploadButton').click(function(){
            
            
            var isOpera = !!window.opera || navigator.userAgent.indexOf('Opera') >= 0;
            // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
            var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
            var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
            // At least Safari 3+: "[object HTMLElementConstructor]"
            var isChrome = !!window.chrome;                          // Chrome 1+
            var isIE = /*@cc_on!@*/false;                            // At least IE6
 
            if(isIE)
            {
                document.frames['frameId'].document;
            }
            else if(isChrome || isFirefox)
            {
                document.getElementById("frameId").contentDocument.location.reload(true)
            }
            
            $('#light1').css("display", "none");
            $('#fade').css("display", "none");            
	});
        
        if($.browser.mozilla)
        {
                $('.labelcls').click(function() {
                    $('#filebrowse').click();
                });
        }
    });
</script>
<form id="frmEditProfile" name="frmEditProfile" method="post" action="editmembersrecords.php" enctype="multipart/form-data">
    <div id="page">
        <?php echo $hidID;echo $hidImgPath; echo $hidOrigin; echo $hidId2; echo $hidMemberID; echo $hidPage; echo $hidLang;?>
        <table width="100%">
            <tr>
                <td>
                    <table cellspacing="3">   
                        <tr>
                            <td colspan="2" align="center"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="fontboldblack">
                                <div id="tableheader">User Account Profile</div>
                            </td>
                        </tr>
                        <tr style="background-color:gray; height:30px;">
                            <td style="color:red;width:50%;">&nbsp;&nbsp;Membership Card Number</td>
                            <?php if($status == 1)
                                  {
                            ?>
                            <td style="width:50%;color:red;">&nbsp;&nbsp;A<?php printf("%1$05d", $membershipCardNumber); ?></td>
                            <?php }
                                  else if($status == 2 || $status == 0 || $status == 3 || $status == 4)
                                  {
                            ?>
                            <td style="width:50%;"></td>
                            <?php } ?>
                        </tr>
                        <tr style="background-color:lightgray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;*First Name</td>
                            <td>&nbsp;&nbsp;<?php echo $txtFName ?></td>
                        </tr>
                        <tr style="background-color:gray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;*Last Name</td>
                            <td>&nbsp;&nbsp;<?php echo $txtLName ?></td>
                        </tr>
                        <tr style="background-color:lightgray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;*Birthdate</td>
                            <td>&nbsp;&nbsp;<?php echo $ddlMonth ?>&nbsp;<?php echo $ddlDay ?>&nbsp;<?php echo $ddlYear ?></td>
                        </tr>
                        <tr style="background-color:gray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;*Gender</td>
                            <td>&nbsp;&nbsp;<?php echo $ddlGender ?></td>
                        </tr>
                        <tr style="background-color:lightgray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;*ID Presented</td>
                            <td>&nbsp;&nbsp;<?php echo $ddlIdPresented ?><div id="others"><center><?php echo $txtOthers; ?></center></div></td>
                        </tr>
                        <tr style="background-color:gray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;*ID Number</td>
                            <td>&nbsp;&nbsp;<?php echo $txtIdNumber ?></td>
                        </tr>
                        <tr style="background-color:lightgray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;Nationality</td>
                            <td>&nbsp;&nbsp;<?php echo $ddlNationality ?></td>
                        </tr>
                        <tr style="background-color:gray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;Marital Status</td>
                            <td>&nbsp;&nbsp;<?php echo $ddlMaritalStat ?></td>
                        </tr>
                        <tr style="background-color:lightgray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;Address</td>
                            <td>&nbsp;&nbsp;<?php echo $txtAddress ?></td>
                        </tr>
                        <tr style="background-color:gray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;Contact Number</td>
                            <td>&nbsp;&nbsp;<?php echo $txtContactNumber ?></td>
                        </tr>
                        <tr style="background-color:lightgray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;E-mail Address</td>
                            <td>&nbsp;&nbsp;<?php echo $txtEmail ?></td>
                        </tr>
                        <tr style="background-color:gray; height:30px;">
                            <td style="color: red;">&nbsp;&nbsp;Membership Date</td>
                            <td style="color: red;">&nbsp;&nbsp;<?php echo $membershipDate ?></td>
                        </tr>
                        <tr style="background-color:lightgray; height:30px;">
                            <td style="color: red;">&nbsp;&nbsp;Application Tool</td>
                            <td style="color: red;">&nbsp;&nbsp;<?php if($appTool == 2)
                                                  {
                                                    echo 'Online';
                                                  }
                                                  else if($appTool == 1)
                                                  {
                                                    echo 'Onsite';
                                                  }
                                             ?>
                            </td>
                        </tr>
                        <tr style="background-color:gray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;Status</td>
                            <td>&nbsp;&nbsp;<?php echo $ddlMemberStatus ?></td>
                        </tr>
                        <tr style="background-color:lightgray; height:30px;">
                            <td class="fontboldblack">&nbsp;&nbsp;Remarks</td>
                            <td>&nbsp;&nbsp;<?php echo $txtRemarks ?></td>
                        </tr>
                        <tr>
                            <td style="color: red"><i>(*) Required fields.</i></td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table width="100%;" cellspacing="10" cellpadding="10">
                        <tr>
                            <td>
                                <div id="photos">
                                    <iframe id="frameId" frameborder="no" scrolling="no" name="frameId" height="190" width="240"></iframe>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="lblpic" style="margin-left: 8%;">
                                    <label for="filebrowse" class="labelcls">Browse Photo</label> or
                                    <label id="lnktake" style="color: blue; cursor: pointer;">Take Photo</label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td id="tdbrowse">
                                <input type="file" name="filebrowse" id="filebrowse" class="filecls"/><?php echo $btnBrowse; ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <center>
            <?php echo $btnSubmit ?>
        </center>
        <br />
    <div id="light1" class="white_content">
        <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black; padding-top: 0px;">
            <b id="errorHead"></b>
        </div>
        <br/>
        <p id="errorMessage"><br/></p>
        <br/>
        <p id="button">
        <?php if ($uploadSuccessful): ?>
            <input type="button" id="btnOkayUpload" class="labelbutton_black" value="OKAY" />
        <?php else: ?>
            <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';"/>
        <?php endif; ?>
        </p>
     </div>		
     <div id="lightphoto" class="white_content">
        <div style="width: auto; top: 0px;"></div>
        <div id="camera">
            <div id="screen"></div>
            <div id="buttons">
                <div class="buttonPane">
                    <a id="shootButton" href="" class="labelbutton2" style="text-decoration: none;">SHOOT</a>
                    <a class="labelbutton2" style="text-decoration: none;" onclick ="document.getElementById('lightphoto').style.display='none';document.getElementById('fade').style.display='none';">CLOSE</a>
                </div>
                <div class="buttonPane hidden">
                    <a id="uploadButton" href="" class="labelbutton2" style="text-decoration: none;">UPLOAD</a>
                    <a id="cancelButton" href="" class="labelbutton2" style="text-decoration: none;">CANCEL</a> 
                </div>
            </div>
            <span class="settings"></span>
        </div>
    </div>
    <div id="fade" class="black_overlay"></div>
    </div>
    <?php if ($uploadSuccessful) : ?>
        <script>
            document.getElementById('errorHead').innerHTML = "<?php echo $messageTitle; ?>";
            document.getElementById('errorMessage').innerHTML = "<?php echo $message;?>";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>  
    <?php endif; $uploadSuccessful = false; ?>
    <?php if (isset($errorMessage)) : ?>
        <script>
            document.getElementById('errorHead').innerHTML = "<?php echo $errorTitle; ?> ";
            document.getElementById('errorMessage').innerHTML = "<?php echo $errorMessage;?>";
            <?php if($confirm == true)
                  {
            ?>
                    document.getElementById('button').innerHTML = '<?php echo addslashes($btnProceed);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="labelbutton_black" onclick="document.getElementById(\'light1\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">CANCEL</button> ';
            <?php } ?>
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>  
    <?php endif; unset($errorMessage); ?>
    <?php if (isset($errorMessage2)) : ?>
        <script>
            document.getElementById('errorHead').innerHTML = "CONFIRMATION";
            document.getElementById('errorMessage').innerHTML = "<?php echo $errorMessage2;?>"; 
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>  
    <?php endif; unset($errorMessage2); ?>
    <?php if (isset($successMessage)) : ?>
        <script>
            document.getElementById('errorHead').innerHTML = "SUCCESSFUL NOTIFICATION";
            document.getElementById('errorMessage').innerHTML = "<?php echo $successMessage;?>";
            <?php if($success==true) : ?>
                document.getElementById('button').innerHTML = '<button type="button" class="labelbutton_black" onclick="window.location = \'filterandviewmembers.php\';">OKAY</button> ';
            <?php endif;?>
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>  
    <?php endif; unset($successMessage); ?>
</form>
<?php include("footer.php");?>
