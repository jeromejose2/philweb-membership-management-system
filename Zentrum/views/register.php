<?php
/**
 * The view page. This module will be used for registration of e-Games members.
 * @author Noel Antonio 04-02-2013
 * @copyright 2013 PhilWeb Corporation 
 */
header("Pragma: no-cache");
header("cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
require_once("tc_calendar.php");
include_once("membershipcard.php");
require_once("../controller/registerprocess.php");
require_once("header.php");
?>
<head>
    <!--<link rel="stylesheet" type="text/css" href="css/calendar.css" />-->
    <link rel="stylesheet" type="text/css" href="css/default.css" />
    <link rel="stylesheet" type="text/css" href="mywebcam/assets/css/styles.css" />
    <script src="jscripts/jquery-1.5.2.min.js"></script>
    <script src="mywebcam/assets/fancybox/jquery.easing-1.3.pack.js"></script>
    <script src="mywebcam/assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script src="mywebcam/assets/webcam/webcam.js"></script>
    <script src="mywebcam/assets/js/script.js"></script>
    <script src="jscripts/checkinputs.js"></script>
    <script language="javascript" src="jscripts/calendar.js"></script>
    <style type="text/css">
        .overlay{
            opacity: 0;
            filter: alpha(opacity = 0);
            position: absolute;
            top: 0; bottom: 0; left: 0; right: 0;
            display: block;
            z-index: 2;
            background: transparent;
        }

        .labelcls {
            color: blue;
            cursor: pointer;
        }

        .filecls {
            opacity: 0;
            width: 0px;
            height: 0px;
            display: inline-block;
            padding: 0;
            margin: 0;
            border: 0;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $('input[type=text]').bind('cut copy paste', function (e) {
                    e.preventDefault();
            });
              $('#content-div').css({
                height:  '700px' 
            });
            var $body = $(this.ie6 ? document.body : document);
            $('#fade').css({
                width: $body.width(),
                height: $body.height()
            });

            if ($.trim($('#txtFirstName').val()) !== "" || $.trim($('#txtLastName').val()) !== "") {
                document.getElementById("frameId").contentDocument.location.reload(true);
                $('#frameId').attr('src', 'image.php');
            }

            var val = $('#ddlIdPresented').val();
            if (val == 4) {
                $('#others').show();
            } else {
                $('#others').hide();
            }
            $('#btnBrowse').hide();
            $('#ddlIdPresented').change(function() {
                var val = $('#ddlIdPresented').val();
                if (val == 4) {
                    $('#others').show();
                } else {
                    $('#others').hide();
                }
            });

            $('#linkTake').click(function() {
                document.getElementById('lightphoto').style.display = 'block';
                document.getElementById('fade').style.display = 'block';
                return false;
            });

            $('#file').change(function() {
                $('#btnBrowse').trigger('click');
            });

            $('#btnOkayUpload').click(function() {
                $('#frameId').attr('src', 'image.php');
                $('#light1').css("display", "none");
                $('#fade').css("display", "none");
            });

            if ($.browser.mozilla) {
                $('.labelcls').click(function() {
                    $('#file').click();
                });
            }

        });
    </script>
</head>
<form encType="multipart/form-data" id="frmRegister" name="frmRegister" method="POST" action="register.php">
    <table width="100%">
        <tr>
            <td>
                <?php 
                    echo $hidID;
                    echo $hidImgPath;
                    echo $hidBday;
                ?>
                <table cellspacing="10">
                    <tr>
                        <td><div id="lblfn">* First Name</div></td><td><?php echo $txtFirstName; ?></td>
                        <td><div id="lblln">* Last Name</div></td><td><?php echo $txtLastName; ?></td>
                    </tr>
                    <tr>
                        <td><div id="lblbday">* Birthdate</div></td>
                        <td>
                            <?php $myCalendar->writeScript(); ?>
                        </td>
                        <td><div id="lblsex">* Gender</div></td><td><?php echo $ddlSex; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;Nationality</td><td><?php echo $ddlNationality; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;Marital Status</td><td><?php echo $ddlMaritalStat; ?></td>
                    </tr>
                </table>

                <table cellspacing="10">
                    <tr >
                        <td>&nbsp;&nbsp;&nbsp;Address</td><td><?php echo $txtAddress; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;Contact Number</td><td><?php echo $txtContactNo; ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;E-mail Address</td><td><?php echo $txtEmail; ?></td>
                    </tr>
                </table>

                <table cellspacing="10">
                    <tr>
                        <td><div id="lblidpr">* ID Presented</div></td><td><?php echo $ddlIdPresented; ?><div id="others"><?php echo $txtOthers; ?></div></td>
                        <td><div id="lblidno">* ID Number</div></td><td><?php echo $txtIDNo; ?></td>
                    </tr>
                    <tr>
                        <td><div id="lblstatus">* Status</div></td><td><?php echo $ddlStatus; ?></td>
                    </tr>
                    <tr>
                        <td style="color: red"><i>(*) Required fields.</i></td>
                    </tr>
                </table>
                
                <div align="center">
                    <table>
                        <tr>
                            <td width="150px"><?php echo $btnSubmit; ?></td>
                            <td><?php echo $btnBack; ?></td>
                        </tr>
                    </table>
                </div>
            </td>
            <td>
                <table width="100%;" cellspacing="10" cellpadding="10">
                    <tr>
                        <td>
                            <div id="photos">
                                <iframe id="frameId" frameborder="no" scrolling="no" name="frameId" height="190" width="240"></iframe>
                                <!--<div class="overlay"></div>-->
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="lblpic" style="margin-left: 8%;"> 
                                <label for="file" class="labelcls">Browse Photo</label> or 
                                <label id="linkTake" style="color: blue; cursor: pointer;">Take Photo</label>
                            </div>
                        </td>
                    </tr>
                    <input type="file" id="file" name="file" class="filecls"><?php echo $btnBrowse; ?>
                </table>
            </td>
        </tr>
    </table>

    <div id="light1" style="text-align: center;font-size: 16pt;height: 220px;background-color: #e6e6e6;" class="white_content">
        <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black; padding-top: 0px;">
            <b id="errorHead"></b>
        </div>
        <br/>
        <p id="errorMsg"><br/></p>
        <br/>
        <p id="button">
            <?php if ($uploadSuccessful): ?>
                <input type="button" id="btnOkayUpload" class="labelbutton_black" value="OKAY" />
            <?php else: ?>    
                <input type="button" class="labelbutton_black" value="OKAY" onclick ="document.getElementById('light1').style.display = 'none';
                document.getElementById('fade').style.display = 'none';"/>
            <?php endif; ?>
        </p>
    </div>

    <div id="light2" style="text-align: center;font-size: 16pt;height: 220px;background-color: #e6e6e6;" class="white_content">
        <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black; padding-top: 0px;">
            <b id="errorHead2"></b>
        </div>
        <br/>
        <p id="errorMsg2"><br/></p>
        <br/>
        <p id="button">
            <input type="button" class="labelbutton_black" value="OKAY" onclick ="window.location.href = 'filterandviewmembers.php'"/>
        </p>
    </div>

    <div id="light3" style="text-align: center;font-size: 16pt;height: 220px;background-color: #e6e6e6;" class="white_content">
        <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black; padding-top: 0px;">
            <b id="errorHead3"></b>
        </div>
        <br/>
        <p id="errorMsg3"><br/></p>
        <br/>
        <p id="button">
                   <?php echo $btnOkay; ?>&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="button" value="CANCEL" class="labelbutton_black" onclick ="document.getElementById('light3').style.display = 'none';
            document.getElementById('fade').style.display = 'none';"/>
        </p>
    </div>

    <div id="lightphoto" style="text-align: center;font-size: 16pt; height: auto;background-color: #e6e6e6;" class="white_content">
        <div style="width: auto; top: 0px;"></div>

        <div id="camera">
            <div id="screen"></div>
            <div id="buttons">
                <div class="buttonPane">
                    <a id="shootButton" href="" class="labelbutton2" style="text-decoration: none;">SHOOT</a>
                    <a class="labelbutton2" style="text-decoration: none;" onclick ="document.getElementById('lightphoto').style.display = 'none';
                    document.getElementById('fade').style.display = 'none';">CLOSE</a>
                </div>
                <div class="buttonPane hidden">
                    <a id="uploadButton" href="" class="labelbutton2" style="text-decoration: none;">UPLOAD</a>
                    <a id="cancelButton" href="" class="labelbutton2" style="text-decoration: none;">CANCEL</a> 
                </div>
            </div>

            <span class="settings"></span>
        </div>

    </div>
    <div id="fade" class="black_overlay"></div>

<?php if ($uploadSuccessful) : ?>
        <script>
            document.getElementById('errorHead').innerHTML = "<?php echo $msgTitle; ?>";
            document.getElementById('errorMsg').innerHTML = "<?php echo $msg; ?>";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>  
<?php endif; $uploadSuccessful = false; ?>

<?php if (isset($errorMsg)) : ?>
        <script>
            document.getElementById('errorHead').innerHTML = "NOTIFICATION";
            document.getElementById('errorMsg').innerHTML = "<?php echo $errorMsg; ?>";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>  
<?php endif; unset($errorMsg); ?>

<?php if (isset($successMsg)) : ?>
        <script>
            document.getElementById('errorHead2').innerHTML = "SUCCESSFUL NOTIFICATION";
            document.getElementById('errorMsg2').innerHTML = "<?php echo $successMsg; ?>";
            document.getElementById('light2').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
        </script>  
<?php endif; unset($successMsg); ?>
</form>