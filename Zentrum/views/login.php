<?php
/*
 * @author Jerome Jose 2013-04-01
 * 
 * Purpose : Login Page: Entry point.
 */
include("../controller/loginprocess.php");
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
        <script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
        <script language="Javascript" src="jscripts/checkinputs.js" ></script>

        <title>
            ZENTRUM E-GAMES MEMBERSHIP CATALOG
        </title>  
        <script type="text/javascript"> 
            function checkLogin_forloginphp()
            {
      
                var oUsername = document.getElementById("txtUsername");
                var oPassword = document.getElementById("txtPassword");
                var pattern=/\s/;
                if (oUsername.value.length == 0 || oUsername.value.match(pattern))
                {
                    document.getElementById('errorHead').innerHTML = 'INVALID LOG-IN';
                    document.getElementById('errorMsg').innerHTML = 'Please enter your username.';
                    $('#light1').fadeIn('slow');document.getElementById('fade').style.display = 'block';
                    document.getElementById('txtUsername').focus();
                    return false;
                }
                else if (oPassword.value.length == 0)
                {
                    document.getElementById('errorHead').innerHTML = 'INVALID LOG-IN';
                    document.getElementById('errorMsg').innerHTML = 'Please enter your password.';
                    $('#light1').fadeIn('slow');document.getElementById('fade').style.display = 'block';
                    document.getElementById('txtPassword').focus();
                    return false;
                }
                return true;
            }
            function openforgotpw()
            {
                document.getElementById('a4').innerHTML = 'RESET PASSWORD CONFIRMATION ';
                document.getElementById('b4').innerHTML = 'You are about to reset your password. Please enter your registered e-mail address.';
                $('#light4').fadeIn('slow');document.getElementById('fade').style.display = 'block';
                return false;
            }
            function redirect()
            {
                var email = document.getElementById('txtEmail').value;
        
                if(email.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
                {
                    document.getElementById('a4').innerHTML = "BLANK E-MAIL";
                    document.getElementById('b4').innerHTML = "Please enter your e-mail address.";
                    document.getElementById('fade').style.display='block';
                    return false;
                }
                if(!checkemail_reset(email))
                {
                    document.getElementById('a4').innerHTML = "INVALID E-MAIL";
                    document.getElementById('b4').innerHTML = "You have entered an invalid e-mail address. Please try again.";
                    document.getElementById('fade').style.display='block';
                    return false;
                }
                return true;
            }
            function checkemail_reset(email)
            {
                var str=email
                var filter=/^.+@.+\..{2,3}$/
                if (filter.test(str))
                    return true;
                else
                    return false;
                return true;
        
        
            }
            $(document).ready(function(){
                var $body = $(this.ie6 ? document.body : document);
                    
                if($.browser.msie)
                { 
                    $('#fade').css({
                    });
                }
                $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
                    e.preventDefault();
                });
            });
    
        </script>
    </head>
    <body onload="//$('#light4').fadeIn('slow');$('#fade').fadeIn('slow');">
        <form id="frmLogin" name="frmLogin" action="login.php" method="POST">
            <div id="container">
                <div id="login"> <br/><br/>
                    <table cellspacing="10" width="100%">
                        <tr>
                            <td colspan="3" align="center">                
                                <div style="width:370px;"><img src="images/e_gameslogo.jpg"></div>
                                <br /><h3>ZENTRUM </h3>
                                <h4> E-GAMES MEMBERSHIP CATALOG</h4>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="fontboldblack" align="center">
                                <br/>Username: <?php echo $txtUserName; ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="fontboldblack" align="center">
                                Password: <?php echo $txtPassword; ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <?php echo $btnSubmit; ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <div>
                                    <a href="#" onclick="javascript: return openforgotpw();" style="font-size:1.0em; font-weight:bold; cursor:pointer; color:#000000;">Forgot Password</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div id="light1" style="text-align: center;font-size: 16pt;height: 220px;background-color: #e6e6e6;" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black;padding-top: 5px;">
                    <b id="errorHead"></b>
                </div>
                <br/>
                <p id="errorMsg"><br/></p>
                <br/>
                <input type="button"  id="btnokayfade" value="OKAY" class="labelbutton_black" onclick =""/>
            </div>
            <!-- FORGOT PASSWORD-->
            <div id="light4" style="text-align: center;font-size: 16pt;height: auto;background-color: #e6e6e6;" class="white_content">
                <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black;padding-top: 5px;">
                    <b id="a4"></b>
                </div>
                <br/>
                <p id="b4"><br/></p>
                <br/><?php echo $txtEmail; ?><br /><br />
                <?php echo $btnReset; ?>
                <input id="btnokayfade" type="button" value="CANCEL" class="labelbutton_black" onclick="document.getElementById('txtEmail').value='';"></input>
            </div>
            <!-- FORGOT PASSWORD--> 

            <div id="fade" class="black_overlay"></div>
        </form>
        <script type="text/javascript">  
<?php if ( isset($errorMessage) ): ?>
        document.getElementById('errorHead').innerHTML = "<?php echo $errorMessageTitle; ?>";
        document.getElementById('errorMsg').innerHTML = "<?php echo $errorMessage; ?>";
        document.getElementById('light1').style.display='block';
        document.getElementById('fade').style.display='block';
<?php endif; ?>
        
<?php if ( isset($successmsg) ): ?>
        document.getElementById('errorHead').innerHTML = "<?php echo $successmsgtitle; ?>";
        document.getElementById('errorMsg').innerHTML = "<?php echo $successmsg; ?>";
        document.getElementById('light1').style.display='block';
        document.getElementById('fade').style.display='block';
<?php endif; ?>    
    $('#btnokayfade').live('click',function(){
        $('#light1').fadeOut('slow');
        $('#light4').fadeOut('slow');
        document.getElementById('fade').style.display = 'none';
    });    
        
        </script>

    </body>


</html>
