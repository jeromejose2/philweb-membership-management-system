<?php

/**
 * This page will be used to rename the default name of the image taken.
 * 
 * @author Noel Antonio <ndantonio@philweb.com.ph>
 * @copyright (c) 2013, PhilWeb Corporation
 */

require_once '../../init.inc.php';

$basepath = App::getParam('appdir');

$orig = $_GET["orig"];
$new = $_GET["new"] . '.jpg';
$path = $_GET["path"];

if($path == 2)
{
    rename($basepath.'TegsRegistration/views/mywebcam/uploads/original/'.$orig, $basepath.'TegsRegistration/views/mywebcam/uploads/original/'.$new);
    rename($basepath.'TegsRegistration/views/mywebcam/uploads/thumbs/'.$orig, $basepath.'TegsRegistration/views/mywebcam/uploads/thumbs/'.$new);
}
else
{
    rename('mywebcam/uploads/original/'.$orig,'mywebcam/uploads/original/'.$new);
    rename('mywebcam/uploads/thumbs/'.$orig,'mywebcam/uploads/thumbs/'.$new);
    }

?>