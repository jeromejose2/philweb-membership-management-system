<?php
/*
 * Created By   :           JFJ
 * Created On   :           April 15,2013
 * Purpose      :           Header Page
 */
//error_reporting(E_ALL ^ E_NOTICE);
//ini_set('display_errors',1);
//error_reporting(-1);

App::LoadModuleClass("MembershipTimor","MTAccountSessions");
App::LoadModuleClass("MembershipTimor","MTAccounts");
App::LoadModuleClass("MembershipTimor","MTAuditTrail");

$cadminacctsessions = new MTAccountSessions();

$date_time      = $cadminacctsessions->selectNow();
$serverdatetime = $date_time[0][0];
$startdate      = date("F d, Y H:i:s");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>ZENTRUM E-GAMES MEMBERSHIP CATALOG</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language='javascript' type='text/javascript' src='jscripts/jquery-1.5.2.min.js'></script>
        <script language='javascript' type='text/javascript' src='jscripts/checkinputs.js'></script>
        <script language='javascript' type='text/javascript' src='jscripts/datepicker.js'></script>
        <link rel='stylesheet' type='text/css' media='screen' href='css/default.css' />
        <link rel='stylesheet' type='text/css' media='screen' href='css/datepicker.css' />   
        <script type="text/javascript">
            $(document).ready(function(){
               
                
                var $body = $(this.ie6 ? document.body : document);
                if($.browser.msie)
                { 
                    $('#fade').css({
                        height:  $body.height() 
                    });
                        
                }else
                {
                    $('#fade').css({
                        width:  $body.width(),
                        height:  $body.height() 
                    });
                }
             
           
                function preventBackandForward()
                {
                    window.history.forward();
                }

                preventBackandForward();
                window.onload=preventBackandForward();
                window.inhibited_load=preventBackandForward;
                window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
                window.inhibited_unload=function(){void(0);};
            });
        </script>
        <style>
            #logo{
                margin-left: 0.5%;
                padding-top: 0.5%;
                width: auto;
            }
        </style>
    </head>
    <body onload="" style="">
        <div id="containertemplate">
            <div id="fade" class="black_overlay"></div>
            <div id="logo">
                <img src="images/e_gameslogo.jpg" alt="" height ="90px" width="220px"/>
            </div>
            <div id="datetime" style ="height: 100px; width:230px; position: relative; margin-top: -120px; margin-left: 880px;">
                <div><br /><br />
                    <b>Current Date:</b>&nbsp;<span id="servdate" name="servdate">&nbsp;</span>
                </div>
                <div>
                    <b>Current Time:</b>&nbsp;<span id="clock">&nbsp;</span>
                </div>
            </div>
<script type="text/javascript">
            var tempDate = "<?php echo $serverdatetime; ?>";
            var tempTime = "<?php echo $serverdatetime; ?>";
            
            tempDate = tempDate.substring(5,7) + "/" + tempDate.substring(8,10) + "/" + tempDate.substring(0,4);
            document.getElementById("servdate").firstChild.nodeValue = tempDate;



            var timesetter = new Date('<?php echo $startdate ?>');
            var TimeNow = '';
            function MakeTime() {
                timesetter.setTime(timesetter.getTime()+1000);
                var hhN  = timesetter.getHours();
                if(hhN > 12){
                    var hh = String(hhN - 12);
                    var AP = 'PM';
                } else if(hhN == 12) {
                    var hh = '12';
                    var AP = 'PM';
                }else if(hhN == 0){
                    var hh = '12';
                    var AP = 'AM';
                }else{
                    var hh = String(hhN);
                    var AP = 'AM';
                }
                var mm  = String(timesetter.getMinutes());
                var ss  = String(timesetter.getSeconds());
                TimeNow = ((hh < 10) ? ' ' : '') + hh + ((mm < 10) ? ':0' : ':') + mm + ((ss < 10) ? ':0' : ':') + ss + ' ' + AP;
                document.getElementById("clock").firstChild.nodeValue = TimeNow;
                setTimeout(function(){
                    MakeTime();},1000);
            }
            MakeTime();

            function timedRefresh(timeoutPeriod)
            {
                setTimeout("location.reload(true);",timeoutPeriod);
            }
        </script>

            <table style="margin-top: 0; width: 100%;">
                <tr>
                    <td>
                        <div id="menu">
                            <?php // include_once('menu.php');  ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="" style="
                             width:auto;
                             height:600px;
                             position:relative;
                             margin-top:3px;
                             border-color:#000000;
                             background-color:#f3f3f3;
                             border-width:thin;
                             border-style:solid;
                             overflow-y:auto;
                             vertical-align:top;
                             padding: 1% 1% 1% 1%;
                             ">
