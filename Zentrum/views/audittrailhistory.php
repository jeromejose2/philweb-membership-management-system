<?php
/*
 * @author Jerome Jose 04-18-2013
 * Purpose   : controller for audittrail
 */
require_once('../controller/audittrailprocess.php');
require_once('header.php');

?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script language="javascript" src="jscripts/datepicker.js" type="text/javascript"></script>
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script type="text/javascript">
    function checkaudittrail()
    {
	//modified by MKGE 06-19-2013
        var DateFrom    = document.getElementById("txtDateFrom").value;
        var DateTo      = document.getElementById("txtDateTo").value;
        var validDateRange = (DateFrom > DateTo);
        if (validDateRange)
        {
            document.getElementById('title12').innerHTML= 'ERROR!';
            document.getElementById('msg12').innerHTML = 'Please select a valid date range.';
            document.getElementById('light12').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
        return true;
    }
    $('#btnClose').live('click',function(){
        $('#light12').fadeOut('slow');
        $('#fade').fadeOut('slow');   
    }); 
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms[0].submit();
    }
</script>
<style>
    .paging
    {
        font-size:0.95em;
        font-family:Verdana, Arial, Helvetica, sans-serif;
        text-decoration:none;
        color: black;
        font-weight:bolder;
    }
</style>
<form name="formauditrail" method="POST">
    <div style="width:100%; text-align:center;">
        <!--Modified by: Mark Kenneth Esguerra 06-19-2013-->
        <!--Add Date Range-->
        <table width="100%">
            <tr>
                <td class="labelbold2">Date From:
                    <?php echo $txtDateFrom; //Date From ?>
                    <img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateFrom', false, 'mdy', '/');"/>
                    &nbsp;To:
                    <?php echo $txtDateTo; //Date To ?>
                    <img name="cal" src="images/close.gif" width="16" height="16" border="0" alt="Pick a date" onClick="displayDatePicker('txtDateTo', false, 'mdy', '/');"/>
                    &nbsp;<?php echo $ddlFunction; ?>&nbsp;<?php echo $btnSubmit; ?>  
                </td>
            </tr>

        </table>
        <?php if ( isset($tableDataList) ): ?> 
            <div class="paging"><?php echo $pageAudit; ?></div>
            <table class="table-list">
                <tr>
                    <th align="left">Date</th>
                    <th align="left">Username</th>
                    <th align="left">Transaction Details</th>
                    <th align="left">IP Address</th>
                    <th align="left">Function</th>
                </tr>
                <?php if ( count($tableDataList) > 0 ): ?>
                    <?php for ($ctr = 0; $ctr < count($tableDataList); $ctr++): ?>
                        <?php ($ctr % 2) == 0 ? $class    = "evenrow" : $class    = "oddrow"; ?>
                        <tr class = "<?php echo $class ?>">
                            <?php $dateTime = new DateTime($tableDataList[$ctr]['TransDateTime']); ?>
                            <td><?php echo $dateTime->format('Y-m-d h:i:s A') ?></td>
                            <td><?php echo $tableDataList[$ctr]['UserName'] ?></td>
                            <td><?php echo $tableDataList[$ctr]['TransDetails'] ?></td>
                            <td><?php echo $tableDataList[$ctr]['RemoteIP'] ?></td>
                            <td><?php echo $tableDataList[$ctr]['AuditFunctionName'] ?></td>
                        </tr>
                    <?php endfor; ?>
                <?php else: ?>
                    <tr style="" align="center"><td colspan="5">No records to display</td></tr>
                <?php endif; ?>
            </table>
        <?php endif; ?>
    </div>
</form>
</div>
<div id="light12" style="text-align: center;font-size: 16pt;height: 220px;background-color: #e6e6e6;" class="white_content">
    <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black;padding-top: 5px;">
        <b id="title12"></b>
    </div>
    <br/>
    <p id="msg12"><br/></p>
    <br/>
    <input id="btnClose" type="button" value="OKAY" class="labelbutton_black" />
</div>
