<?php

/**
 * Loads the image.
 * 
 */
header("Pragma: no-cache");
header("cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

include_once 'membershipcard.php';
include_once '../controller/editmembersrecordsprocess.php';

$srcPath = '';

// no database records and no uploaded images triggered
if(($memberDataPhoto == "" && (!isset($_SESSION['btnbrowse']) && !isset($_SESSION['takephoto']))))
{
    if ($memberDataOrigin == 1)
        $srcPath = "mywebcam/uploads/original/Blank.png";
    else
        $srcPath = "/TegsRegistration/views/mywebcam/uploads/original/Blank.png";
}
// image uploaded and saved
else
{
    if ($memberDataOrigin == 1)
        $srcPath = "mywebcam/uploads/original/" . $memberDataId . ".jpg";
    else
        $srcPath = "/TegsRegistration/views/mywebcam/uploads/original/" . $memberDataId . ".jpg";
}
?>

<html>
    <head><?php if(!isset($_GET['jy'])) { echo '<meta http-equiv="refresh" content= "0;URL=?jy=image" />'; } ?></head>
    <body>
         <img src="<?php echo $srcPath; ?>" height="190" width="240" />    
    </body>
</html>