<?php
/**
 * Loads the image.
 * @author Noel Antonio <ndantonio@philweb.com.ph>
 * @copyright (c) 2013, PhilWeb Corporation
 */
include_once './membershipcard.php';
?>
<html>
    <head><?php if(!isset($_GET['jy'])) { echo '<meta http-equiv="refresh" content= "0;URL=?jy=image" />'; } ?></head>
    <body>
       <img src="mywebcam/uploads/original/<?php echo $memberID . ".jpg"; ?>" height="190" width="240" /> 
    </body>
</html>