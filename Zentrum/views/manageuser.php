<?php
/*
 * Created by : Angelo Cubos
 * Date Created : April 01 2013
 * Purpose : view useraccountprofile
 */
//require_once("header.php");
require_once("../controller/manageuserprocess.php");
require_once("header.php");
?>

<head>
    <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
    <script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
    <script language="javascript" src="jscripts/checkinputs.js"></script>

    <script language="javascript">
        function showData()
        {
        <?php if (strlen($userName) > 0): ?>
                        document.getElementById("result").style.visibility = "visible";
        <? endif; ?>
        }
    </script>
</head>
<body>
    <form method="post">
        <br>
        <div align="center"> 
            <div align="center"><b>USERNAME</b>&nbsp;&nbsp;&nbsp;<?php echo $ddlUserName; ?>&nbsp;<?php echo $btnSearch; ?></div>
            <br>
            <div id="result"
            <?php
            $style = 'style="visibility:hidden"';
            if (strlen($userName) == 0 && $btnSearch->SubmittedValue == "SEARCH")
            {
                echo $style;
            } elseif (strlen($userAccountID) == 0 && $btnSearch->SubmittedValue != "SEARCH")
            {
                $style = 'style="visibility:hidden"';
                echo $style;
            }
            ?> 
                 ><!--closing div-->

                <table width="50%">    
                    <tr>
                        <td colspan="2">
                            <div><h3 style="text-decoration: underline;" align="left">MANAGE USER ACCOUNT</h3></div>
                        </td>
                    </tr>
                    <tr style="height:30px;">
                        <td width="5%" align="left">USERNAME&nbsp;&nbsp;&nbsp;</td>
                        <td width="45%" style="border-style:solid;">&nbsp;&nbsp;&nbsp;<?php echo $userName; ?></td>
                    </tr>
                </table>
                <br>
                <table width="50%">    
                    <tr>
                        <td colspan="2">
                            <div><h3 style="text-decoration: underline;" align="left">ACCOUNT STATUS INFORMATION</h3></div>
                        </td>
                    </tr>
                    <tr style="height:30px;">
                        <td width="25%" align="left">CURRENT ACCOUNT STATUS</td>
                        <td width="25%" style="text-align: center; border-style:solid;"><?php
            if ($status == 1)
            {
                echo "ACTIVE";
            }
            if ($status == 2)
            {
                echo "SUSPENDED";
            }
            if ($status == 5)
            {
                echo "TERMINATED";
            }
            ?></td>
                    </tr>
                    <tr style="height:30px;">
                        <td width="25%" align="left">REMARKS</td>
                        <td width="25%" style="border-style:solid; text-align: center;">&nbsp;<?php echo $txtRemarks->Text; ?>&nbsp;</td>
                    </tr>

                </table>

                <br><br>
                <div align="center">
<?php
if ($status != 5)
{
    $btnEdit->Enabled = True;
    echo $btnEdit;
}
?>
                </div>

            </div><!--result div-->
        </div><!--main div-->
        <div id="light1"  class="white_content">
            <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black; padding-top: 0px;">
                <b id="errorHead"></b>
            </div>
            <br/>
            <p id="errorMsg"><br/></p>
            <br/>
            <p id="button">
        
                <button type="button" class="labelbutton_black" label="OKAY" onclick ="document.getElementById('light1').style.display = 'none';
            document.getElementById('fade').style.display = 'none';"/>OKAY</button>
            </p>
        </div>
        <div id="fade" class="black_overlay"></div>
    </form>
</body>

<?php include("footer.php"); ?>
<script type="text/javascript">

<?php if (strlen($errorTitle) > 0) : ?>
            document.getElementById('errorHead').innerHTML = '<?php echo $errorTitle; ?>';
            document.getElementById('errorMsg').innerHTML = '<?php echo $errorMessage; ?>';
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
<?php endif; ?>
</script>  



