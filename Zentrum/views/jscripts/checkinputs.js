function checklogin()
{
    var sUsername = document.getElementById("txtUsername");
    var sPassword = document.getElementById("txtPassword");
    alert(sUsername);
    sUsername = sUsername.trim();
    sPasword = sPassword.trim();

    if (sUsername.value.length == 0)
    {
        alert("Please enter your username.");
        sUsername.focus();
        return false;
    }
    else if (sPassword.value.length == 0)
    {
        alert("Please enter your password.");
        sPassword.focus();
        return false;
    }
    return true;
}

function processchangepassword(sid)
{
    document.getElementById('light12').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    document.getElementById('title2').innerHTML = "CONFIRMATION";
    document.getElementById('msg2').innerHTML = "You are about to change your current password. Do you wish to continue?";
}

function checkadduseracct()
{
    var sFName = document.getElementById("txtFName");
    var sMName = document.getElementById("txtMName");
    var sLName = document.getElementById("txtLName");
    var sEmail = document.getElementById("txtEmail");
    var sPosition = document.getElementById("txtPosition");
    var sCompany = document.getElementById("txtCompany");
    var sDepartment = document.getElementById("txtDepartment");
    var sGroup = document.getElementById("ddlGroup").options[document.getElementById("ddlGroup").selectedIndex].text;
    var sUName = document.getElementById("txtAuname");
    var sPWord = document.getElementById("txtPWord");
    var sCPWord = document.getElementById("txtCPWord");

    if (sUName.value.length == 0 && sPWord.value.length == 0 &&
            sCPWord.value.length == 0 && sFName.value.length == 0 &&
            sMName.value.length == 0 && sLName.value.length == 0 &&
            sEmail.value.length == 0 && sPosition.value.length == 0 &&
            sCompany.value.length == 0 && sDepartment.value.length == 0 && sGroup == "Select Group") {

        var msgtitle = "INVALID INPUT";
        var msg = "Please fill in all fields.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        sUName.focus();
        return false;
    }

    if (jQuery.trim(sUName.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the username.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sUName.focus();
        return false;
    }
    if (sUName.value.length < 6 || jQuery.trim(sUName.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Invalid Username.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sUName.focus();
        return false;
    }
    if (jQuery.trim(sPWord.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the password.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sPWord.focus();
        return false;
    }
    if (sPWord.value.length < 6 || jQuery.trim(sPWord.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Invalid Password.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sPWord.focus();
        return false;
    }
    if (jQuery.trim(sCPWord.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please confirm password.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sCPWord.focus();
        return false;
    }
    if (sPWord.value != sCPWord.value)
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Password you've entered do not match.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sCPWord.focus();
        return false;
    }
    if (jQuery.trim(sFName.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the first name.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sFName.focus();
        return false;
    }
    if (jQuery.trim(sMName.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the middle name.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sMName.focus();
        return false;
    }
    if (jQuery.trim(sLName.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the last name.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sLName.focus();
        return false;
    }
    if (jQuery.trim(sEmail.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the email address.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sLName.focus();
        return false;
    }
    if (!checkemail(sEmail))
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply valid email address.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sLName.focus();
        return false;
    }
    if (jQuery.trim(sPosition.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the position.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sPosition.focus();
        return false;
    }
    if (jQuery.trim(sCompany.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the company.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sCompany.focus();
        return false;
    }
    if (jQuery.trim(sDepartment.value) == '')
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the department.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDepartment.focus();
        return false;
    }
    if (sGroup == "Select Group")
    {
        var msgtitle = "INVALID INPUT";
        var msg = "Please supply the group type.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        document.getElementById("ddlGroup").focus();
        return false;
    }

    document.getElementById("hidGroup").value = document.getElementById("ddlGroup").options[document.getElementById("ddlGroup").selectedIndex].value;
    return true;
}

// date created june 29,2011
function showLightBoxMessage(msg) {
    var e = document.getElementById('frmTemplate');
    var conheight = e.offsetHeight;
    document.getElementById('fade').style.height = conheight;
    document.getElementById('light14').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    document.getElementById('title').innerHTML = "INVALID INPUT";
    document.getElementById('msg').innerHTML = msg;
}

function resetadduseracct()
{
    document.getElementById("txtFName").value = "";
    document.getElementById("txtMName").value = "";
    document.getElementById("txtLName").value = "";
    document.getElementById("txtEmail").value = "";
    document.getElementById("txtPosition").value = "";
    document.getElementById("txtCompany").value = "";
    document.getElementById("txtDepartment").value = "";
    document.getElementById("ddlGroup").value = "0";
    document.getElementById("txtAuname").value = "";
    document.getElementById("txtPWord").value = "";
    document.getElementById("txtCPWord").value = "";
}

function onchange_username()
{

    document.getElementById("hidUsername").value = document.getElementById("ddlUsername").options[document.getElementById("ddlUsername").selectedIndex].text;
    document.getElementById("hidAccountID").value = document.getElementById("ddlUsername").options[document.getElementById("ddlUsername").selectedIndex].value;

}


function checkedituseracct()
{
    var sFName = document.getElementById("txtFName");
    var sMName = document.getElementById("txtMName");
    var sLName = document.getElementById("txtLName");
    var sEmail = document.getElementById("txtEmail");
    var sPosition = document.getElementById("txtPosition");
    var sCompany = document.getElementById("txtCompany");
    var sDepartment = document.getElementById("txtDepartment");
    var sGroup = document.getElementById("ddlGroup").options[document.getElementById("ddlGroup").selectedIndex].text;

    if (sFName.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the first name.";
        sFName.focus();
        return false;
    }
    if (sMName.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the middle name.";
        sMName.focus();
        return false;
    }
    if (sLName.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the last name.";
        sLName.focus();
        return false;
    }
    if (sEmail.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the email address.";
        sLName.focus();
        return false;
    }
    if (!checkemail(sEmail))
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply valid email address.";
        sLName.focus();
        return false;
    }
    if (sPosition.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the position.";
        sPosition.focus();
        return false;
    }
    if (sCompany.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the company.";
        sCompany.focus();
        return false;
    }
    if (sDepartment.value.length == 0)
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please supply the department.";
        sDepartment.focus();
        return false;
    }
    if (sGroup == "Select Group")
    {
        document.getElementById('light21').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please choose the group type.";
        document.getElementById("ddlGroup").focus();
        return false;
    }

    document.getElementById("hidGroup").value = document.getElementById("ddlGroup").options[document.getElementById("ddlGroup").selectedIndex].value;
    document.getElementById("hidUsername").value = document.getElementById("ddlUsername").options[document.getElementById("ddlUsername").selectedIndex].value;
    return true;
}

function checkupdateacctstatus()
{
    var sRemarks = document.getElementById("txtRemarks");
    var sStatus = document.getElementById("ddlStatus").options[document.getElementById("ddlStatus").selectedIndex].text;

    if (sStatus == "----")
    {
        document.getElementById('light15').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please choose the account status.";
        document.getElementById("ddlStatus").focus();
        return false;
    }
    if (sRemarks.value.length == 0)
    {
        document.getElementById('light15').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "INVALID INPUT";
        document.getElementById('msg2').innerHTML = "Please enter the reason for changing the account status.";
        sRemarks.focus();
        return false;
    }

    document.getElementById('light8').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    document.getElementById("hidStatus").value = document.getElementById("ddlStatus").options[document.getElementById("ddlStatus").selectedIndex].value;
    document.getElementById("hidStatusval").value = document.getElementById("ddlStatus").options[document.getElementById("ddlStatus").selectedIndex].text;
    return true;
}

function checkinput30()
{
    var sDateFrom = document.getElementById("txtDateFr");
    var sDateTo = document.getElementById("txtDateTo");
    var sAccountId = document.getElementById("ddlPosName").value;
    var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;

    if (sDateFrom.value.length == 0)
    {
        document.getElementById('msg').innerHTML = "";
        var msg = "Please enter the start date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value) == false)
    {
        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {
        var msg = "Please enter the end date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value) == false)
    {
        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    var from = sDateFrom.value;
    var to = sDateTo.value;
    from = from.split('/', 3);
    from = from[2] + from[0] + from[1];
    to = to.split('/', 3);
    to = to[2] + to[0] + to[1];
    if (to < from)
    {
        var msg = "Invalid date range.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sAccountId == "---")
    {
        var msg = "Insufficient Data ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
    }
    if (sAccountId == "")
    {
        var msg = " Insufficient Data";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        return false;
    }

    return true;
}


function checkinput()
{
    var sDateFrom = document.getElementById("txtDateFr");
    var sDateTo = document.getElementById("txtDateTo");
    var sAccountId = document.getElementById("ddlPos").value;
    var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;

    if (sDateFrom.value.length == 0)
    {
        document.getElementById('msg').innerHTML = "";
        var msg = "Please enter the start date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value) == false)
    {
        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {
        var msg = "Please enter the end date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value) == false)
    {
        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }

    var from = sDateFrom.value;
    var to = sDateTo.value;
    from = from.split('/', 3);
    from = from[2] + from[0] + from[1];
    to = to.split('/', 3);
    to = to[2] + to[0] + to[1];

    if (to < from)
    {
        var msg = "Invalid date range.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sAccountId == "---")
    {
        var msg = "Insufficient Data ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
    }
    if (sAccountId == "")
    {
        var msg = " Insufficient Data";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        return false;
    }

    return true;
}
function checkinput20()
{
    var sDateFrom = document.getElementById("txtDateFr");
    var sDateTo = document.getElementById("txtDateTo");
    var sAccountId = document.getElementById("ddlPos").value;
    var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;
    if (sDateFrom.value.length == 0)
    {

        document.getElementById('msg').innerHTML = "";
        var msg = "Please enter the start date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);

        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value) == false)
    {
        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {
        var msg = "Please enter the end date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value) == false)
    {
        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }

    var from = sDateFrom.value;
    var to = sDateTo.value;
    from = from.split('/', 3);
    from = from[2] + from[0] + from[1];
    to = to.split('/', 3);
    to = to[2] + to[0] + to[1];

    if (to < from)
    {
        var msg = "Invalid date range.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }

    return true;
}

function num_only()
{
    if (event.keyCode < 47 || event.keyCode > 57)
        return false;
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isLetter(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode < 31 && (charCode > 48 || charCode < 57))
        return false;

    return true;
}

function checkemail(email)
{
    var str = email.value
    var filter = /^.+@.+\..{2,3}$/

    if (filter.test(str))
        return true;
    else
    {
        document.getElementById("txtEmail").focus();
        return false;
    }
    return true;
}

/*Added by Arlene*/
function checkUsername(lightbox)
{
    if (document.getElementById('ddlUsername').value == 0)
    {
        document.getElementById(lightbox).style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }
    else
    {
        return true;
    }
}

function redirect2(page)
{
    window.location = 'template.php?page=' + page;
}

function sample()
{
    alert("ok");
}

function success()
{
    document.getElementById('light17').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    document.getElementById('title2').innerHTML = "INVALID INPUT";
    document.getElementById('msg2').innerHTML = "New user account has been successfully created. Log in credentials have been sent to registered email address.";
}

function redirectUserAccount()
{
    window.location = 'template.php?page=useracctprofile';
}

function numeric(evt)
{
    keyHit = evt.which;
    NumericCode = "61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122";
    if (NumericCode.indexOf(keyHit) < 0)
    {
        return false;
    }
    return true;
}

function checkrptinput()
{
    var sDateFrom = document.getElementById("txtDateFr");
    var sDateTo = document.getElementById("txtDateTo");
    var sAccountId = document.getElementById("ddlPos").value;
    var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;

    if (sDateFrom.value.length == 0)
    {
        var msg = "Please enter the start date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {
        var msg = "Please enter the end date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    var from = sDateFrom.value;
    var to = sDateTo.value;
    from = from.split('/', 3);
    from = from[2] + from[0] + from[1];
    to = to.split('/', 3);
    to = to[2] + to[0] + to[1];
    if (to < from)
    {
        var msg = "Invalid date range.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }

    if (sAccountId == "---" || sAccountId == "")
    {

        var msg = "Insufficient Data ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
    }

    return true;
}

function checkvoucherinput()
{
    var sDateFrom = document.getElementById("txtDateFr");
    var sDateTo = document.getElementById("txtDateTo");
    var sAccountId = document.getElementById("ddlPos").value;
    var sVoucherCode = document.getElementById("vchrcode").value;
    var sVoucherUsage = document.getElementById("vchrusage").value;
    var re = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d+$/;

    if (sDateFrom.value.length == 0)
    {

        var msg = "Please enter the start date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (re.test(sDateFrom.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }
    if (sDateTo.value.length == 0)
    {

        var msg = "Please enter the end date. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    if (re.test(sDateTo.value) == false)
    {

        var msg = "Date must be mm/dd/yyyy format. ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateTo.focus();
        return false;
    }
    var from = sDateFrom.value;
    var to = sDateTo.value;
    from = from.split('/', 3);
    from = from[2] + from[0] + from[1];
    to = to.split('/', 3);
    to = to[2] + to[0] + to[1];
    if (to < from)
    {
        var msg = "Invalid date range.";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        sDateFrom.focus();
        return false;
    }

    if (sAccountId == "" || sAccountId == "---")
    {

        var msg = "Insufficient Data ";
        var msgtitle = "INVALID INPUT";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        return false;
    }

    return true;
}

function validateEmail(elementValue)
{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(elementValue);
}

/**************************************************
 * Author: Noel Antonio
 * Date Created: April 3, 2013
 * Description: Wrapper for document.getElementById
 *************************************************/
function elem(id)
{
    return document.getElementById(id);
}

/**************************************************
 * Author: Noel Antonio
 * Date Created: April 3, 2013
 * Parameter: string lblId
 * Parameter: string inputId
 * Description:  
 *************************************************/
function addOrRemoveErrorFieldClass(lblId, inputId)
{
    var lblElem = elem(lblId);

    if (inputId == '' || inputId == 0) {
        addClass(lblElem, "errorField");
    } else {
        removeClass(lblElem, "errorField");
    }
}

/********************************************
 * Author: Noel Antonio
 * Date Created: April 3, 2013
 * Parameter: object ele (element)
 * Parameter: string cls (class name)
 * Description: Check if element has this cls
 ********************************************/
function hasClass(ele, cls)
{
    return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

/**************************************
 * Author: Noel Antonio
 * Date Created: April 3, 2013
 * Parameter: object ele (element)
 * Parameter: string cls (class name)
 * Description: Remove class name (cls)
 **************************************/
function removeClass(ele, cls)
{
    if (hasClass(ele, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg, ' ');
    }
}

/************************************
 * Author: Noel Antonio
 * Date Created: April 3, 2013
 * Parameter: object ele (element)
 * Parameter: string cls (class name)
 * Description: Add class name (cls)
 ************************************/
function addClass(ele, cls) {
    if (!this.hasClass(ele, cls))
        ele.className += " " + cls;
}

function atleastOne()
{
    var passport = elem('txtpassport');
    var dlisnse = elem('txtdriver');
    var sss = elem('txtsss');
    var others = elem('txtothers');
    var please = elem('please');
    if (passport.value.trim() == '' && dlisnse.value.trim() == '' && sss.value.trim() == '' && others.value.trim() == '') {
        addClass(please, 'errorField');
    } else {
        removeClass(please, 'errorField');
    }
}

/** added by bryan **/
function setFocusAndReturnFalse(elem)
{
    var e = document.getElementById('frmTemplate');
    var conheight = e.offsetHeight;
    document.getElementById('fade').style.height = conheight;
    html = '<div class=\"titleLightbox\">Notification<\/div>';
    html += '<div class=\"msgLightbox\">';
    html += '<br /><br /><p>Please fill in all required fields</p><br /><br />';
    html += '<input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"><\/input>';
    html += '</div>';

    $('#frmTemplate').append('<div id="light" class="white_content2"></div>');
    $('.white_content2').html(html);

    $('#fade').show();
    $('.white_content2').show();
    elem.select();
    elem.focus();
    return false;
}

function checkinput_create_pos_acct()
{
    var validagent = document.getElementById('hiddenvalidauname').value;
    var sUname = elem("txtuname");
    var sfname = elem("txtfname");
    var smname = elem("txtmname");
    var slname = elem("txtlname");
    var sscode = elem("txtscode");
    var sAuname = elem("txtAuname");
    var shadd = elem("txthadd");
    var szip = elem("txtzip");
    var stelno = elem("txttelno");
    var semail = elem("txtemail");
    var sbday = elem("txtbday");
    var spassport = elem("txtpassport");
    var sdlisnse = elem("txtdriver");
    var ssss = elem("txtsss");
    var sothers = elem("txtothers");

    addOrRemoveErrorFieldClass("lblCashier", "txtuname");
    addOrRemoveErrorFieldClass("lblFname", "txtfname");
    addOrRemoveErrorFieldClass("lblMname", "txtmname");
    addOrRemoveErrorFieldClass("lblLname", "txtlname");
    addOrRemoveErrorFieldClass("lblSwc", "txtscode");
    addOrRemoveErrorFieldClass("lblAus", "txtAuname");
    addOrRemoveErrorFieldClass("lblHomeAddr", "txthadd");
    addOrRemoveErrorFieldClass("lblZipcode", "txtzip");
    addOrRemoveErrorFieldClass("lblTel", "txttelno");
    addOrRemoveErrorFieldClass("lblEmail", "txtemail");
    addOrRemoveErrorFieldClass("lblBday", "txtbday");
    addOrRemoveErrorFieldClass("lblPassport", "txtpassport", "chkpass");
    addOrRemoveErrorFieldClass("lblDriverLicense", "txtdriver", "chkdriver");
    addOrRemoveErrorFieldClass("lblSss", "txtsss", "chksss");
    addOrRemoveErrorFieldClass("lblOthers", "txtothers", "chkothers");

    var lblElem = elem('please');
    removeClass(lblElem, "errorField");

    atleastOne();

    if (sUname.value.length <= 7)
    {
        return setFocusAndReturnFalse(sUname);
    }
    else if (sfname.value.trim() == '')
    {
        return setFocusAndReturnFalse(sfname);
    }
    else if (smname.value.trim() == '')
    {
        return setFocusAndReturnFalse(smname);
    }
    else if (slname.value.trim() == '')
    {
        return setFocusAndReturnFalse(slname);
    }
    else if (sscode.value.trim() == '')
    {
        return setFocusAndReturnFalse(sscode);
    }
    else if (sAuname.value.trim() == '')
    {
        return setFocusAndReturnFalse(sAuname);
    }
    else if (shadd.value.trim() == '')
    {
        return setFocusAndReturnFalse(shadd);
    }
    else if (szip.value.trim() == '')
    {
        return setFocusAndReturnFalse(szip);
    }
    else if (stelno.value.trim() == '')
    {
        return setFocusAndReturnFalse(stelno);
    }
    else if (semail.value.trim() == '')
    {
        return setFocusAndReturnFalse(semail);
    }
    else if (!validateEmail(semail.value.trim()))
    {
        return setFocusAndReturnFalse(semail);
    }
    else if (trim(sbday.value.trim()) == '')
    {
        return setFocusAndReturnFalse(sbday);
    }
    else if (isChecked("chkpass") && spassport.value.trim() == '') {
        return setFocusAndReturnFalse(spassport);
    }
    else if (isChecked("chkdriver") && sdlisnse.value.trim() == '') {
        return setFocusAndReturnFalse(sdlisnse);
    }
    else if (isChecked("chksss") && ssss.value.trim() == '') {
        return setFocusAndReturnFalse(ssss);
    }
    else if (isChecked("chkothers") && sothers.value.trim() == '') {
        return setFocusAndReturnFalse(sothers);
    }
    if (spassport.value.length == 0 && sdlisnse.value.trim() == '' && ssss.value.trim() == '' && sothers.value.trim() == '') {
        addClass(lblElem, "errorField");
        return setFocusAndReturnFalse(spassport);
    } else {
        if (sAuname.value.length < 8 && sAuname.value.length != 0)
        {
            var msgtitle = "ERROR";
            var msg = "Agent Username should not be less than 8 character.";
            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
            html += '<div class=\"msgLightbox\">';
            html += '<p>' + msg + '</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
            html += '</div>';
            showLightBox(html);
            return false;
        } else if (validagent == "notvalid") {
            var msgtitle = "ERROR";
            var msg = "Please supply a valid agent username.";
            html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
            html += '<div class=\"msgLightbox\">';
            html += '<p>' + msg + '</p>';
            html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
            html += '</div>';
            showLightBox(html);
            return false;
        }
        else {
            document.getElementById('hidden').value = 1;
            document.forms[0].submit();
            return true;
        }
    }
}

/** add by bryan **/
function isChecked(chkId)
{
    var chkElem = elem(chkId);
    if (chkElem.checked) {
        return true;
    }
    return false;
}

function checkinputvoucherC()
{
    if (document.getElementById("vouchercde").value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0 && document.getElementById("ddldenom").value == 0)
    {
        document.getElementById('light24').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Please select at least one.";
        return false;
    }
    return true;
}

function checkinputvoucherC2()
{
    if (document.getElementById("remarks").value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
    {
        document.getElementById('light24').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "Please input remarks.";
        return false;
    }
    return true;

}

function checkinputvoucherLO()
{
    if (document.getElementById("ternme").value == '' || document.getElementById("ddlPos").value == 0)
    {
        document.getElementById('light24').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title').innerHTML = "INVALID INPUT";
        document.getElementById('msg').innerHTML = "All fields are required.";
        return false;
    }
    return true;
}

/*
 *   Added by: Angelo Niño G. Cubos
 *   Date created: November 9, 2012 
 */
function AlphaNumericOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if (/*DisableWhiteSpaces*/(charCode == 32) || /*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;

    return true;
}
function AlphaOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if (/*DisableWhiteSpaces*/(charCode == 32) || /*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
    // returns false if a numeric character has been entered
    return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */);
    return (chCode == 32);

    return true;
}
function AlphaAndSpaces(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if (/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
    // returns false if a numeric character has been entered
    return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */);
    return (chCode == 32);

    return true;
}
function OnlyNumbers(event)
{
    var e = event || evt; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;

}
function verifyEmail(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode == 64) || (charCode == 95) || (charCode == 46))/* @=64, _=95, .=46 characters are allowed*/
    {
        return true;
        return true;
    } else {
        if (/*DisableWhiteSpaces*/(charCode == 32) || /*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
            return false;
        return true;
    }
}
function DisableSpacesOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if (/*DisableSpaces*/(charCode == 32))
        return false;

    return true;
}

function chkuname()
{
    var uname = document.getElementById("txtAuname");
    if (uname.value.length < 8 && uname.value.length != 0)
    {
        var msgtitle = "ERROR";
        var msg = "Agent Username should not be less than 8 character.";
        html = '<div class=\"titleLightbox\">' + msgtitle + '</div><br />';
        html += '<div class=\"msgLightbox\">';
        html += '<p>' + msg + '</p>';
        html += '<br /><input id=\"btnOkay\" type=\"button\" value=\"OKAY\" class=\"labelbutton2\"></input>';
        html += '</div>';
        showLightBox(html);
        $('#txtAuname').css('background-color', 'red');
    }
    else if (uname.value.length == 0) {
        $('#txtAuname').css('background-color', '');
    }
    else {
        verify_agentuname()
    }
}

/*
 * Author       :   Noel Antonio
 * Date Created :   April 3, 2013
 * Description  :   Validate email address
 */
function echeck(str)
{
    var at = "@"
    var dot = "."
    var lat = str.indexOf(at)
    var lstr = str.length
    var ldot = str.indexOf(dot)

    if (str.indexOf(at) == -1) {
        return false;
    }

    if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
        return false;
    }

    if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
        return false;
    }

    if (str.indexOf(at, (lat + 1)) != -1) {
        return false;
    }

    if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
        return false;
    }

    if (str.indexOf(dot, (lat + 2)) == -1) {
        return false;
    }

    if (str.indexOf(" ") != -1) {
        return false;
    }

    return true;
}

/*
 * Author       :   Noel Antonio
 * Date Created :   April 3, 2013
 * Description  :   Validate registration form
 */
function checkRegistrationInputs()
{
    var lname = document.getElementById("txtLastName").value;
    var fname = document.getElementById("txtFirstName").value;
    var idno = document.getElementById("txtIDNo").value;
    var others = document.getElementById("txtOthers").value;
    var email = document.getElementById("txtEmail").value;
    var sex = document.getElementById("ddlSex").options[document.getElementById("ddlSex").selectedIndex].value;
    var idpr = document.getElementById("ddlIdPresented").options[document.getElementById("ddlIdPresented").selectedIndex].value;
    var status = document.getElementById("ddlStatus").options[document.getElementById("ddlStatus").selectedIndex].value;
    //var imgpath = document.getElementById("hidImgPath").value;
    var bday = document.getElementById("hidBday").value;
    var date_array = bday.split("-");
    var month = date_array[1];
    var day = date_array[2];
    var year = date_array[0];

    email = email.replace(/^\s+|\s+$/, '');
    var regEx = /[a-zA-Z0-9]/;

    fname = $.trim(fname);
    lname = $.trim(lname);
    idno = $.trim(idno);
    others = $.trim(others);

    addOrRemoveErrorFieldClass("lblfn", fname);
    addOrRemoveErrorFieldClass("lblln", lname);
    addOrRemoveErrorFieldClass("lblidno", idno);
    addOrRemoveErrorFieldClass("lblbday", month);
    addOrRemoveErrorFieldClass("lblbday", day);
    addOrRemoveErrorFieldClass("lblbday", year);
    addOrRemoveErrorFieldClass("lblsex", sex);
    addOrRemoveErrorFieldClass("lblidpr", idpr);
    addOrRemoveErrorFieldClass("lblstatus", status);
    //addOrRemoveErrorFieldClass("lblpic", imgpath);

    document.getElementById('errorHead').innerHTML = "INCOMPLETE INFORMATION";
    if ((fname.length == 0) || (fname == "") || (lname.length == 0) || (lname == "") || (month == 00 || day == 00 || year == 0000) || (sex == 0) || (idpr == 0 || (idpr == 4 && others == ""))
       || (idno.length == 0) || (idno == "") || (status == ''))
    {
        document.getElementById('errorMsg').innerHTML = "Kindly fill out all required fields.";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    if (month != 00 && day != 00 && year != 0000)
    {
        if (calculateAge(new Date(year, month - 1, day)) < 17) {
            document.getElementById('errorMsg').innerHTML = "Age below 17 years old is prohibited.";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
    }

    if (email != "" && echeck(email) == false)
    {
        document.getElementById('errorMsg').innerHTML = "Please enter a valid Email Address.";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    
    /*if ((imgpath == 0) || (imgpath == ""))
    {
        document.getElementById('errorMsg').innerHTML = "Please take or upload an image file.";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }*/

    document.getElementById('errorHead3').innerHTML = "CONFIRMATION";
    document.getElementById('errorMsg3').innerHTML = "You are about to create a new membership account. Do you wish to continue?";
    document.getElementById('light3').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    return true;
}

/*
 * Author       :   Noel Antonio
 * Date Created :   April 23, 2013
 * Description  :   Only allows alphanumeric keys and space.
 */
function AlphaNumericAndSpacesOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;
    if (/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;

    return true;
}

/*
 * Author       :   Frances Ralph Sison
 * Date Created :   April 25, 2013
 */
function editMemberInfo(id)
{
    document.getElementById("hidMemberInfoID").value = id;
    document.forms["FrmMemberInfo"].submit();
}

function editMemberProfile(id)
{
    document.getElementById("hiddenId").value = id;
    document.forms["frmUserProfile"].submit();
}

/*
 * Author       :   Noel Antonio
 * Date Created :   April 26, 2013
 * Description  :   Calculate age
 */
function calculateAge(dateString) {

    var birthday = new Date(dateString);
    var datenow = new Date().valueOf();
    return ~~((datenow - birthday) / (31557600000));
}

function checkeditmemberinputs()
{
    var lname = document.getElementById("txtLName").value;
    var fname = document.getElementById("txtFName").value;
    var idno = document.getElementById("txtIDnum").value;
    //var others = document.getElementById("txtothers").value;
    var email = document.getElementById("txtEmail").value;
    var month = document.getElementById("ddlMonth").options[document.getElementById("ddlMonth").selectedIndex].value;
    var day = document.getElementById("ddlDay").options[document.getElementById("ddlDay").selectedIndex].value;
    var year = document.getElementById("ddlYear").options[document.getElementById("ddlYear").selectedIndex].value;
    var sex = document.getElementById("ddlGender").options[document.getElementById("ddlGender").selectedIndex].value;
    var idpr = document.getElementById("ddlIDpresented").options[document.getElementById("ddlIDpresented").selectedIndex].value;
    var status = document.getElementById("ddlMemberStatus").options[document.getElementById("ddlMemberStatus").selectedIndex].value;
    var imgpath = document.getElementById("hidimgpath").value;

    email = email.replace(/^\s+|\s+$/, '');
    var regEx = /[a-zA-Z0-9]/;

    fname = $.trim(fname);
    lname = $.trim(lname);
    idno = $.trim(idno);
    // others = $.trim(others);

    addOrRemoveErrorFieldClass("lblfn", fname);
    addOrRemoveErrorFieldClass("lblln", lname);
    addOrRemoveErrorFieldClass("lblidno", idno);
    addOrRemoveErrorFieldClass("lblbday", month);
    addOrRemoveErrorFieldClass("lblbday", day);
    addOrRemoveErrorFieldClass("lblbday", year);
    addOrRemoveErrorFieldClass("lblsex", sex);
    addOrRemoveErrorFieldClass("lblidpr", idpr);
    addOrRemoveErrorFieldClass("lblstatus", status);
    addOrRemoveErrorFieldClass("lblpic", imgpath);

    document.getElementById('errorHead').innerHTML = "INCOMPLETE INFORMATION";
    if ((fname.length == 0) || (fname == "") || (lname.length == 0) || (lname == "") || (month == 0 || day == 0 || year == 0) || (sex == 0) || (idpr == 0 || (idpr == 4 && others == ""))
       || (idno.length == 0) || (idno == ""))
    {
        document.getElementById('errorMsg').innerHTML = "Kindly fill out all required fields.";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    if (month != 0 && day != 0 && year != 0)
    {
        if (calculateAge(new Date(year, month - 1, day)) < 17) {
            document.getElementById('errorMsg').innerHTML = "Age below 17 years old is prohibited.";
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            return false;
        }
    }

    if (email != "" && echeck(email) == false)
    {
        document.getElementById('errorMsg').innerHTML = "Please enter a valid Email Address.";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        return false;
    }

    /*  if (status == 0)
     {
     document.getElementById('errorMsg').innerHTML = "Please enter the Status.";
     document.getElementById('light1').style.display='block';
     document.getElementById('fade').style.display='block';
     return false;
     }
     */

    document.getElementById('errorHead3').innerHTML = "CONFIRMATION";
    document.getElementById('errorMsg3').innerHTML = "You are about to change details in the membership account. Do you wish to continue?";
    document.getElementById('light3').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    return true;
}
/*
 *   Added by: Angelo Niño G. Cubos
 *   Date created: June 4, 2013 
 */
function AlphaOnlyForFirstName(event)
{
    var sFName = document.getElementById("txtFirstName");
    
    //Validation for First Name    
    if(sFName.value.length === 0 || sFName.value.slice(-1) === " ")
    {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode == 32) || (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        return (chCode < 48 || chCode > 57);
        return (chCode == 32);
    return true;         
    }else{
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        return (chCode < 48 || chCode > 57 );
        return (chCode == 32);
    return true;
    }       
}
/*
 *   Added by: Angelo Niño G. Cubos
 *   Date created: June 4, 2013 
 */
function AlphaOnlyForMiddleName(event)
{
    var sMName = document.getElementById("txtMiddleName");

    //Validation for Middle Name    
    if(sMName.value.length === 0 || sMName.value.slice(-1) === " ")
    {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode == 32) || (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        return (chCode < 48 || chCode > 57);
        return (chCode == 32);
    return true;         
    }else{
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        return (chCode < 48 || chCode > 57 );
        return (chCode == 32);
    return true;
    }        
}
/*
 *   Added by: Angelo Niño G. Cubos
 *   Date created: June 4, 2013 
 */
function AlphaOnlyForLastName(event)
{
    var sLName = document.getElementById("txtLastName");

    //Validation for Last Name
    if(sLName.value.length === 0 || sLName.value.slice(-1) === " ")
    {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode == 32) || (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        return (chCode < 48 || chCode > 57);
        return (chCode == 32);
    return true;         
    }else{
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
        return (chCode < 48 || chCode > 57 );
        return (chCode == 32);
    return true;
    }       
}

/*
 *   Added by: Ralph Sison
 *   Date created: August 27, 2013 
 */

function checkvalidation()
{
    var searchby = document.getElementById("filterSearch");
    var searchfield = document.getElementById("searchField");
    
    if(searchby.value == '0')
    {
         document.getElementById('errorHead').innerHTML = "ERROR!";
         document.getElementById('errorMsg').innerHTML = "No search by filter selected.";
         //document.getElementById('button').innerHTML = '<button type="button" class="labelbutton_black" onclick="window.location = \'filterandviewmembers.php\';">OKAY</button> ';
         //document.getElementById('button').innerHTML = '<input type="button" class="labelbutton_black" ';
         document.getElementById('light1').style.display='block';
         document.getElementById('fade').style.display='block';
         return false;
    }
    else if(searchby.value == '1' && searchfield.value == '')
    {
         document.getElementById('errorHead').innerHTML = "ERROR!";
         document.getElementById('errorMsg').innerHTML = "No filter inputted.";
         //document.getElementById('button').innerHTML = '<button type="button" class="labelbutton_black" onclick="window.location = \'filterandviewmembers.php\';">OKAY</button> ';
         //document.getElementById('button').innerHTML = '<input type="button" class="labelbutton_black" ';
         document.getElementById('light1').style.display='block';
         document.getElementById('fade').style.display='block';
         return false;
    }
    else if(searchby.value == '2' && searchfield.value == '')
    {
         document.getElementById('errorHead').innerHTML = "ERROR!";
         document.getElementById('errorMsg').innerHTML = "No filter inputted.";
         //document.getElementById('button').innerHTML = '<button type="button" class="labelbutton_black" onclick="window.location = \'filterandviewmembers.php\';">OKAY</button> ';
         //document.getElementById('button').innerHTML = '<input type="button" class="labelbutton_black" ';
         document.getElementById('light1').style.display='block';
         document.getElementById('fade').style.display='block';
         return false;
    }
    else if(searchby.value == '2' && searchfield.value != '')
    {
        var searchtolower = searchfield.value.toLowerCase();
        if(searchfield.value.length < 6)
        {
            document.getElementById('errorHead').innerHTML = "ERROR!";
            document.getElementById('errorMsg').innerHTML = "Invalid Card Number.";
            //document.getElementById('button').innerHTML = '<button type="button" class="labelbutton_black" onclick="window.location = \'filterandviewmembers.php\';">OKAY</button> ';
            //document.getElementById('button').innerHTML = '<input type="button" class="labelbutton_black" ';
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        else if(searchfield.value.length == 6 && searchtolower.charAt(0) != 'a')
        {
            document.getElementById('errorHead').innerHTML = "ERROR!";
            document.getElementById('errorMsg').innerHTML = "Invalid Card Number.";
            //document.getElementById('button').innerHTML = '<button type="button" class="labelbutton_black" onclick="window.location = \'filterandviewmembers.php\';">OKAY</button> ';
            //document.getElementById('button').innerHTML = '<input type="button" class="labelbutton_black" ';
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
        else if(searchfield.value.length > 6)
        {
            document.getElementById('errorHead').innerHTML = "ERROR!";
            document.getElementById('errorMsg').innerHTML = "Invalid Card Number.";
            //document.getElementById('button').innerHTML = '<button type="button" class="labelbutton_black" onclick="window.location = \'filterandviewmembers.php\';">OKAY</button> ';
            //document.getElementById('button').innerHTML = '<input type="button" class="labelbutton_black" ';
            document.getElementById('light1').style.display='block';
            document.getElementById('fade').style.display='block';
            return false;
        }
         
    }
}
