<?php
/*
 * Created by : Angelo Cubos
 * Date Created : April 01 2013
 * Purpose : view create user account
 */

require_once("../controller/createuseraccountprocess.php");
require_once("header.php");
?>

<style type="text/css">
    input
    {
        width: 300px;
    }
</style>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<script language="javascript" src="jscripts/checkinputs.js"></script>
<body ondragstart="return false;" ondrop="return false;">
    <form id="frmCreateAcct" method="post">
        <?php echo $hiddenUname;?>
        <br>
        <div align ="center">
            <table width="50%" border="0">
                <tr>
                    <th colspan ="2">CREATE USER ACCOUNT</td>
                </tr>
                <tr>
                    <td width="20%" align="left">Username</td>
                    <td width="40%" align="center"><?php echo $txtUserName; ?></td>
                </tr>
            </table>
            <br>    
            <table width="50%" border="0">    
                <tr>
                    <th colspan ="2">ACCOUNT INFORMATION</td>
                </tr>
                <tr>
                    <td width="20%" align="left">First Name</td>
                    <td width="40%" align="center"><?php echo $txtFirstName; ?></td>
                </tr>
                <tr>
                    <td width="20%" align="left">Middle Name</td>
                    <td width="40%" align="center"><?php echo $txtMiddleName; ?></td>
                </tr>
                <tr>
                    <td width="20%" align="left">Last Name</td>
                    <td width="40%" align="center"><?php echo $txtLastName; ?></td>
                </tr>
                <tr>
                    <td width="20%" align="left">Email Address</td>
                    <td width="40%" align="center"><?php echo $txtEmail; ?></td>
                </tr>
                <tr>
                    <td width="20%" align="left">Position</td>
                    <td width="40%" align="center"><?php echo $txtPosition; ?></td>
                </tr>    
                <tr>
                    <td width="20%" align="left">Account Type</td>
                    <td width="40%" align="center"><?php echo $ddlAccType; ?></td>
                </tr>    
            </table>
        </div>
        <br><br>
        <div align ="center">
            <table width="50%" border="0">
                <tr>
                <div class="tago">
                    <td class="fontboldred" colspan ="2" align="center">
                        All fields are required.
                    </td>
                </div>
                </tr>
                <tr>
                    <td align="center">
                        <?php echo $btnSubmit; ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php echo $btnCancel; ?>
                    </td>
                </tr>
            </table>    
        </div>

        <div id="light1"  class="white_content">
        <div style="width: 100%;height: 27px;background-color: #a6a6a6;
                            top: 0px;color: black; padding-top: 0px;">
            <b id="errorHead"></b>
        </div>
            <br>
            <p id="errorMsg"><br></p>
            <br>
            <p id="button">
                <?php echo $btnHideMsg; ?>
            </p>
        </div>
        <div id="fade" class="black_overlay"></div>
    </form>
</body>    
<?php include("footer.php"); ?>

<script>
function filluname()
{
    var fname = document.getElementById("txtFirstName").value;
    var mname = document.getElementById("txtMiddleName").value;
    var lname = document.getElementById("txtLastName").value;
    var uname = fname.substring(0,1)+mname.substring(0,1)+lname;
    uname = uname.replace(/\s+/g, '');
    
    document.getElementById("hiddenUname").value = uname;
    document.getElementById("txtUserName").value = uname.toLowerCase();    
}
    $(function() {
        $('input[type=text],[type=password]').bind('cut copy paste', function(e) {
            e.preventDefault();
        });
    });
</script>

<script type="text/javascript">

<?php if (strlen($errorTitle) > 0) : ?>
    document.getElementById('errorHead').innerHTML = '<?php echo $errorTitle; ?>';
    document.getElementById('errorMsg').innerHTML = '<?php echo $errorMessage; ?>';
    <?php if ($email_sent) : ?>
    document.getElementById('button').innerHTML = 
    '<button type="button" class="labelbutton_black" \n\
    onclick="window.location = \'useraccountprofile.php\';">OKAY</button> ';
    <?php endif; ?>
    document.getElementById('light1').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
<?php endif; ?>
<?php if (strlen($confirmTitle) > 0) : ?>
    document.getElementById('errorHead').innerHTML = '<?php echo $confirmTitle; ?>';
    document.getElementById('errorMsg').innerHTML = '<?php echo $confirmMessage; ?>';
    document.getElementById('button').innerHTML = 
    '<?php echo addslashes($btnProceed); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\
    <button type="button" class="labelbutton_black" \n\
    onclick="document.getElementById(\'light1\').style.display=\'none\';\n\
    document.getElementById(\'fade\').style.display=\'none\';">CANCEL</button> ';
    document.getElementById('light1').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
<?php endif; ?>
</script>            
