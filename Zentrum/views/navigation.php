<?php
/*
 * @author 
 * Purpose   : view for navigation
 */

require_once("../../init.inc.php");

$request_url=$_SERVER['REQUEST_URI'];
$pathinfo = pathinfo($request_url);
$file = explode(".", $pathinfo['basename']);
$pagename = $file[0];

$menus = $_SESSION["usermenus"];
App::LoadModuleClass("MembershipTimor", "MTSubMenus");
$selectedmenuid = 0;
$accounttypeid = $_SESSION['accttype'];
$navigationsubmenu = new MTSubMenus();
$arrnavsubmenus = $navigationsubmenu->GetGroupSubMenusBySubMenuID($accounttypeid);

//app::pr($arrnavsubmenus);exit();
?>

<script type='text/javascript' src='jscripts/jquery-1.5.2.min.js'></script>
<script type="text/javascript">
function mainmenu(){
$(" #nav ul ").css({display: "none"}); // Opera Fix
$(" #nav li").hover(function(){
		$(this).find('ul:first').css({visibility: "visible",display: "none"}).show(400);
		},function(){
		$(this).find('ul:first').css({visibility: "hidden"});
		});
}

 
 
 $(document).ready(function(){					
	mainmenu();
});
</script>

<style type="text/css">
body{
font-size:0.85em;
font-family:Verdana, Arial, Helvetica, sans-serif;
}

#nav, #nav ul{
margin:0;
padding:0;
list-style-type:none;
list-style-position:outside;
position:relative;
background-color: black;
line-height: 28px;
z-index: 1000;
}

#nav a{
color:whitesmoke;
font-size:14px;
font-weight:bold;
text-decoration:none;
padding:0 20px 0 6px;
display:block;
text-decoration:none;
background-color:black;
}

#nav ul a:hover{
background-color:white;
border: solid 1px gray;
color:black;
}
#nav li{
float:left;
position:relative;
display: inline-block;  border-left:1px solid #FFFFFF;
}
#nav li:first-child{border-left:none;}
#nav ul {
position:absolute;
display:none;
width:12em;
top:1.5em;

}

#nav li ul a{
width:12em;
height:auto;
float:left;
border: solid 1px grey;
}

#nav ul ul{
top:auto;
}	

#nav li ul ul {
left:12em;
margin:0px 0 0 10px;
border: solid 1px #2E4B88;
}

#nav li:hover ul ul, #nav li:hover ul ul ul, #nav li:hover ul ul ul ul{
display:none;
}
#nav li:hover ul, #nav li li:hover ul, #nav li li li:hover ul, #nav li li li li:hover ul{
display:block;
}
.test{
color:whitesmoke;
font-size:14px;
font-weight:bold;
text-decoration:none;
padding:0 20px 0 6px;
display:block;
}
#nav #selectedsubmenu  
{
    display: block;
    height: 26px;
    color: yellow;
    background-color: #c0c0c0;
    font-weight: bold;
}
</style>
<body>
            <?php 
            $countmenus = count($menus);
            $length = (100/($countmenus));
            $length = floor($length);
            ?>
    <table width="100%">
        <tr>
            <td>          
        <ul id="nav" style="padding-left: <?php echo ($countmenus).'%';?>;padding-right: <?php echo ($countmenus).'%';?>;">
        <?php
        for ($i = 0; $i < count($menus); $i++)
        {   $menu = $menus[$i];
            
            ?>
        <li style="padding-left: 1px;width: <?php echo ($length-1).'%';?>;">
            
       <?php if ($pagename.'.php' ==  $menu["url"]){?>
           <a id="menu<?php echo $menu['MenuID'];?>" href="<?php echo $menu["url"];?>" style="color:yellow;text-align: center;" > <?php echo $menu["title"];?></a>
       <?php }else{ ?>
          <a id="menu<?php echo $menu['MenuID'];?>" href="<?php echo $menu["url"];?>" style="color: whitesmoke;text-align: center;"> <?php echo $menu["title"];?></a>
       <?php } ?>
        
          <ul>
          <li>
           <?php
                for ($x = 0; $x < count($arrnavsubmenus); $x++)
                {
                    if ($menu["MenuID"] == $arrnavsubmenus[$x]["MenuID"])
                    {
                        $navsubmenu = $arrnavsubmenus[$x];
                ?>
                    <?php if ($navsubmenu["Link"] ==  $menu["url"] ){?>
                    <?php }else{ ?>
              
              <?php 
                            $arrsubmenu = $navigationsubmenu->getsubmenus($accounttypeid, $arrnavsubmenus[$x]["MenuID"]);
                            $countsubmenus = $arrsubmenu[0][0];
              if($countsubmenus == 1)
              {?>
                                    <?php if($pagename.'.php' == $navsubmenu["Link"])
                                              { ?>
                                        <script type="text/javascript">$("#menu<?php echo $navsubmenu["MenuID"];?>").css({color:"yellow"});</script>
                                        <?php } ?>
                  
            <?php  }else{
              ?>
                    <a href="<?php echo $navsubmenu["Link"]; ?>" style="white-space: nowrap;" id="<?php echo ($pagename.'.php' == $navsubmenu["Link"]) ? 'selectedsubmenu' : ''; ?>"><?php echo $navsubmenu["Name"]; ?></a></b>
                                        <?php if($pagename.'.php' == $navsubmenu["Link"])
                                              { ?>
                                        <script type="text/javascript">$("#menu<?php echo $navsubmenu["MenuID"];?>").css({color:"yellow"});</script>
                                        <?php } ?>
            <?php } ?>
                        <?php } ?>
                <?php } ?>
            <?php } ?>
               </li>
        </ul>   
        </li>
  <?php } ?>
        </ul>           
            </td>
        </tr>
            </table>

</body>
       