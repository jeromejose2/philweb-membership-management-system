<?php
/*
 * Created By   :           JFJ
 * Created On   :           April 15,2013
 * Purpose      :           Header Page
 */
//error_reporting(E_ALL ^ E_NOTICE);
//ini_set('display_errors', 1);

App::LoadModuleClass("MembershipTimor", "MTAccountSessions");
App::LoadModuleClass("MembershipTimor", "MTAccounts");
App::LoadModuleClass("MembershipTimor", "MTAuditTrail");
App::LoadCore("DateSelector.class.php");

$cadminacctsessions = new MTAccountSessions();
$caudittrail        = new MTAuditTrail();

$ds             = new DateSelector();
$ds->SetTimeZone("Asia/Dili");
$serverdatetime = $ds->GetCurrentDateFormat("Y-m-d h:i:s");
$startdate      = $ds->GetCurrentDateFormat("F d, Y H:i:s");
//$date_time      = $cadminacctsessions->selectNow();
//$serverdatetime = $date_time[0][0];
//$startdate = Date('F d, Y H:i:s');
$boolrollback   = false;

/* -- START OF MANAGE SESSION -- */
if (isset($_SESSION['sid']))
{
    $sessiondtls = $cadminacctsessions->GetSessionDetails($_SESSION['sid']);

    if (count($sessiondtls) > 0)
    {
        $currtime  = $sessiondtls[0]["CurrTime"];
        $timestart = $sessiondtls[0]["TransDate"];
        $id        = $sessiondtls[0]["ID"];


        $timediff = (strtotime($currtime) - strtotime($timestart)) / 60;
        if ($timediff >= 120)
        {
            // end active session
            $cadminacctsessions->StartTransaction();
            $cadminacctsessions->logout($id);
            if ($cadminacctsessions->HasError)
            {
                $boolrollback = true;
            }
            else
            {
                $cadminacctsessions->CommitTransaction();
            }

            // log to audit trail
            $caudittrail->StartTransaction();
            $scauditlogparam["SessionID"]            = $_SESSION['sid'];
            $scauditlogparam['AuditTrailFunctionID'] = '41';
            $scauditlogparam["AID"]                  = $_SESSION['aid'];
            $scauditlogparam["TransDetails"]         = "Logout: " . $_SESSION['uname'];
            $scauditlogparam["RemoteIP"]             = $_SERVER['REMOTE_ADDR'];
            $scauditlogparam["TransDateTime"]        = "now_usec()";
            $caudittrail->Insert($scauditlogparam);
            if ($caudittrail->HasError)
            {
                $boolrollback = true;
            }
            else
            {
                $caudittrail->CommitTransaction();
            }
        }

        $cadminacctsessions->StartTransaction();
        $updateSession["ID"]        = $id;
        $updateSession["TransDate"] = 'now_usec()';
        $cadminacctsessions->UpdateByArray($updateSession);
        if ($cadminacctsessions->HasError)
        {
            $boolrollback = true;
        }
        else
        {
            $cadminacctsessions->CommitTransaction();
        }
    }
    else
    {
        $cadminacctsessions->StartTransaction();
        $cadminacctsessions->logout($id);
        if ($cadminacctsessions->HasError)
        {
            $boolrollback = true;
        }
        else
        {
            $cadminacctsessions->CommitTransaction();
        }
        session_destroy();
        session_unset();
        app::pr("<script>window.location='login.php'</script>");
    }

    if ($boolrollback)
    {
        $caudittrail->RollBackTransaction();
        $cadminacctsessions->RollBackTransaction();
    }
}
else
{
    session_destroy();
    session_unset();
    app::pr("<script>window.location='login.php'</script>");
}
/* -- END OF MANAGE SESSION -- */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <?php
        $stylesheets[] = "css/default.css";
        $stylesheets[] = "css/datepicker.css";
        $javascripts[] = "jscripts/jquery-1.5.2.min.js";
        $javascripts[] = "jscripts/checkinputs.js";
        $javascripts[] = "jscripts/datepicker.js";
        $headerinfo    = "";

        for ($i = 0; $i < count($javascripts); $i++)
        {
            $js = $javascripts[$i];
            $headerinfo .= "<script language='javascript' type='text/javascript' src='$js'></script>";
        }
        for ($i = 0; $i < count($stylesheets); $i++)
        {
            $css = $stylesheets[$i];
            $headerinfo .= "<link rel='stylesheet' type='text/css' media='screen' href='$css' />";
        }
        ?>

        <?php echo $headerinfo; ?>
        <script type="text/javascript">
            $(document).ready(function() {
                var $body = $($.browser.msie ? document.body : document);
                if ($.browser.msie)
                {
                    $('#fade').css({
                        height: $body.height()
                    });

                } else
                {
                    $('#fade').css({
                        width: $body.width(),
                        height: $body.height()
                    });
                }


                function preventBackandForward()
                {
                    window.history.forward();
                }

                preventBackandForward();
                window.onload = preventBackandForward();
                window.inhibited_load = preventBackandForward;
                window.onpageshow = function(evt) {
                    if (evt.persisted)
                        preventBackandForward();
                };
                window.inhibited_unload = function() {
                    void(0);
                };
            });
        </script>
        <title>ZENTRUM E-GAMES MEMBERSHIP CATALOG</title>

        <style>
            #logo{
                margin-left: 0.5%;
                padding-top: 0.5%;
                width: auto;
            }
        </style>
<!--  <script type="text/javascript" src="https://getfirebug.com/firebug-lite-debug.js"></script>-->
    </head>
    <body onload="" style="" ondragstart="return false;" ondrop="return false;">
        <div id="containertemplate">
            <div id="fade" class="black_overlay"></div>
            <div style="width:370px;margin-top: 30px;"><img src="images/e_gameslogo.jpg"></div>
            <div id="datetime" style ="height: 100px; width:230px; position: relative; margin-top: -120px; margin-left: 880px;">
                <br /><br /><br />
                <div><br />
                    <b>Current Date:</b>&nbsp;<span id="servdate" name="servdate">&nbsp;</span>
                </div>
                <div>
                    <b>Current Time:</b>&nbsp;<span id="clock">&nbsp;</span>
                </div>
            </div>

            <script type="text/javascript">
            var tempDate = "<?php echo $serverdatetime; ?>";
            var tempTime = "<?php echo $serverdatetime; ?>";
            tempDate = tempDate.substring(5, 7) + "/" + tempDate.substring(8, 10) + "/" + tempDate.substring(0, 4);
            
            document.getElementById("servdate").firstChild.nodeValue = tempDate;



            var timesetter = new Date('<?php echo $startdate ?>');
            var TimeNow = '';
            function MakeTime() {
                timesetter.setTime(timesetter.getTime() + 1000);
                var hhN = timesetter.getHours();
                if (hhN > 12) {
                    var hh = String(hhN - 12);
                    var AP = 'PM';
                } else if (hhN == 12) {
                    var hh = '12';
                    var AP = 'PM';
                } else if (hhN == 0) {
                    var hh = '12';
                    var AP = 'AM';
                } else {
                    var hh = String(hhN);
                    var AP = 'AM';
                }
                var mm = String(timesetter.getMinutes());
                var ss = String(timesetter.getSeconds());
                TimeNow = ((hh < 10) ? ' ' : '') + hh + ((mm < 10) ? ':0' : ':') + mm + ((ss < 10) ? ':0' : ':') + ss + ' ' + AP;
                document.getElementById("clock").firstChild.nodeValue = TimeNow;
                setTimeout(function() {
                    MakeTime();
                }, 1000);
            }
            MakeTime();

            function timedRefresh(timeoutPeriod)
            {
                setTimeout("location.reload(true);", timeoutPeriod);
            }
            </script>

            <table style="margin-top: 0; width: 100%;">
                <tr>
                    <td>
                        <div id="menu">
                            <?php // include_once('menu.php');   ?>
                            <?php include_once('navigation.php'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="content-div" style="
                             width:auto;
                             height:auto;
                             position:relative;
                             margin-top:3px;
                             border-color:#000000;
                             background-color:#f3f3f3;
                             border-width:thin;
                             border-style:solid;
                             overflow-y:auto;
                             vertical-align:top;
                             padding: 1% 1% 1% 1%;
                             ">
