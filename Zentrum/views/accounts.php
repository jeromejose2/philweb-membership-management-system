<?php
/* * ***************** 
 * Author: Jerome Jose 
 * Date Created: 2013-04-15
 * Company: Philweb
 * ***************** */
//include_once('header.php');
require_once("../../init.inc.php");
//error_reporting(E_ALL);
//ini_set('display_errors',1);
App::LoadModuleClass("MembershipTimor","MTAccessRights");
$dbaccessrights = new MTAccessRights();

$arrlink    = $dbaccessrights->getdefaultpagepercols($_SESSION['accttype'],3);
$Newpage    = $arrlink[0][0];
$user_agent = $_SERVER['HTTP_USER_AGENT'];
if ( preg_match('/MSIE/i',$user_agent) )
{
    App::Pr("<script> window.location = '$Newpage'; </script>");
}
else if (preg_match('/chrome/i',$user_agent))
{
     URL::Redirect($Newpage);
}else
{
    URL::Redirect($Newpage);
}
exit();
?>

