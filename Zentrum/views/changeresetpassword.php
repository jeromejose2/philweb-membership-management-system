<?php
/*
 * @author Jerome Jose 2013-04-02
 * Purpose : Change Admin password.
 */
include('../controller/changeresetpasswordprocess.php');
?>
<?php include("headerForNoSession.php") ?>
<script type="text/javascript">
    function checkchangepassword()
    {
        var sPassword = document.getElementById("txtPassword");
        var sNewPassword = document.getElementById("txtNewPassword");
        var sConfirm = document.getElementById("txtConfPassword");

        //    var currentpassword = '<?php echo $_SESSION['actualresetpassword']; ?>';
        addOrRemoveErrorFieldClass("lblPassword", "txtPassword");
        addOrRemoveErrorFieldClass("lblNewPassword", "txtNewPassword");
        addOrRemoveErrorFieldClass("lblConfPassword", "txtConfPassword");

        if (sPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
        {

            document.getElementById('light12').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead12').innerHTML = "INSUFFICIENT INFORMATION";
            document.getElementById('errorMsg12').innerHTML = "Please fill in all the fields on the form to change your password.";
            sPassword.focus();
            return false;
        }
        if (sPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
        {
            var msg = "Password must not be less than 8 characters.";
            var msgtitle = "INVALID INPUT";
            document.getElementById('light12').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead12').innerHTML = msgtitle;
            document.getElementById('errorMsg12').innerHTML = msg;
            sPassword.focus();
            return false;
        }
        if (sNewPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
        {

            var msg = "Please fill in all the fields on the form to change your password.";
            var msgtitle = "INVALID INPUT";
            document.getElementById('light12').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead12').innerHTML = msgtitle;
            document.getElementById('errorMsg12').innerHTML = msg;
            sNewPassword.focus();
            return false;
        }
        if (sNewPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
        {

            var msg = "Password must not be less than 8 characters.";
            var msgtitle = "INVALID INPUT";
            document.getElementById('light12').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead12').innerHTML = msgtitle;
            document.getElementById('errorMsg12').innerHTML = msg;
            sNewPassword.focus();
            return false;
        }
        if (sConfirm.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
        {

            var msg = "Please fill in all the fields on the form to change your password.";
            var msgtitle = "INSUFFICIENT INFORMATION";
            document.getElementById('light12').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead12').innerHTML = msgtitle;
            document.getElementById('errorMsg12').innerHTML = msg;
            sConfirm.focus();
            return false;
        }
        if (sConfirm.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
        {

            var msg = "Password must not be less than 8 characters.";
            var msgtitle = "INVALID INPUT";
            document.getElementById('light12').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead12').innerHTML = msgtitle;
            document.getElementById('errorMsg12').innerHTML = msg;
            sConfirm.focus();
            return false;
        }
        if (sNewPassword.value != sConfirm.value)
        {

            var msg = "Your passwords information do not match. Please try again.";
            var msgtitle = "UNMATCHING PASSWORDS";
            document.getElementById('light12').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead12').innerHTML = msgtitle;
            document.getElementById('errorMsg12').innerHTML = msg;
            sConfirm.focus();
            return false;
        }
        //	if (sPassword.value != currentpassword)
        //    {
        //		var msg = currentpassword+"currentpasswordYour passwords information do not match. Please try again1.";
        //                var msgtitle = "UNMATCHING PASSWORDS";
        //                document.getElementById('light12').style.display='block';
        //		document.getElementById('fade').style.display='block';
        //		document.getElementById('errorHead12').innerHTML = msgtitle;
        //		document.getElementById('errorMsg12').innerHTML = msg;
        //		return false;
        //    }
        if (sNewPassword.value == sPassword.value)
        {

            var msg = "New password cannot be similar to the current password.";
            var msgtitle = "INVALID INPUT";
            document.getElementById('light12').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead12').innerHTML = msgtitle;
            document.getElementById('errorMsg12').innerHTML = msg;
            return false;
        }

        document.getElementById('light11').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "CONFIRMATION";
        document.getElementById('msg2').innerHTML = "You are about to change your current password. Do you wish to continue?";
        return false;
    }
    function cancellink()
    {
        window.location.href = 'filterandviewmembers.php';
    }
    function addOrRemoveErrorFieldClass(lblId, inputId)
    {
        var lblElem = elem(lblId);
        var inputElem = elem(inputId);

        if (inputElem.value.replace(/^\s+|\s+$/, '') == '' || inputElem.value.replace(/^\s+|\s+$/, '').length < 8) {
            addClass(lblElem, "errorField");
        } else {
            removeClass(lblElem, "errorField");
        }

    }
</script>



<form action="changeresetpassword.php?Uname=<?php echo $rcode; ?>" method="post">
    <div id="page">
        <table width="100%">
            <tr>
                <td colspan="2"><div id="header" style="color: white;background-color: black;">&nbsp;&nbsp;&nbsp;Please enter the following information to change your password.</div>
                </td>
            </tr>
            <tr style="background-color:#E6E6E6; height:30px;">
                <td class="fontboldblack" style="width:400px;"><div id="lblPassword">&nbsp;&nbsp;*Current Password:</div></td>
                <td style="width:400px;">
                    &nbsp;&nbsp;<?php echo $txtPassword; ?>
                </td>
            </tr>
            <tr style="background-color:#a6a6a6; height:30px;">
                <td class="fontboldblack"><div id="lblNewPassword">&nbsp;&nbsp;*New Password:</div></td>
                <td>
                    &nbsp;&nbsp;<?php echo $txtNewPassword; ?>
                </td>
            </tr>
            <tr style="background-color:#E6E6E6; height:30px;">
                <td class="fontboldblack"><div id="lblConfPassword">&nbsp;&nbsp;*Confirm New Password:</div></td>
                <td>
                    &nbsp;&nbsp;<?php echo $txtConfPassword; ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <?php echo $btnOk; ?>
                    <?php echo $btncancel; ?>
                </td>
            </tr>
        </table>

        <div id="light1" style="text-align: center;font-size: 16pt;height: 220px;background-color: #e6e6e6;" class="white_content">
            <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black;padding-top: 5px;">
                <b id="errorHead"></b>
            </div>
            <br/>
            <p id="errorMsg"><br/></p>
            <br/>
            <input type="button" class="labelbutton_black" value="OKAY" onclick ="window.location = 'login.php';"/>
        </div>

        <!-- ERROR MESSAGE(Invalid change password) -->
        <div id="light12" style="text-align: center;font-size: 16pt;height: 220px;background-color: #e6e6e6;" class="white_content">
            <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black;padding-top: 5px;">
                <b id="errorHead12"></b>
            </div>
            <br/>
            <p id="errorMsg12"><br/></p>
            <br/>
            <input id="btnClose" type="button" value="OKAY" class="labelbutton_black" />
        </div>
        <!-- END OF ERROR MESSAGE(Invalid change password) -->	
        <!-- CONFIRMATION MESSAGE -->
        <div id="light11" style="text-align: center;font-size: 16pt;height: 250px; margin-left:-95px;background-color: #e6e6e6;" class="white_content">
            <b> <div id="title2" style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black;padding-top: 5px;">
                </div></b><br /><br /><div id="msg2"></div>
            <br/><br/><br />
            <?php echo $passSubmit; ?>
            <input id="btnClose" type="button" value="CANCEL" class="labelbutton_black" />
        </div>
        <!-- END OF CONFIRMATION MESSAGE -->	
        <!-- ERROR MESSAGES -->
        <div id="light10" style="text-align: center;font-size: 16pt;height: 250px; margin-left:-95px;background-color: #e6e6e6;" class="white_content">
            <div id="title3" style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: white;padding-top: 5px;">
            </div><br /><br /><div id="msg3"></div><br/><br/><br />
            <input id="passSubmit1" type="button" name="passSubmit1" value="OKAY" class="labelbutton_black" />
        </div>
        <!-- END OF ERROR MESSAGES -->
    </div>
    <div id="fade" class="black_overlay"></div>
</form>

<script type="text/javascript">
    $('#passSubmit').live('click', function() {
        $('#light10').fadeOut('slow');
        document.getElementById('fade').style.display = 'none';
    });

    $('#passSubmit1').live('click', function() {
        $('#light10').fadeOut('slow');
        document.getElementById('fade').style.display = 'none';

    });

    $('#btnClose').live('click', function() {
        $('#light11').fadeOut('slow');
        $('#light12').fadeOut('slow');
        document.getElementById('fade').style.display = 'none';

    });


<?php if (isset($successmsg)): ?>

        var msg = "You have successfully changed your password.";
        var msgtitle = "SUCCESSFUL PASSWORD CHANGE";
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('errorHead').innerHTML = msgtitle;
        document.getElementById('errorMsg').innerHTML = msg;
        $("input#txtPassword,input#txtNewPassword,input#txtConfPassword").val("");


<?php endif; ?>
<?php if (isset($errormsg)): ?>
        var msg = '<?php echo $errormsg; ?>';
        var msgtitle = "ERROR";
        document.getElementById('light12').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        document.getElementById('errorHead12').innerHTML = msgtitle;
        document.getElementById('errorMsg12').innerHTML = msg;
        //                $("input#txtPassword,input#txtNewPassword,input#txtConfPassword").val("");
<?php endif; ?>

</script>
<script>
    $(function() {
        $('input[type=text],[type=password]').bind('cut copy paste', function(e) {
            e.preventDefault();
        });
    });
</script>
<?php include("footer.php") ?>
