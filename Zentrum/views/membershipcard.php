<?php
/**
 * This file will generate the Membership Card No. and to be included in several pages.
 * 
 * @author Noel Antonio 04-18-2013 <ndantonio@philweb.com.ph>
 * @copyright c 2013, PhilWeb Corporation
 */

require_once '../../init.inc.php';

App::LoadModuleClass("MembershipTimor", "MTCards");
App::LoadModuleClass("MembershipTimor", "MTMemberInfo");

$mtCards = new MTCards();
$mtMemberInfo = new MTMemberInfo();

// Getting available card
$cards = $mtCards->SelectMemberCardNo();
$cardID = $cards[0]["CardID"];

// Getting last member ID (increment by 1)
$member = $mtMemberInfo->SelectLastMemberID();
$memberID = $member[0]["MemberID"] + 1;

$memberData = $mtMemberInfo->getMemberAcctDtlsWithID($_SESSION['hiddenMemberId']);
$memberDataOrigin = $memberData[0]["RegistrationOrigin"];
$memberDataPhoto = $memberData[0]["PhotoFileName"];
$memberDataId = $memberData[0]["MemberInfoID"];
?>