<?php
/*
 * Created By   :           JFJ
 * Created On   :           April 15,2013
 * Purpose      :           Header Page JUST for simple header only , session not included
 */
require_once("../../init.inc.php");
App::LoadModuleClass("MembershipTimor", "MTAccountSessions");
App::LoadModuleClass("MembershipTimor", "MTAccounts");
App::LoadModuleClass("MembershipTimor", "MTAuditTrail");

$cadminacctsessions = new MTAccountSessions();
$caudittrail = new MTAuditTrail();

$date_time = $cadminacctsessions->selectNow();
$serverdatetime = $date_time[0][0];
$boolrollback = false;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<?php

$stylesheets[] = "css/default.css";
$stylesheets[] = "css/datepicker.css";
$javascripts[] = "jscripts/jquery-1.5.2.min.js";
$javascripts[] = "jscripts/checkinputs.js";
$javascripts[] = "jscripts/datepicker.js";
$headerinfo = "";

for ($i = 0; $i < count($javascripts); $i++)
{
    $js = $javascripts[$i];
    $headerinfo .= "<script language='javascript' type='text/javascript' src='$js'></script>";
}
for ($i = 0; $i < count($stylesheets); $i++)
{
    $css = $stylesheets[$i];
    $headerinfo .= "<link rel='stylesheet' type='text/css' media='screen' href='$css' />";
}
?>

        <script type="text/javascript">
            function preventBackandForward()
            {
                window.history.forward();
            }

            preventBackandForward();
            window.onload=preventBackandForward();
            window.inhibited_load=preventBackandForward;
            window.onpageshow=function(evt){if(evt.persisted)preventBackandForward();};
            window.inhibited_unload=function(){void(0);};
        </script>
        <title>ZENTRUM E-GAMES MEMBERSHIP CATALOG</title>
        <?php echo $headerinfo; ?>
         <style>
            #logo{
                margin-left: 0.5%;
                padding-top: 0.5%;
                width: auto;
            }
        </style>
    </head>
    <body onload="//timedRefresh(20000);">
        <form id="frmTemplate" name="frmTemplate" method="post" enctype="multipart/form-data">
        <div id="containertemplate">
	<div id="fade" class="black_overlay"></div>	
          <div id="logo">
                <img src="images/e_gameslogo.jpg" alt="" height ="90px" width="220px"/>
            </div>
            <div id="datetime" style ="height: 100px; width:230px; position: relative; margin-top: -120px; margin-left: 880px;">
                <div><br /><br />
                    <b>Current Date:</b>&nbsp;<span id="servdate" name="servdate">&nbsp;</span>
                </div>
                <div>
                    <b>Current Time:</b>&nbsp;<span id="clock">&nbsp;</span>
                </div>
            </div>
          
            <script type="text/javascript">
                var tempDate = "<?php echo $serverdatetime; ?>";
                var tempTime = "<?php echo $serverdatetime; ?>";
                tempDate = tempDate.substring(5,7) + "/" + tempDate.substring(8,10) + "/" + tempDate.substring(0,4);
                document.getElementById("servdate").firstChild.nodeValue = tempDate;


                <?php $startdate = date("F d, Y H:i:s"); ?>
                var timesetter = new Date('<?php echo $startdate ?>');
                var TimeNow = '';
                function MakeTime() {
                timesetter.setTime(timesetter.getTime()+1000);
                var hhN  = timesetter.getHours();
                if(hhN > 12){
                var hh = String(hhN - 12);
                var AP = 'PM';
                } else if(hhN == 12) {
                var hh = '12';
                var AP = 'PM';
                }else if(hhN == 0){
                var hh = '12';
                var AP = 'AM';
                }else{
                var hh = String(hhN);
                var AP = 'AM';
                }
                var mm  = String(timesetter.getMinutes());
                var ss  = String(timesetter.getSeconds());
                TimeNow = ((hh < 10) ? ' ' : '') + hh + ((mm < 10) ? ':0' : ':') + mm + ((ss < 10) ? ':0' : ':') + ss + ' ' + AP;
                document.getElementById("clock").firstChild.nodeValue = TimeNow;
                setTimeout(function(){
                MakeTime();},1000);
                }
                MakeTime();

                function timedRefresh(timeoutPeriod)
                {
                setTimeout("location.reload(true);",timeoutPeriod);
                }
            </script>
            
            <table style="margin-top: 0; width: 100%;">
            <tr>
              <td>
                <div id="menu">
                  <?php // include_once('menu.php'); ?>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div id="" style="
                    width:auto;
                    height:600px;
                    position:relative;
                    margin-top:3px;
                    border-color:#000000;
                    background-color:#f3f3f3;
                    border-width:thin;
                    border-style:solid;
                    overflow-y:auto;
                    vertical-align:top;
                    padding: 2% 2% 2% 2%;
                    ">
