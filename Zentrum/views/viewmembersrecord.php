<?php

/*
 * Created by : Frances Ralph DL. Sison
 * Date Created : April 01, 2013
 * Purpose :  Edit Member's Profile
 */
header("Pragma: no-cache");
header("cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

include("../controller/viewmembersrecordprocess.php");
include("header.php");
?>
<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="mywebcam/assets/css/styles.css" />
<script src="jscripts/jquery-1.5.2.min.js"></script>
<script src="mywebcam/assets/fancybox/jquery.easing-1.3.pack.js"></script>
<script src="mywebcam/assets/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script src="mywebcam/assets/webcam/webcam.js"></script>
<script src="mywebcam/assets/js/script.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<style type="text/css">
    
    .overlay{
        opacity: 0;
        filter: alpha(opacity = 0);
        position: absolute;
        top: 0; bottom: 0; left: 0; right: 0;
        display: block;
        z-index: 2;
        background: transparent;
    }
    
</style>
<script type="text/javascript">
    $(document).ready(function(){
        var $body = $(this.ie6 ? document.body : document);
        $('#fade').css({
            width:  $body.width(),
            height:  $body.height()
        });
        
        var isOpera = !!window.opera || navigator.userAgent.indexOf('Opera') >= 0;
        // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
        var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        // At least Safari 3+: "[object HTMLElementConstructor]"
        var isChrome = !!window.chrome;                          // Chrome 1+
        var isIE = /*@cc_on!@*/false;                            // At least IE6
                
        if(isIE)
        {
            document.frames['frameid2'].document;
        }
        else if(isChrome || isFirefox)
        {
            document.getElementById("frameid2").contentDocument.location.reload(true);
        }
        $('#frameid2').attr('src', 'photo2.php');
        
        $('#others').hide();
        $('#tdbrowse').hide();
        $('#ddlidpresented').change(function(){
           var val = $('#ddlidpresented').val();
           if (val == 4){
               $('#others').show();
           } else {
               $('#others').hide();
           }
        });
        
        $('#lnkbrowse').click(function(){
            $('input[type=file]').click();            
            return false;
        });
        
        $('#lnktake').click(function(){
            document.getElementById('lightphoto').style.display='block';
            document.getElementById('fade').style.display='block';           
            return false;
        });
    });
</script>
<?php if($display == true)
      {
?>
<form id="frmUserProfile" name="frmUserProfile" method="POST" action="editmembersrecords.php" enctype="multipart/form-data">
<div id="page">     
  <table width="100%" align="center">
    <tr>
        <td>
            <table cellspacing="3">
                <tr>
                    <td colspan="2" align="center"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="fontboldblack">
                        <div id="tableheader">User Account Profile</div>
                    </td>
                </tr>
                <tr style="background-color:gray; height:30px;">
                    <td class="fontboldblack" style="width:50%;">&nbsp;&nbsp;Membership Card Number</td>
                    <?php if($status == 1)
                          {
                    ?>
                    <td style="width:50%;">&nbsp;&nbsp;A<?php printf("%1$05d", $membershipCardNumber); ?></td>
                    <?php }
                          else if($status == 0 || $status == 2 || $status == 3 || $status == 4)
                          {
                    ?>
                    <td style="width:50%;"></td>
                    <?php } ?>
                </tr>
                <tr style="background-color:lightgray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;First Name</td>
                    <td>&nbsp;&nbsp;<?php echo $firstName ?></td>
                </tr>
                <tr style="background-color:gray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Last Name</td>
                    <td>&nbsp;&nbsp;<?php echo $lastName ?></td>
                </tr>
                <tr style="background-color:lightgray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Birthdate</td>
                    <td>&nbsp;&nbsp;<?php echo $birthDate ?></td>
                </tr>
                <tr style="background-color:gray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Gender</td>
                    <td>&nbsp;&nbsp;<?php if($gender == 1)
                                          {
                                            echo 'Male';
                                          }
                                          else if($gender == 2)
                                          {
                                            echo 'Female';
                                          }
                                     ?>
                   </td>
                </tr>
                <tr style="background-color:lightgray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;ID Presented</td>
                    
                    <td>&nbsp;&nbsp;
                        <?php if($idList[0]["IdentificationID"] == 4)
                                          {
                                            echo $option1;
                                          }
                                          else
                                          {
                                            echo $idList[0]["IdentificationName"];
                                          }
                        ?>
                    </td>
                </tr>
                <tr style="background-color:gray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;ID Number</td>
                    <td>&nbsp;&nbsp;<?php echo $idNumber ?></td>
                </tr>
                <tr style="background-color:lightgray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Nationality</td>
                    <td>&nbsp;&nbsp;<?php echo $countryList[0]["CountryName"] ?></td>
                </tr>
                <tr style="background-color:gray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Marital Status</td>
                    <td>&nbsp;&nbsp;<?php if($maritalStatus == 1)
                                          {
                                            echo 'Single';
                                          }
                                          else if($maritalStatus == 2)
                                          {
                                            echo 'Married';
                                          }
                                          else if($maritalStatus == 3)
                                          {
                                            echo 'Widowed';
                                          }
                                          else
                                          {
                                            echo '';
                                          }
                                    ?>
                    </td>
                </tr>
                <tr style="background-color:lightgray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Address</td>
                    <td>&nbsp;&nbsp;<?php echo $address ?></td>
                </tr>
                <tr style="background-color:gray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Contact Number</td>
                    <td>&nbsp;&nbsp;<?php echo $contactNumber ?></td>
                </tr>
                <tr style="background-color:lightgray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;E-mail Address</td>
                    <td>&nbsp;&nbsp;<?php echo $email ?></td>
                </tr>
                <tr style="background-color:gray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Membership Date</td>
                    <td>&nbsp;&nbsp;<?php echo $membershipDate ?></td>
                </tr>
                <tr style="background-color:lightgray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Application Tool</td>
                    <td>&nbsp;&nbsp;<?php if($appTool == 2)
                                          {
                                            echo 'Online';
                                          }
                                          else if($appTool == 1)
                                          {
                                            echo 'Onsite';
                                          }
                                    ?>
                    </td>
                </tr>
                <tr style="background-color:gray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Status</td>
                    <td>&nbsp;&nbsp;<?php if($status == 0)
                                          {
                                            echo 'Inactive';
                                          }
                                          else if($status == 1)
                                          {
                                            echo 'Active';                         
                                          }
                                          else if($status == 2)
                                          {
                                            echo 'Suspended';
                                          }
                                          else if($status == 3)
                                          {
                                            echo 'Banned';
                                          }
                                    ?>
                    </td>
                </tr>
                <tr style="background-color:lightgray; height:30px;">
                    <td class="fontboldblack">&nbsp;&nbsp;Remarks</td>
                    <td>&nbsp;&nbsp;<?php echo $remarks ?></td>
                </tr>
            </table>
        </td>
        <td>
            <table width="100%;" cellspacing="10" cellpadding="10">
                <tr>
                    <td>
                        <div id="photos">
                            <iframe id="frameid2" frameborder="no" scrolling="no" name="frameid2" height="190" width="240" src=""></iframe>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="lblpic" style="margin-left: 8%;"></div>
                    </td>
                </tr>           
            </table>
        </td>
    </tr>
  </table>
</div>
<center>
    <?php echo $hiddenId; ?>
    <?php if($userType == 1 || $userType == 2)
          {
    ?>
       <a href="#" class="labelbutton_black" onclick="javascript: return editMemberProfile(<?php echo $hidMember; ?>);">EDIT</a>
    <?php } ?>
</center>
<br />
</form>
<?php } ?>
<form name="frmprofile" method="POST" enctype="multipart/form-data">  
    <div id="light8" class="white_content">
        <div style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;">
		 Error Message
	</div>
        <br />
	<br />
                 Please select username.
        <br/>
        <br/>
        <br />
        <input id="btnOkClose" type="button" value="OK" class="labelbutton2" onclick="document.getElementById('light8').style.display='none';document.getElementById('fade').style.display='none';" />
     </div>
    <!-- CONFIRMATION MESSAGE -->
    <div id="light20" class="white_content">
		<div id="title" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;"></div>
		<br />
		<br />
		<div id="msg"></div>
		<br />
		<br />
		<br />
		<input id="btnOkClose1" name ="btnOkClose1" type="submit" value="OK" class="labelbutton2" onclick="document.getElementById('light20').style.display='none';document.getElementById('fade').style.display='none';" />
    </div>
    <!-- END OF CONFIRMATION MESSAGE -->
    <!-- CONFIRMATION MESSAGE -->
    <div id="light21" class="white_content">
        <div id="title2" style="width: 100%;height: 27px;background-color: #FF9C42;top: 0px;color: white;padding-top: 5px;"> </div>
        <br />
	<br />
	<div id="msg2"></div>
	<br/>
	<br/>
	<br />
	<input id="btnOkClose" type="button" value="OK" class="labelbutton2" />
    </div>
	<!-- END OF CONFIRMATION MESSAGE -->		
    <br />
</form>
<?php include("footer.php");?>
<script>
$(function(){
    $('input[type=text],[type=password]').bind('cut copy paste', function (e){
        e.preventDefault();
    });
});
</script>