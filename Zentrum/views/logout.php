<?php
/*
 * Created By       :       JFJ
 * Date Created     :       April 2,2013
 * Purpose          :       For log-out user and end active session.
 */

include("../../init.inc.php");
App::LoadModuleClass("MembershipTimor","MTAccountSessions");
App::LoadModuleClass("MembershipTimor","MTAuditTrail");
$cadminAccountSessions = new MTAccountSessions();
$cauditTrail           = new MTAuditTrail();

$sessiondtls = $cadminAccountSessions->GetSessionDetails($_SESSION['sid']);
$id          = $sessiondtls[0]["ID"];
// end current active session
$cadminAccountSessions->StartTransaction();
$cadminAccountSessions->logout($id);
if ( $cadminAccountSessions->HasError )
{
    $cadminAccountSessions->RollBackTransaction();
}
else
{
    $cadminAccountSessions->CommitTransaction();
    // log to audit trail
    $cauditTrail->StartTransaction();
    $auditLogParameter["SessionID"]            = $_SESSION['sid'];
    $auditLogParameter['AuditTrailFunctionID'] = '2';
    $auditLogParameter["AID"]                  = $_SESSION['aid'];
    $auditLogParameter["TransDetails"]         = "Logout: " . $_SESSION['uname'];
    $auditLogParameter["RemoteIP"]             = $_SERVER['REMOTE_ADDR'];
    $auditLogParameter["TransDateTime"]        = "now_usec()";
    $cauditTrail->Insert($auditLogParameter);
    if ( $cauditTrail->HasError )
    {
        $cauditTrail->RollBackTransaction();
    }
    else
    {
        $cauditTrail->CommitTransaction();
        session_destroy();
        session_unset();
        URL::Redirect('login.php');
    }
}
?>
