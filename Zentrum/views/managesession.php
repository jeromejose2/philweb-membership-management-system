<?php
/*
 * @author JFJ April 15,2013
 * Purpose   : controller for managesession
 */
require_once("../../init.inc.php");
App::LoadModuleClass("MembershipTimor", "MTAccountSessions");
App::LoadModuleClass("MembershipTimor", "MTAuditTrail");
App::LoadModuleClass("MembershipTimor", "MTAccessRights");


if (isset($_SESSION['sid']) && $_SESSION['sid'] != '')
{
    $tmsid = $_SESSION['sid'];
    $accttypeid = $_SESSION['accttype'];
    $tmsession = new MTAccountSessions();
    $tmauditlog = new MTAuditTrail();
    $tmaccessrights = new MTAccessRights();
  
    //check if has page access..
    $arrPageAccess = $tmaccessrights->PageAccessRights($accttypeid, $pagesubmenuid);

    if ($arrPageAccess == 0)
    {
        $chkPage = 0;
    }
    else
    {
        $_SESSION['currPage'] = URL::CurrentPage();
        $chkPage = 1;
      
        //check if session exists
        $arrSession = $tmsession->GetSessionID($tmsid);
         
        if (count($arrSession) == 1)
        {
            $sessiondtls = $arrSession[0];
            $sessionid = $sessiondtls["SessionID"];
            $currtime = $sessiondtls["DateStart"];
            $timestart = $sessiondtls["TransDate"];
            $id = $sessiondtls["ID"];
            $aid = $sessiondtls["AID"];
            $_SESSION['acctid'] = $aid;
            
            //check if expired
            $timediff = (strtotime($timestart) - strtotime($currtime)) / 60;
          
            var_dump($timediff);
            if($timediff >= 120) // SESSION TIMEOUT
            {
                $tmsession->StartTransaction();
                $tmsession->UpdateDateEnd($aid);
                if ($tmsession->HasError)
                {
                    $errormsgacc = $tmsession->getError();
                    $errormsgacctitleacc = "ERROR!";
                }
                else {$tmsession->CommitTransaction();}

                $tmauditlog->StartTransaction();
                //insert in auditlog table      
                $tmaudit["SessionID"] = $_SESSION['sid'];
                $tmaudit["AID"] = $_SESSION['acctid'];
                $tmaudit["TransDetails"] = 'Account ID: '.$_SESSION['acctid'];
                $tmaudit["TransDateTime"] = 'now_usec()';
                $tmaudit["RemoteIP"] = $_SERVER['REMOTE_ADDR'];
                $tmaudit["AuditTrailFunctionID"] = '2';  
                $tmauditlog->Insert($tmaudit);
                if ($tmauditlog->HasError)
                {
                    $errormsgacc = $tmauditlog->getError();
                    $errormsgacctitleacc = "ERROR!";
                }  else {
                $tmauditlog->CommitTransaction(); 
                session_destroy();
                URL::Redirect('login.php');
                }
                
            }
            else
            {               
                //update session id  
                $tmsession->StartTransaction();
                $tmupdatesid = $tmsession->UpdateTransDate($id);
                var_dump($tmupdatesid);exit();
                if ($tmsession->HasError)
                {
                    $errormsgacc = $tmsession->getError();
                    $errormsgacctitleacc = "ERROR!";
                }else
                {
                    $tmsession->CommitTransaction();
                }
            }
        }
        else
        {
            URL::Redirect('login.php');
        }
    }
}
else
{
    // If SessionID Does not Exist ..
    URL::Redirect('login.php');
}
?>
