/*
 * File:                webcam script.js
 * Author:              Noel Antonio
 * Date Created:        April 15, 2013
 * Purpose:             This script will call the swf file for taking pictures.
 *                      It also handles events for capturing and uploading the image.
 *                      Image will be uploaded using an API URL file, upload.php.
 *                      Settings for camera are also detected by the swf file.
 */

$(document).ready(function(){
    
	var camera = $('#camera'),
        screen =  $('#screen');
        
        var shootEnabled = false;
        

	/*----------------------------------
		Setting up the web camera
	----------------------------------*/
    
	webcam.set_swf_url('mywebcam/assets/webcam/webcam.swf');
        if ($('#hidOrigin').val() == 2)
        {
            webcam.set_api_url('upload2.php');
        }
	else
        {
            webcam.set_api_url('upload.php');
        }
        
	webcam.set_quality(80);
	webcam.set_shutter_sound(true, 'mywebcam/assets/webcam/shutter.mp3');

	// Generating the embed code and adding it to the page:	
        screen.html(
                webcam.get_html(screen.width(), screen.height())
        );
            

	/*----------------------------------
		Binding event listeners
	----------------------------------*/
		
	$('#shootButton').click(function(){
		
		if(!shootEnabled){
                    return false;
		}
		
		webcam.freeze();
		togglePane();
		return false;
	});
	
	$('#cancelButton').click(function(){
		webcam.reset();
		togglePane();
		return false;
	});
	
	$('#uploadButton').live('click', function(){
		webcam.upload();
		webcam.reset();
		togglePane();
                
                $("#shootButton").hide();
                $("#closeButton").hide();
                
		return false;
	});
        

	camera.find('.settings').click(function(){
		if(!shootEnabled){
			return false;
		}
		
		webcam.configure('camera');
	});

	
        /*---------------------- 
		Camera Panel
	----------------------*/
        
        
        camera.animate({
                bottom:-5
        },{easing:'easeOutExpo',duration:'slow'});



	/*---------------------- 
		Callbacks
	----------------------*/
	
	
	webcam.set_hook('onLoad',function(){
		// When the flash loads, enable
		// the Shoot and settings buttons:
		shootEnabled = true;
	});
	
	webcam.set_hook('onComplete', function(msg){
		
		// This response is returned by upload.php
		// and it holds the name of the image in a
		// JSON object format:
		
		msg = $.parseJSON(msg);
		
		if(msg.error){
			alert(msg.message);
		}
		else {
                    var newimg = $('#hidID').val();
                    var origin2 = $('#hidOrigin').val();
                    $.ajax({
                        url: 'rename.php?orig=' + msg.filename + '&new=' + newimg + '&path=' + origin2,
                        type: 'get',
                        success: function(){
                            // Adding it to the page;
                            reloadImage();
                            $('#lightphoto').css("display", "none");
                            $('#fade').css("display", "none");
                            
                            $("#shootButton").show();
                            $("#closeButton").show();
                        },
                        error: function(e){
                            alert(e);
                        }
                    });
		}
	});
	
	webcam.set_hook('onError',function(e){
		screen.html(e);
	});
	
	
	/*-------------------------------------
		Populating the page with images
	-------------------------------------*/        
        
	function reloadImage(){
                var page = $('#hidPage').val();
                
                if (page == "editmembersrecords")
                {
                    $('#frameId').attr('src', 'photo.php');
                }
                else
                {
                    $('#frameId').attr('src', 'image.php');
                }
                $('#hidImgPath').val($('#hidID').val() + '.jpg');
		return false;
        }


	// This function toggles the two
	// .buttonPane divs into visibility:
	
	function togglePane(){
		var visible = $('#camera .buttonPane:visible:first');
		var hidden = $('#camera .buttonPane:hidden:first');
		
		visible.fadeOut('fast',function(){
			hidden.show();
		});
	}
	
	
	// Helper function for replacing "{KEYWORD}" with
	// the respectful values of an object:
	
	function templateReplace(template,data){
		return template.replace(/{([^}]+)}/g,function(match,group){
			return data[group.toLowerCase()];
		});
	}
});
