<?php

/*
 * Author : Frances Ralph DL. Sison
 * Date Created : April 01, 2013
 * Purpose : Filter and View Membership Accounts
 */

$pagesubmenuid = 15;

require_once("../controller/filterandviewmembersprocess.php");
require_once("header.php");
?>

<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="text/javascript" src="jscripts/checkinputs.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        
        $('#filterSearch').change(function(){
            $('#searchField').focus();
            $('#searchField').select();
            $('#searchField').val('');
        });
        
        $("#searchField").live('keypress', function(event){
                
                var filterSearch = document.getElementById('filterSearch').options[document.getElementById('filterSearch').selectedIndex].value;
                var charCode = (event.which) ? event.which : event.keyCode;
                
                if (filterSearch == 1)
                {
                    
                    if (/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
                        return false;
                    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
                    // returns false if a numeric character has been entered
                    return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */);
                    return (chCode == 32);
                    
                }
                else if (filterSearch == 2)
                {
                    
                    if (/*DisableWhiteSpaces*/(charCode == 32) || /*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
                        return false;
                    
                    return true;
                    
                }
                else
                {
                    return false;
                }

        });

    });
   
</script>
<script language="javascript" type="text/javascript">
    
    function ChangePage(pagenum)
    {
        selectedindex = document.getElementById("pgSelectedPage");
        selectedindex.value = pagenum;
        document.forms['form2'].submit();
    }
    
//    function acceptSpace()
//    {        
//        document.getElementById("filterSearch").onchange = show_value;
//        if(namefiltersearch == 1)
//        {
//            document.getElementById("searchField").onkeypress = AlphaNumericAndSpacesOnly(event);
//        }
//        else
//        {
//            document.getElementById("searchField").onkeypress = AlphaNumericOnly(event);
//        }
//    }
//    
//    function show_value()
//    {
//        
//    }
//    
//    function AlphaNumericAndSpacesOnly(event)
//    {
//        var charCode = (event.which) ? event.which : event.keyCode;
//        if (/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
//        return false;
//
//        //return true;
//    }
//    
//    function AlphaNumericOnly(event)
//    {
//        var charCode = (event.which) ? event.which : event.keyCode;
//        if (/*DisableWhiteSpaces*/(charCode == 32) || /*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))
//        return false;
//
//       // return true;
//    }
     
</script>
<form name="form2"  method="post" enctype="multipart/form-data">
    <div id="page">
        <div id="fontboldblack">
                <table width="100%">
                  <tr>
                   <td align="left">
                    Status:&nbsp;&nbsp;<?php echo $ddlMemberStatus;?>
                   </td>           
                   <td align="right">
                    Search By:&nbsp;&nbsp;<?php echo $filterSearch;?>&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php echo $searchField;?>&nbsp;&nbsp;&nbsp;
                    <?php echo $btnSearch;?>
                   </td>
                  </tr>
                </table>
        </div>
    </div>
    <br />
    <div class="form-page" align="right"><?php echo $pgTransactionHistory;?></div>
</form>
<form method="POST" action="viewmembersrecord.php" name="FrmMemberInfo" id="FrmMemberInfo" enctype="multipart/form-data">
    <div class="content-page" style="padding: 1% 1% 1% 1%;width: auto;">
        <?php echo $hidMemberInfoID;?>
        <table align="center" style="width: 100%">
            <br />
            <tr align="center" style="background-color:black; color:white; height:30px;">
                <th class="fontboldblack">&nbsp;&nbsp;Membership Card No.</th>
                <th class="fontboldblack">&nbsp;&nbsp;Last Name</th>
                <th class="fontboldblack">&nbsp;&nbsp;First Name</th>
                <th class="fontboldblack">&nbsp;&nbsp;Birthdate</th>
                <th class="fontboldblack">&nbsp;&nbsp;Gender</th>
                <th class="fontboldblack">&nbsp;&nbsp;Membership Date</th>
                <th class="fontboldblack">&nbsp;&nbsp;Status</th>
                <th class="fontboldblack">&nbsp;&nbsp;Application Tool</th>
            </tr>       
            <?php if(count($filterAndViewList) > 0 && ($display)): ?>
            <?php for($ctr=0;$ctr < count($filterAndViewList);$ctr++): ?>
            <?php ($ctr % 2) == 0 ? $class = "evenrow" : $class = "oddrow"; ?>
            <tr class = "<?php echo $class?>" style="text-align:center;">
                <?php $status = $filterAndViewList[$ctr]['Status'];?>
                <?php if($status == 1)
                      {
                ?>
                <td><a href="#" onclick="javascript: return editMemberInfo(<?php echo $filterAndViewList[$ctr]['MemberInfoID'];?>);" >A<?php printf("%1$05d", $filterAndViewList[$ctr]['CardNumber']);?></a></td>
                <?php }
                      else if($status == 0 || $status == 2 || $status == 3 || $status == 4)
                      {
                ?>
                <td><a href="#" onclick="javascript: return editMemberInfo(<?php echo $filterAndViewList[$ctr]['MemberInfoID'];?>);" >Update</a></td>
                <?php } ?>
                <td><?php echo $filterAndViewList[$ctr]['LastName'];?></td>
                <td><?php echo $filterAndViewList[$ctr]['FirstName'];?></td>
                <?php $birthDate = new DateTime($filterAndViewList[$ctr]['Birthdate']);?>
                <td><?php echo $birthDate->format('m/d/Y');?></td>   
                <td><?php $gender = $filterAndViewList[$ctr]['Gender']; if($gender == 1) { echo "Male"; } else if($gender == 2) { echo "Female";}?></td>
                <?php $membershipDate = new DateTime($filterAndViewList[$ctr]['DateCreated']);?>
                <td><?php echo $membershipDate->format('m/d/Y');?></td>
                <td> <?php if($status == 0)
                           {
                            echo "Inactive";
                           }
                           else if($status == 1)
                           {
                            echo "Active";
                           }
                           else if($status == 2)
                           {
                            echo "Suspended";
                           }
                           else if($status == 3)
                           {
                            echo "Banned";
                           }
                     ?>
                </td>
                <td><?php $appTool = $filterAndViewList[$ctr]['RegistrationOrigin'];
                    if($appTool == 2)
                    {
                        echo "Online";
                    }
                    else if($appTool == 1)
                    {
                        echo "Onsite"; 
                    }
                    ?>
                </td>
            </tr>
            <?php endfor; ?>
            <?php else: ?>
            <tr class="no-record">
                <td colspan="8">No records found</td>
            </tr>
            <?php endif;?>
        </table>
    </div>
    <br />
    <br /><br />
</form>
<form name="form3" id="form3" method="POST" enctype="multipart/form-data">
    <center>
        <?php if(($userType == 1 || $userType == 4) && $display == true)
              {
        ?>
                <div class="form-page"><?php echo $btnDownload;?></div>            
        <?php }           
        ?>
    </center>
    <div id="light1" class="white_content">
        <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black; padding-top: 0px;">
            <b id="errorHead"></b>
        </div>
        <br/>
        <p id="errorMsg"><br/></p>
        <br/>
        <p id="button">
        <input type="button" class="inputBoxEffectPopup" onclick ="document.getElementById('light1').style.display='none';document.getElementById('fade').style.display='none';"/>
        </p>
    </div>
    <div id="fade" class="black_overlay"></div>
    <?php if(isset($errorTitle))
          {
    ?>
            <script>
                document.getElementById('errorHead').innerHTML = '<?php echo $errorTitle;?>';
                document.getElementById('errorMsg').innerHTML = '<?php echo $errorMessage;?>';
                //document.getElementById('button').innerHTML = '<button type="button" class="labelbutton_black" onclick="window.location = \'filterandviewmembers.php\';">OKAY</button> ';
                //document.getElementById('button').innerHTML = '<input type="button" class="labelbutton_black" ';
                document.getElementById('light1').style.display='block';
                document.getElementById('fade').style.display='block';
            </script>
    <?php } ?>
</form>
<?php include("footer.php");?>



