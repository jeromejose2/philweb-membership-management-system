<?php
/**
 * This file receives the JPEG snapshot from webcam.swf as a POST request.
 * 
 * @author Noel Antonio
 * @modified by Ralph Sison
 * @copyright 2013 PhilWeb Corporation
 */
require_once '../../init.inc.php';

$basepath = App::getParam('appdir');

if(strtolower($_SERVER['REQUEST_METHOD']) != 'post'){
	exit;
}

if(!isset($_SESSION))
{
    session_start();
}

$_SESSION['takephoto'] = '5';

$folder = $basepath.'TegsRegistration/views/mywebcam/uploads/';
$filename = date('YmdHis') . '.jpg';
$original = $folder.$filename;

$input = file_get_contents('php://input');

// Blank image. We don't need this one.
if(md5($input) == '7d4df9cc423720b7f1f3d672b89362be'){	
	exit;
}

$result = file_put_contents($original, $input);
if (!$result) {
	echo '{
		"error"		: 1,
		"message"	: "Failed save the image. Make sure you chmod the uploads folder and its subfolders to 777."
	}';
	exit;
}

$info = getimagesize($original);
if($info['mime'] != 'image/jpeg'){
	unlink($original);
	exit;
}

rename($original, $basepath.'TegsRegistration/views/mywebcam/uploads/original/'.$filename);
$original = $basepath.'TegsRegistration/views/mywebcam/uploads/original/'.$filename;


$origImage	= imagecreatefromjpeg($original);
$newImage	= imagecreatetruecolor(154,110);
imagecopyresampled($newImage,$origImage,0,0,0,0,154,110,520,370); 

imagejpeg($newImage, $basepath.'TegsRegistration/views/mywebcam/uploads/thumbs/'.$filename);

echo '{"status":1,"message":"Success!","filename":"'.$filename.'"}';
?>