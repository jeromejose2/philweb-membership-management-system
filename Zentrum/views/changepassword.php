<?php
/*
 * @author Jerome Jose 2013-04-02
 * Purpose : Change Admin password.
 */

require_once('../controller/changepasswordprocess.php');
require_once('header.php');
?>


<link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
<script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
<script language="javascript" src="jscripts/checkinputs.js"></script>
<script type="text/javascript">
    function checkchangepassword()
    {
        var sPassword = document.getElementById("txtPassword");
        var sNewPassword = document.getElementById("txtNewPassword");
        var sConfirm = document.getElementById("txtConfPassword");

        var currentpassword = '<?php echo $_SESSION['actualpassword']; ?>';
        addOrRemoveErrorFieldClass("lblPassword","txtPassword");
        addOrRemoveErrorFieldClass("lblNewPassword","txtNewPassword");
        addOrRemoveErrorFieldClass("lblConfPassword","txtConfPassword");
    
        if (sPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
        {
             
            $('#light1').fadeIn('slow');
            //                $('#fade').fadeIn('slow');
            document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead').innerHTML = "INSUFFICIENT INFORMATION";
            document.getElementById('errorMsg').innerHTML = "Please fill in all the fields on the form to change your password.";
            sPassword.focus();
            return false;
        }
        if (sPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
        {
            var msg = "Password must not be less than 8 characters.";
            var msgtitle = "INVALID INPUT";
            $('#light1').fadeIn('slow');document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead').innerHTML = msgtitle;
            document.getElementById('errorMsg').innerHTML = msg;
            sPassword.focus();
            return false;
        }
        if (sNewPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
        {

            var msg = "Please fill in all the fields on the form to change your password.";
            var msgtitle = "INVALID INPUT";
            $('#light1').fadeIn('slow');document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead').innerHTML = msgtitle;
            document.getElementById('errorMsg').innerHTML = msg;
            sNewPassword.focus();
            return false;
        }
        if (sNewPassword.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
        {

            var msg = "Password must not be less than 8 characters.";
            var msgtitle = "INVALID INPUT";
            $('#light1').fadeIn('slow');document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead').innerHTML = msgtitle;
            document.getElementById('errorMsg').innerHTML = msg;
            sNewPassword.focus();
            return false;
        }
        if (sConfirm.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length == 0)
        { 

            var msg = "Please fill in all the fields on the form to change your password.";
            var msgtitle = "INSUFFICIENT INFORMATION";
            $('#light1').fadeIn('slow');document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead').innerHTML = msgtitle;
            document.getElementById('errorMsg').innerHTML = msg;
            sConfirm.focus();
            return false;
        }
        if (sConfirm.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '').length < 8)
        {

            var msg = "Password must not be less than 8 characters.";
            var msgtitle = "INVALID INPUT";
            $('#light1').fadeIn('slow');document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead').innerHTML = msgtitle;
            document.getElementById('errorMsg').innerHTML = msg;
            sConfirm.focus();
            return false;
        }
        if (jQuery.trim(sNewPassword.value) != jQuery.trim(sConfirm.value))
        {

            var msg = "Your passwords information do not match. Please try again.";
            var msgtitle = "UNMATCHING PASSWORDS";
            $('#light1').fadeIn('slow');document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead').innerHTML = msgtitle;
            document.getElementById('errorMsg').innerHTML = msg;
            sConfirm.focus();
            return false;
        }
        if (jQuery.trim(sPassword.value) != jQuery.trim(currentpassword))
        {
            var msg = "Your passwords information do not match. Please try again.";
            var msgtitle = "UNMATCHING PASSWORDS";
            $('#light1').fadeIn('slow');document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead').innerHTML = msgtitle;
            document.getElementById('errorMsg').innerHTML = msg;
            return false;
        }
        if (jQuery.trim(sNewPassword.value) == jQuery.trim(sPassword.value))
        {

            var msg = "New password cannot be similar to the current password.";
            var msgtitle = "INVALID INPUT";
            $('#light1').fadeIn('slow');document.getElementById('fade').style.display = 'block';
            document.getElementById('errorHead').innerHTML = msgtitle;
            document.getElementById('errorMsg').innerHTML = msg;
            return false;
        }
    
        $('#light11').fadeIn('slow');document.getElementById('fade').style.display = 'block';
        document.getElementById('title2').innerHTML = "CONFIRMATION";
        document.getElementById('msg2').innerHTML = "You are about to change your current password. Do you wish to continue?";
        return false;
    } 
    function cancellink()
    {
        window.location.href='filterandviewmembers.php';
    }
    function addOrRemoveErrorFieldClass(lblId,inputId)
    {
        var lblElem = elem(lblId);
        var inputElem = elem(inputId);

        if(inputElem.value.replace(/^\s+|\s+$/, '') == '' || inputElem.value.replace(/^\s+|\s+$/, '').length < 8) {
            addClass(lblElem,"errorField");
        } else {
            removeClass(lblElem,"errorField");
        }
        
    }
    $(document).ready(function(){
        var $body = $(this.ie6 ? document.body : document);
            $('#content-div').css({
                height:  '400px' 
            });
        if($.browser.msie)
        { 
            $('#fade').css({
                height:  $body.height() 
            });
                           
                        
        }else
        {
            $('#fade').css({
                width:  $body.width(),
                height:  $body.height() 
            });
                           
                            
        }
                   
    });
             
</script>
<form action="changepassword.php" method="post" enctype="multipart/form-data">
    <div id="page">
        <table width="100%">
            <tr>
                <td colspan="2"><div id="header" style="color: white;background-color: black;">&nbsp;&nbsp;&nbsp;Please enter the following information to change your password.</div>
                </td>
            </tr>
            <tr style="background-color:#E6E6E6; height:30px;">
                <td class="fontboldblack" style="width:400px;"><div id="lblPassword">&nbsp;&nbsp;*Current Password:</div></td>
                <td style="width:400px;">
                    &nbsp;&nbsp;<?php echo $txtPassword; ?>
                </td>
            </tr>
            <tr style="background-color:#a6a6a6; height:30px;">
                <td class="fontboldblack"><div id="lblNewPassword">&nbsp;&nbsp;*New Password:</div></td>
                <td>
                    &nbsp;&nbsp;<?php echo $txtNewPassword; ?>
                </td>
            </tr>
            <tr style="background-color:#E6E6E6; height:30px;">
                <td class="fontboldblack"><div id="lblConfPassword">&nbsp;&nbsp;*Confirm New Password:</div></td>
                <td>
                    &nbsp;&nbsp;<?php echo $txtConfPassword; ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center">
<?php echo $btnOk; ?>
                    &nbsp;&nbsp;
<?php echo $btnCancel; ?>
                </td>
            </tr>
        </table>

        <div id="light1" style="" class="white_content">
            <div style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black;padding-top: 5px;">
                <b id="errorHead"></b>
            </div>
            <br/>
            <p id="errorMsg"><br/></p>
            <br/>
<?php if ( isset($successMsg) ): ?>
                <input type="button" id="errorokay" value="OKAY"class="labelbutton_black" onclick ="window.location.href ='index.php';"/>
<?php else: ?>
                <input type="button" id="errorokay" value="OKAY"class="labelbutton_black" onclick =""/>
            <?php endif; ?>
        </div>

        <!-- ERROR MESSAGE(Invalid change password) -->
        <div id="light12" style="" class="white_content">
            <b> <div id="title" style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: white;padding-top: 5px;text-color:black">
                </div></b><br /><br /><div id="msg"></div>
            <br/><br/><br />
            <input id="btnClose" type="button" value="OKAY" class="labelbutton_black" />
        </div>
        <!-- END OF ERROR MESSAGE(Invalid change password) -->	
        <!-- CONFIRMATION MESSAGE -->
        <div id="light11" style="" class="white_content">
            <b> <div id="title2" style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: black;padding-top: 5px;">
                </div></b><br /><br /><div id="msg2"></div>
            <br/><br/><br />
<?php echo $passSubmit; ?>
            <input id="btnClose" type="button" value="CANCEL" class="labelbutton_black" />
        </div>
        <!-- END OF CONFIRMATION MESSAGE -->	
        <!-- ERROR MESSAGES -->
        <div id="light10" style="text-align: center;font-size: 16pt;height: 250px; margin-left:-95px;background-color: #e6e6e6;" class="white_content">
            <div id="title3" style="width: 100%;height: 27px;background-color: #a6a6a6;top: 0px;color: white;padding-top: 5px;">
            </div><br /><br /><div id="msg3"></div><br/><br/><br />
            <input id="passSubmit1" type="button" name="passSubmit1" value="OK" class="labelbutton2" />
        </div>
        <!-- END OF ERROR MESSAGES -->
    </div>
</form>

<script type="text/javascript">
    $('#passSubmit').live('click',function(){
        $('#light10').fadeOut('slow');
        $('#fade').fadeOut('slow');   
    });
    $('#passSubmit1').live('click',function(){
        $('#light10').fadeOut('slow');
        $('#fade').fadeOut('slow');   
    });

    $('#btnClose,#errorokay').live('click',function(){
        $('#light1').fadeOut('slow');
        $('#light11').fadeOut('slow');
        $('#light12').fadeOut('slow');
        $('#fade').fadeOut('slow');   
    });

       
<?php if ( isset($successMsg) ): ?>
                        
        var msg = "You have successfully changed your password. Please check your e-mail for your new log in credentials.";
        var msgtitle = "SUCCESSFUL PASSWORD CHANGE";
        document.getElementById('light1').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('errorHead').innerHTML = msgtitle;
        document.getElementById('errorMsg').innerHTML = msg;
        $("input#txtPassword,input#txtNewPassword,input#txtConfPassword").val("");
                                    
                        
<?php endif; ?>
<?php if ( isset($errorMessage) ): ?>
        var msg = '<?php echo $errorMessage; ?>';
        var msgtitle = "ERROR";
        document.getElementById('light1').style.display='block';
        document.getElementById('fade').style.display='block';
        document.getElementById('errorHead').innerHTML = msgtitle;
        document.getElementById('errorMsg').innerHTML = msg;
        $("input#txtPassword,input#txtNewPassword,input#txtConfPassword").val("");
<?php endif; ?>
        
</script>
<script>
    $(function () {
        $('input[type=text],[type=password]').bind('cut copy paste', function (e) {
            e.preventDefault();
        });
    });
</script>

