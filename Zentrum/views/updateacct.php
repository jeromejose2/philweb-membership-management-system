<?php
/*
 * Created by : Angelo Cubos
 * Date Created : April 01 2013
 * Purpose : view updateacct
 */
//include("header.php");
include("../controller/updateacctprocess.php");
require_once("header.php");
?>

<head>
    <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
    <script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
    <script language="javascript" src="jscripts/checkinputs.js"></script>

    <script language="javascript">
        function showData()
        {
            document.getElementById("result").style.visibility = "visible";
        }
    </script>
</head>
<body ondragstart="return false;" ondrop="return false;">
    <form id="frmupdateacct" method="post">
        <br>

        <div align="center"> 

            <div align="center"><b>USERNAME</b>&nbsp;&nbsp;&nbsp;
                <?php echo $ddlUserName; ?><br><br>
            </div>
            <div align="center"><?php //echo $btnSearch; ?></div>
            <br>
            <?php //$style='style="visibility:hidden"' ?>
            <div id="result" <?php if (strlen($userAccountID) == 0)
            {
                echo $style;
            } ?> >
        <table width="90%">    
            <tr>
                <td colspan="2">
                    <div id="tableheader" align="left">User Account Profile</div>
                </td>
            </tr>
            <tr style="background-color:#A8A8A8; height:30px;">
                <td width="45%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;Username</td>
                <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $userName; ?></td>
            </tr>
            <tr style="background-color:#D0D0D0; height:30px;">
                <td width="45%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;Date of Creation:</td>
                <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $dateCreated; ?></td>
            </tr>
            <tr style="background-color:#A8A8A8; height:30px;">
                <td width="45%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;Account Status </td>
                <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php
                    if ($status == 1)
                    {
                        echo "Active";
                    }
                    if ($status == 2)
                    {
                        echo "Suspended";
                    }
                    if ($status == 5)
                    {
                        echo "Terminated";
                    }
                    ?>        
                </td>
            </tr>
        </table>


                <table width="90%">
                    <tr>
                        <th align="center">First Name</th>
                        <th align="center">Middle Name</th>
                        <th align="center">Last Name</th>
                    </tr>
                    <tr>
                        <td align="center"><?php echo $txtFirstName; ?></td>
                        <td align="center"><?php echo $txtMiddleName; ?></td>
                        <td align="center"><?php echo $txtLastName; ?></td>
                    </tr>
                </table>
                <table width="90%">
                    <tr>
                        <th width="15%"></th>
                        <th width="15%"></th>
                        <th width="15%"></th>
                        <th width="15%"></th>
                        <th width="15%"></th>
                        <th width="15%"></th>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left">E-mail Address</td>
                        <td colspan="2" align="center"><?php echo $txtEmail; ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left">Position</td>
                        <td colspan="2" align="center"><?php echo $txtPosition; ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left">Access Rights</td>
                        <td colspan="2" align="center"><?php echo $ddlAccType; ?></td>
                    </tr>
                </table>

                <br><br>
                <div align="center">
                    <p><?php echo $btnBack; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo $btnSave; ?>
                    </p>
                </div>

            </div><!--result div-->
        </div><!--main div-->
        <div id="light1"  class="white_content">
            <div style="width: 100%;height: 27px;background-color: #a6a6a6;
                 top: 0px;color: black; padding-top: 0px;">
                <b id="errorHead"></b>
            </div>
            <br>
            <p id="errorMsg"><br></p>
            <br>
            <p id="button">

          <button type="button" class="labelbutton_black" label="OKAY" 
                onclick ="document.getElementById('light1').style.display = 'none';
        document.getElementById('fade').style.display = 'none';"/>OKAY</button>
            </p>
        </div>
        <div id="fade" class="black_overlay"></div>
    </form>
</body>

<?php include("footer.php"); ?>
<script type="text/javascript">


<?php if (strlen($errorTitle) > 0) : ?>
        document.getElementById('errorHead').innerHTML = '<?php echo $errorTitle; ?>';
        document.getElementById('errorMsg').innerHTML = '<?php echo $errorMessage; ?>';
    <?php if ($confirm == true) : ?>
            document.getElementById('button').innerHTML = '<?php echo addslashes($btnProceed); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="labelbutton_black" onclick="document.getElementById(\'light1\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">CANCEL</button> ';
    <?php endif; ?>
    <?php if ($success == true) : ?>
            document.getElementById('button').innerHTML = '<button type="button" class="labelbutton_black" onclick="window.location = \'useraccountprofile.php\';">OKAY</button> ';
    <?php endif; ?>
        document.getElementById('light1').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
<?php endif; ?>

</script>  



