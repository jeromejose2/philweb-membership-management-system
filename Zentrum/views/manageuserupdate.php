<?php
/*
 * Created by : Angelo Cubos
 * Date Created : April 01 2013
 * Purpose : view updateacct
 */
//include("header.php");
include("../controller/manageuserupdateprocess.php");
include("header.php");
?>

<head>
    <link rel="stylesheet" type="text/css" media="screen" href="css/default.css" />
    <script language="javascript" src="jscripts/jquery-1.5.2.min.js"></script>
    <script language="javascript" src="jscripts/checkinputs.js"></script>

    <script language="javascript">

        function showData()
        {
            var sa = document.getElementById("textarearemarks").value;
            document.getElementById('remarks').value = sa;
        }

    </script>
</head>
<body ondragstart="return false;" ondrop="return false;">
    <form id="frmmanageuserupdate" method="post">    
        <?php echo $remarks; ?>
        <br>

        <div align="center"> 

            <div align="center"><b>USERNAME</b>&nbsp;&nbsp;&nbsp;
                <?php echo $ddlUserName; ?>&nbsp;<?php echo $btnSearch; ?></div>
            <br>

            <div id="result" <?php if (strlen($userAccountID) == 0)
            {
                echo $style;
            } ?> >

                <table width="50%">    
                    <tr>
                        <td colspan="2">
                        <div><h3 style="text-decoration: underline;" align="left">
                                MANAGE USER ACCOUNT</h3>
                        </div>
                        </td>
                    </tr>
                    <tr style="height:30px;">
                        <td width="5%" align="left">USERNAME&nbsp;&nbsp;&nbsp;</td>
                        <td width="45%" style="border-style:solid;">&nbsp;&nbsp;&nbsp;
                            <?php echo $UserName; ?></td>
                    </tr>
                </table>
                <br>
                <table width="50%">    
                    <tr>
                        <td colspan="2">
                        <div><h3 style="text-decoration: underline;" align="left">
                                ACCOUNT STATUS INFORMATION</h3>
                        </div>
                        </td>
                    </tr>
                    <tr style="height:30px;">
                        <td width="25%" align="left">CURRENT ACCOUNT STATUS</td>
                        <td width="25%" style="text-align: center; border-style:solid;">
                            <?php
                            if ($status == 1)
                            {
                                echo "ACTIVE";
                            }
                            if ($status == 2)
                            {
                                echo "SUSPENDED";
                            }
                            if ($status == 5)
                            {
                                echo "TERMINATED";
                            }
                            ?></td>
                    </tr>
                    <tr style="height:30px;">
                        <td width="25%" align="left">SET ACCOUNT STATUS</td>
                        <td width="25%" style="text-align: center;border-style:solid; ">
                            <?php echo $ddlAccountStatus ?>
                        </td>
                    </tr>
                </table>

                <table width="100%" border="0">
                    <tr>
                        <td width="25%"></td>
                        <td width="25%" align="left">REMARKS</td>
                        <td colspan="2"  align="left">
                        <textarea id="textarearemarks" style="width:400px; height:80px;" align="left">
                            <?php echo $remarks->Text; ?>
                        </textarea>

                            <p>
                                <strong>MAX. 500 CHARACTERS <em id="count"></em></strong>
                            </p>
                        </td>
                    </tr>

                </table>
                <br><br>
                <div align="center">
                    <p><?php echo $btnBack; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo $btnSave; ?>
                    </p>
                </div>

            </div><!--result div-->
        </div><!--main div-->
        <div id="light1"  class="white_content">
            <div style="width: 100%;height: 27px;background-color: #a6a6a6;
                 top: 0px;color: black; padding-top: 0px;">
                <b id="errorHead"></b>
            </div>
            <br>
            <p id="errorMsg"><br></p>
            <br>
            <p id="button">
            
                <button type="button" class="labelbutton_black" label="OKAY" onclick ="document.getElementById('light1').style.display = 'none';
            document.getElementById('fade').style.display = 'none';"/>OKAY</button>
            </p>
        </div>
        <div id="fade" class="black_overlay"></div>
    </form>
</body>

<?php include("footer.php"); ?>
<script type="text/javascript">

<?php if (strlen($errorTitle) > 0) : ?>
            document.getElementById('errorHead').innerHTML = '<?php echo $errorTitle; ?>';
            document.getElementById('errorMsg').innerHTML = '<?php echo $errorMessage; ?>';
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
<?php endif; ?>

<?php if (strlen($confirmTitle) > 0) : ?>
            document.getElementById('errorHead').innerHTML = '<?php echo $confirmTitle; ?>';
            document.getElementById('errorMsg').innerHTML = '<?php echo $confirmMessage; ?>';
            document.getElementById('button').innerHTML = '<?php echo addslashes($btnOkay); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="labelbutton_black" onclick="document.getElementById(\'light1\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">CANCEL</button> ';
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
<?php endif; ?>

<?php if (strlen($successTitle) > 0) : ?>
            document.getElementById('errorHead').innerHTML = '<?php echo $successTitle; ?>';
            document.getElementById('errorMsg').innerHTML = '<?php echo $successMessage; ?>';
            document.getElementById('button').innerHTML = '<button type="button" class="labelbutton_black" onclick="window.location = \'manageuser.php\';">OKAY</button>';
            document.getElementById('light1').style.display = 'block';
            document.getElementById('fade').style.display = 'block';
<?php endif; ?>

<?php if ($success != true) : ?>
            document.getElementById("textarearemarks").value = '<?php echo $_SESSION["remarks"]; ?>';
<?php endif; ?>

        maxCharacters = 500;

        $('#count').text(maxCharacters);

        $('textarea').bind('keyup keydown', function() {
            var count = $('#count');
            var characters = $(this).val().length;

            if (characters > maxCharacters) {
                count.addClass('over');
            } else {
                count.removeClass('over');
            }

            count.text(maxCharacters - characters);
        });
</script>  



