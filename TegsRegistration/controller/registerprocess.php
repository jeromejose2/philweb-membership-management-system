<?php
/**
 * The controller page. This module will be used for registration of Timor e-Games members.
 * @author Jerome Jose May 28
 * @copyright (c) 2013, PhilWeb Corporation
 */


App::LoadModuleClass("MembershipTimor","MTCountries");
App::LoadModuleClass("MembershipTimor","MTAccounts");
App::LoadModuleClass("MembershipTimor","MTMemberInfo");
App::LoadModuleClass("MembershipTimor","MTAccountDetails");
App::LoadModuleClass("MembershipTimor","MTRefIdentification");
App::LoadModuleClass("MembershipTimor","MTCards");
App::LoadModuleClass("MembershipTimor","MTMemberCards");
App::LoadModuleClass("MembershipTimor","MTAuditTrail");

App::LoadControl("TextBox");
App::LoadControl("ComboBox");
App::LoadControl("Button");
App::LoadControl("Hidden");
App::LoadCore("PHPMailer.class.php");
App::LoadCore("DateSelector.class.php");
$mtAuditTrail = new MTAuditTrail();

$mtcountries         = new MTCountries();
$mtaccounts          = new MTAccounts();
$mtmemberinfo        = new MTMemberInfo();
$mtacctdtls          = new MTAccountDetails();
$mtrefidentification = new MTRefIdentification();
$mtcards             = new MTCards();
$mtmembercards       = new MTMemberCards();

$txtfn           = new TextBox("txtfn","txtfn","First Name ");
$txtfn->Length   = 30;
$txtfn->CssClass = 'input-full';
$txtfn->Args     = "size='40px' autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";

$txtln           = new TextBox("txtln","txtln","Last Name ");
$txtln->Length   = 30;
$txtln->CssClass = 'input-full';
$txtln->Args     = "size='40px' autocomplete='off' onkeypress='javascript: return AlphaAndSpaces(event);'";

$txtaddr           = new TextBox("txtaddr","txtaddr","Address");
$txtaddr->CssClass = 'input-full';
$txtaddr->Args     = "autocomplete='off' onkeypress='javascript: return AlphaNumericAndSpacesOnly(event);'";

$txtcn           = new TextBox("txtcn","txtcn","Contact Number");
$txtcn->Length   = 20;
$txtcn->CssClass = 'input-full';
$txtcn->Args     = "size='40px' autocomplete='off' onkeypress='javascript: return OnlyNumbers(event);'";

$txtemail           = new TextBox("txtemail","txtemail","E-mail Address");
$txtemail->CssClass = 'input-full';
$txtemail->Args     = "size='40px' autocomplete='off'";

//$ddlmonth = new ComboBox("ddlmonth", "ddlmonth", "Month");
//$opt_month = null;
//$opt_month[] = new ListItem("--", "0", true);
//for ($i = 1; $i <= 12; $i++)
//{
//    $opt_month[$i] = new ListItem(date("F", mktime(0, 0, 0, $i, 10)), $i);
//}
//$ddlmonth->Items = $opt_month;
//$ddlmonth->CssClass='input-normal';
//$ddlday = new ComboBox("ddlday", "ddlday", "Day");
//$opt_day = null;
//$opt_day[] = new ListItem("--", "0", true);
//for ($i = 1; $i <= 31; $i++)
//{
//    $opt_day[$i] = new ListItem($i, $i);
//}
//$ddlday->Items = $opt_day;
//$ddlday->CssClass='input-auto';
//$ddlyear = new ComboBox("ddlyear", "ddlyear", "Year");
//$opt_yr = null;
//$opt_yr[] = new ListItem("--", "0", true);
//$start = date("Y") - 16;
//for ($i = 1; $i <= 101; $i++)
//{
//    $start -= 1;
//    $opt_yr[$i] = new ListItem($start, $start);
//}
//$ddlyear->Items = $opt_yr;
//$ddlyear->CssClass='input-normal';

$ddlsex           = new ComboBox("ddlsex","ddlsex","Sex");
$opt_sex          = null;
$opt_sex[]        = new ListItem("--","0",true);
$opt_sex[]        = new ListItem("Male","1");
$opt_sex[]        = new ListItem("Female","2");
$ddlsex->Items    = $opt_sex;
$ddlsex->CssClass = 'input-normal';

$ddlnationality                  = new ComboBox("ddlnationality","ddlnationality","Nationality");
$opt_country                     = null;
$opt_country[]                   = new ListItem("--","0",true);
$opt_country[]                   = new ListItem("Timor Leste","212");
$opt_country[]                   = new ListItem("Portugal","174");
$opt_country[]                   = new ListItem("Malaysia","148");
$opt_country[]                   = new ListItem("China","45");
$opt_country[]                   = new ListItem("Indonesia","96");
$opt_country[]                   = new ListItem("- - - - - - - ","0");
$ddlnationality->Items           = $opt_country;
//$array_countries = $mtcountries->SelectCountriesByPriority();
$array_countries                 = $mtcountries->selectspecialcountry();
$list_countries                  = new ArrayList();
$list_countries->AddArray($array_countries);
$ddlnationality->DataSource      = $list_countries;
$ddlnationality->DataSourceText  = "CountryName";
$ddlnationality->DataSourceValue = "CountryID";
$ddlnationality->CssClass        = 'input-normal';
$ddlnationality->DataBind();

$ddlstatus           = new ComboBox("ddlstatus","ddlstatus","Status"); // this is now marital status
$opt_status          = null;
$opt_status[]        = new ListItem("--","0",true);
$opt_status[]        = new ListItem("Single","1");
$opt_status[]        = new ListItem("Married","2");
$opt_status[]        = new ListItem("Widowed","3");
$ddlstatus->CssClass = 'input-normal';
$ddlstatus->Items    = $opt_status;

$btnSubmit           = new Button("btnSubmit","btnSubmit","Submit");
$btnSubmit->CssClass = "tegs-submit-btn";
$btnSubmit->IsSubmit = true;
$btnSubmit->Args     = "onclick='javascript: return checkregistrationinputs();'";

$hidimgpath = new Hidden("hidimgpath","hidimgpath","Image Path");
$hidid      = new Hidden("hidid","hidid","Membership ID");

$hidid->Text = $id; // $id came from the file included - 'membershipcard.php'.

$hidBday = new Hidden("hidBday", "hidBday", "Birthday");

$fproc = new FormsProcessor();
$fproc->AddControl($txtfn);
$fproc->AddControl($txtln);
$fproc->AddControl($txtaddr);
$fproc->AddControl($txtcn);
$fproc->AddControl($txtemail);
//$fproc->AddControl($ddlmonth);
//$fproc->AddControl($ddlday);
//$fproc->AddControl($ddlyear);
$fproc->AddControl($ddlsex);
$fproc->AddControl($ddlnationality);
$fproc->AddControl($ddlstatus);
$fproc->AddControl($btnSubmit);
$fproc->AddControl($hidid);
$fproc->AddControl($hidimgpath);
$fproc->AddControl($hidBday);

$fproc->ProcessForms();

$houserules = false;
$year = '0000';
$month = '00';
$day = '00';
$myCalendar = new tc_calendar("hidBday",true);
$myCalendar->setIcon("images/iconCalendar.gif");
$myCalendar->setDate($day,$month,$year);
$myCalendar->setPath("./");
$myCalendar->setYearInterval(1900,(date('Y') - 17));
$myCalendar->dateAllow('1900-01-01','$ddlyear-$ddlmonth-$ddlday');
$myCalendar->disabledDay("Sat");
$myCalendar->disabledDay("sun");

if ( $fproc->IsPostBack )
{
 $myCalendar->setDate($_POST['hidBday_day'], $_POST['hidBday_month'], $_POST['hidBday_year']);
    $hidBday->Text = $_POST['hidBday_year'] . '-' . $_POST['hidBday_month'] . '-' . $_POST['hidBday_day'];

    if ( $btnSubmit->SubmittedValue == "Submit" )
    {


        if ( isset($_FILES["filebrowse"]["name"]) && $_FILES["filebrowse"]["error"] != 4 )
        {
            $allowedExts = array("gif","jpeg","jpg","png");
            $extension = end(explode(".",$_FILES["filebrowse"]["name"]));

            if ( (($_FILES["filebrowse"]["type"] == "image/gif")
                      || ($_FILES["filebrowse"]["type"] == "image/jpeg")
                      || ($_FILES["filebrowse"]["type"] == "image/jpg")
                      || ($_FILES["filebrowse"]["type"] == "image/png"))
                      || ($_FILES["filebrowse"]["size"] < 1048576)
                      && in_array($extension,$allowedExts) )
            {

                if ( $_FILES["filebrowse"]["error"] > 0 )
                {
                    $errorMsg = "Return Code: " . $_FILES["filebrowse"]["error"] . "<br>";
                }
                else
                {
                    $imagename        = $hidid->Text . ".jpg";
                    $hidimgpath->Text = $imagename;
                    move_uploaded_file($_FILES["filebrowse"]["tmp_name"],"mywebcam/uploads/original/" . $imagename);
                    //$msg =  "Image successfully uploaded.";
                    //$msgtitle = "SUCCESSFUL NOTIFICATION";
                    //$uploadSuccessful = true;
                }
            }
            else
            {
                $errorMsg = "Invalid file";
            }
        }


        // check if first and last name of member already exist.
        $arrNames  = $mtacctdtls->SelectAccountNameExist($txtfn->SubmittedValue,$txtln->SubmittedValue);
        $nameExist = $arrNames[0]["Exist"];

        $erroremail = false;
        if ( $nameExist >= 1 )
        {
            $errorMsg = "The name of the member already exist.";
        }
        else
        {
            //check email
            if ( $txtemail->SubmittedValue != NULL )
            {
                $arrEmails = $mtacctdtls->SelectEmailExist(trim($txtemail->SubmittedValue));
                if ( $arrEmails[0][0] != 0 )
                {
                    $errorMsg   = "Email address already exist.";
                    $erroremail = false;
                }
                else
                {
                    $erroremail = true;
                }
            }
            else
            {
                $erroremail = true;
            }
            
            if ( $erroremail == true )
            {
                $_SESSION['hasSession']      = 1;
                $mtaccounts->StartTransaction();
                
                // insert into accounts
                $insertacct["UserName"]      = date("YmdHis");
                $insertacct["Password"]      = md5("password");
                $insertacct["AccountTypeID"] = 5;
                $insertacct["Status"]        = '0';
                $insertacct["DateCreated"]   = "now_usec()";
                $insertacct["CreatedByAID"]  = ''; //$_SESSION['aid'];
                $mtaccounts->Insert($insertacct);
                if ( $mtaccounts->HasError )
                {
                    $mtaccounts->RollBackTransaction();
                    $errorMsg = "An error occured: " . $mtaccounts->getErrors();
                }
                else
                {
                    $mtaccounts->CommitTransaction();
                    $lastInsertedID = $mtaccounts->LastInsertID;

                    $mtacctdtls->StartTransaction();

                    // update account details
                    $updatedtls["AID"]          = $lastInsertedID;
                    $updatedtls["FirstName"]    = $txtfn->SubmittedValue;
                    $updatedtls["LastName"]     = $txtln->SubmittedValue;
                    $updatedtls["Address"]      = $txtaddr->SubmittedValue;
                    $updatedtls["MobileNumber"] = $txtcn->SubmittedValue;
                    $updatedtls["Email"]        = $txtemail->SubmittedValue;
                    $mtacctdtls->UpdateByArray($updatedtls);
                    if ( $mtacctdtls->HasError )
                    {
                        $mtacctdtls->RollBackTransaction();
                        $errorMsg = "An error occured: " . $mtacctdtls->getErrors();
                    }
                    else
                    {
                        $mtacctdtls->CommitTransaction();

                        $arr          = $mtmemberinfo->SelectMemberInfoByAID($lastInsertedID);
                        $memberinfoid = $arr[0]["MemberInfoID"];

                        $mtmemberinfo->StartTransaction();


                        // update memberinfo
                        $updateinfo["MemberInfoID"]         = $memberinfoid;
                        $updateinfo["FirstName"]            = $txtfn->SubmittedValue;
                        $updateinfo["LastName"]             = $txtln->SubmittedValue;
                        $updateinfo["Birthdate"]            = $hidBday->Text; 
                        $updateinfo["Address"]              = $txtaddr->SubmittedValue;
                        $updateinfo["ContactNumber"]        = $txtcn->SubmittedValue;
                        $updateinfo["Email"]                = $txtemail->SubmittedValue;
                        $updateinfo["IdentificationNumber"] = ''; //$txtidno->SubmittedValue;
                        $updateinfo["DateCreated"]          = "now_usec()";
                        $updateinfo["CreatedByAID"]         = '';
                        $updateinfo["RegistrationOrigin"]   = '2';
                        $updateinfo["PhotoFileName"]        = $hidimgpath->Text;
                        $updateinfo["MaritalStatus"]        = $ddlstatus->SubmittedValue;
                        $updateinfo["Status"]               = '0';
                        $updateinfo["Nationality"]          = $ddlnationality->SubmittedValue;
                        $updateinfo["Gender"]               = $ddlsex->SubmittedValue;
                        $alias                              = ($ddlsex->SubmittedValue == '1') ? 'Mr.' : 'Ms.';

                        $mtmemberinfo->UpdateByArray($updateinfo);
                        if ( $mtmemberinfo->HasError )
                        {
                            $mtmemberinfo->RollBackTransaction();
                            $errorMsg = "An error occured: " . $mtmemberinfo->getErrors();
                        }
                        else
                        {
                            $mtmemberinfo->CommitTransaction();


                            //send EMAIL
                            $email   = $txtemail->SubmittedValue;
                            $surname = $txtln->SubmittedValue;
                            $mtAuditTrail->StartTransaction();
                            if ( $email != NULL )
                            {
                                $ds           = new DateSelector();
                                $ds->SetTimeZone("Asia/Dili");
                                $date         = Date('m/d/Y',strtotime($ds->CurrentDate));
                                $time         = $ds->GetCurrentDateFormat("h:i A");
                                $pm           = new PHPMailer();
                                $pm->AddAddress($email,$surname);
                                $pm->IsHTML(true);
                                $pm->Body     = "<HTML><BODY>						
                                                Dear $alias $surname,
                                                <br /><br />
                                                Good Day!
                                                <br /><br />
                                                We have received your e-Games membership registration on this date $date and time $time. 
                                                <br /><br />
                                                To finalize your membership application, visit the e-Games casino at  Level 2, Unit 01, CBD IV, Timor Plaza and present your Valid ID.
                                                <br /><br />
                                                For further inquiries, email us at membership@egames.com.tl.
                                                <br /><br />
                                                Looking forward to give you the e-Games experience!
                                                <br /><br />
                                                Regards,<br />
                                                Customer Support<br />
                                                e-Games<br />
                                                </BODY></HTML>";
                                $pm->From     = "membership@egames.com.tl";
                                $pm->FromName = "e-Games Marketing Team";
                                $pm->Host     = "localhost";
                                $pm->Subject  = "e-Games Membership Registration";
                                $email_sent   = $pm->Send();
                                if ( $email_sent )
                                {
                                    // log to audit trail

                                    $auditLogParameter['SessionID']            = '';
                                    $auditLogParameter['AID']                  = '';
                                    $auditLogParameter['AuditTrailFunctionID'] = "5";
                                    $auditLogParameter['TransDetails']         = "Register online member: " . $txtfn->SubmittedValue . " " . $txtln->SubmittedValue;
                                    $auditLogParameter['RemoteIP']             = $_SERVER['REMOTE_ADDR'];
                                    $auditLogParameter['TransDateTime']        = "now_usec()";
                                    $mtAuditTrail->Insert($auditLogParameter);

                                    if ( $mtAuditTrail->HasError )
                                    {
                                        $mtAuditTrail->RollBackTransaction();
                                        $errorMsg = "An error occured: " .
                                                  $mtAuditTrail->getErrors();
                                    }
                                    else
                                    {
                                        $mtAuditTrail->CommitTransaction();
                                        $houserules       = true;
                                        $hidimgpath->Text = "";
                                    }
                                }
                            }
                            else
                            {
                                // log to audit trail

                                $auditLogParameter['SessionID']            = '';
                                $auditLogParameter['AID']                  = '';
                                $auditLogParameter['AuditTrailFunctionID'] = "5";
                                $auditLogParameter['TransDetails']         = "Register online member: " . $txtfn->SubmittedValue . " " . $txtln->SubmittedValue;
                                $auditLogParameter['RemoteIP']             = $_SERVER['REMOTE_ADDR'];
                                $auditLogParameter['TransDateTime']        = "now_usec()";
                                $mtAuditTrail->Insert($auditLogParameter);

                                if ( $mtAuditTrail->HasError )
                                {
                                    $mtAuditTrail->RollBackTransaction();
                                    $errorMsg = "An error occured: " .
                                              $mtAuditTrail->getErrors();
                                }
                                else
                                {
                                    $mtAuditTrail->CommitTransaction();
                                    $houserules       = true;
                                    $hidimgpath->Text = "";
                                }
                            }
                        }
                    }
                }
            }//
        }
    }
}
?>
