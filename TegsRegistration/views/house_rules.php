<?php include("membershipcard.php");
if(!isset($_SESSION['language']))
{
    app::pr("<script>window.location.href='registration.php';</script>");
}
?>
<?php if($_SESSION['language'] == 'english'):?>
<!DOCTYPE html>
<html>
    
    <head>
        
        <title>TEGS Website Registration</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        
        <link href="css/style.css" rel="stylesheet" media="screen">
        
    </head>
    
    <body>
        
        <div class="container" id="page-container">
            
            <div class="row-fluid" id="header-container">
                
                <div class="span4"><img src="img/e_gameslogo.jpg"></div>
                
                <div class="span4 pull-right"></div>
                
            </div><!-- #header-container-->
            
            <div id="main-content">

                <div id="content-wrapper-full">

                    <h2 class="page-title">House Rules</h2>

                    <div class="row-fluid full-content">
                        <h5 class="">NO E-GAMES MEMBERSHIP REGISTRATION, NO CASH, NO PLAY</h5>
                        <br/>
                        <h6>NON-ELIGIBLE PLAYERS</h6>
                        <br/>1.&nbsp;&nbsp;Persons who are under 17 years of age; students, regardless of age, from any school,college or university in Timor Leste are also restricted from entering, playing in the casino and becoming a member.
                        <br/>2.&nbsp;&nbsp;Members of the defense and police forces or paramilitary forces of any nationality, whenever in uniform or not.
                        <br/>3.&nbsp;&nbsp;Individuals whose access were barred by the Inspectorate General of Gaming.
                        <br/><br/>
                        <h5 class="">OPERATIONAL RULES</h5>
<br/>1.&nbsp;&nbsp;A "No Membership Registration, No Entry" policy will be strictly applied. Players who are not registered in the membership program will be asked to leave the premises.
<br/>2.&nbsp;&nbsp;Access to the e-Games casino is exclusive to members. Persons who wish to register must present a valid ID and accomplish the Membership Application Form.
<br/>3.&nbsp;&nbsp;e-Games Management reserves the right to deny entry to any person.
<br/>4.&nbsp;&nbsp;Companions of members will not be allowed to enter nor play inside the e-Games casino. No bystanders are allowed.
<br/>5.&nbsp;&nbsp;Before placing a bet, the Member shall be responsible for reading and understanding the following:
<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.House rules - posted within the e-Games casino
<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.Game Rules and mechanics found in the "Help" section of each game
<br/>6.&nbsp;&nbsp;The Member accepts that the determination of the odds of winning, the winning outcome, and the applicable payouts is the sole discretion of e-Games.
<br/>7.&nbsp;&nbsp;The Member shall be fully aware that there is a risk of losing credit by placing a bet and participating in the games.
<br/>8.&nbsp;&nbsp;Any application or system failure voids all plays and pays. e-Games' electronic record, available from the gaming server, shall be the sole and final basis in case of any dispute arising out of, or relating to the game or winning. All claims must be resolved before the particular betting credit is redeemed.
<br/>9.&nbsp;&nbsp;Members must redeem the remaining betting credits before leaving the gaming station or slot machine. Management is not responsible for loss of any betting credits. Any unclaimed credits left for 30 minutes or more shall be redeemed in favor of the house.
<br/>10.&nbsp;&nbsp;Redemption or cash-out above $5,000 shall be given in the following manner: The first $5,000 shall be given in cash and any amount in excess of $5,000 shall be in a cheque that will be given the following day.
<br/>11.&nbsp;&nbsp;e-Games Management may suspend or cancel certain games without prior notice.
                    </div>
                </div><!-- .content-wrapper-fu -->

            </div><!-- #main-content -->
            
        </div><!-- #page-container -->
        
        <script>
            
            function tegspopup(url, title, w, h) {
                var left = (screen.width/2)-(w/2);
                var top = (screen.height/2)-(h/2);
                return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
            } 
            
        </script>
        
    </body>
    
</html>
<?php endif;?>


<?php if($_SESSION['language'] == 'bahasa'):?>
<!DOCTYPE html>
<html>
    
    <head>
        
        <title>TEGS Website Registration</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        
        <link href="css/style.css" rel="stylesheet" media="screen">
        
    </head>
    
    <body>
        
        <div class="container" id="page-container">
            
            <div class="row-fluid" id="header-container">
                
                <div class="span4"><img src="img/e_gameslogo.jpg"></div>
                
                <div class="span4 pull-right"></div>
                
            </div><!-- #header-container-->
            
            <div id="main-content">

                <div id="content-wrapper-full">

                    <h2 class="page-title" style="font-size: 17px;">REGULAMENTU-REGULAMENTU JOGU IHA KASINO</h2>
<br/>
                    <div class="row-fluid full-content">
                        <h5 class="">LAIHA REJISTRASAUN MEMBRU E-GAMES NIAN, LAIHA OSAN LABELE JOGA</h5>
                        <br/>
                        <h6>JOGADOR SIRA NE'EBE LA KOMPLETA KRITERIA/REKEREMENTU</h6>
                        <br/>1.&nbsp;&nbsp;Ema ne'ebe idade 17 ba kraik; estudante sira, preokupa ho idade, husi eskola, kolejio ou universidade iha Timor Leste mos limita atu tama.
                        <br/>2.&nbsp;&nbsp;Membru defeza polisia sira ou korporasaun militar nasionalidae nian, bainhira ho farda.
                        <br/>3.&nbsp;&nbsp;Individu sira ne'ebe asesu sei bandu husi inspetoria jeral jogu nian.
                        <br/><br/>
                        <h5 class="">REGULAMENTU-REGULAMENTU OPERASAUN NIAN</h5>
                        <br/>1.&nbsp;&nbsp;"laiha rejistrasaun membru nian, labele tama" sei utiliza politika ho diak (ketat). Jogador sira ne'ebe seidauk rejistu iha programa membru nian sei husu atu fila kedas.
                        <br/>2.&nbsp;&nbsp;Membru nian dalan tama ba kasino e-Games esklusivu ou mesak. Ema ne'ebe hakarak atu rejistu tenke hatudu ID ida ne'ebe validu, prenxe/finaliza formulariu aplikasaun membru e-Games nian, no nia depozito maizumenus 25 dolares amerikano.
                        <br/>3.&nbsp;&nbsp;Jestaun e-Games mos iha direitu atu rejeita ema ne'ebe de'it mak atu tama.
                        <br/>4.&nbsp;&nbsp;Amigos membru nian sei la permiti atu tama halimar iha kasino e-Games nia laran. Sei la permiti wainhira la kompleta kriteria.
                        <br/>5.&nbsp;&nbsp;Molok tau osan taru, membru ne'e tenke le'e no kompriende  lai hanesan tuir mai ne'e:
                        <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a.Regulamentu-regulamentu kasino nian -  koloka ho kasino e-Games
                        <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b.Regulamentu jogu no mekániku hetan tihaona "Ajuda" iha kada seksaun jogu nia laran.
                        <br/>6.&nbsp;&nbsp;Membru simu katak determinasaun husi manan haksesuk, manan resultado ne'e, no fo sai politika ida ne'ebe e-Games bele utiliza.
                        <br/>7.&nbsp;&nbsp;Membru ne'e tenke ho konsensia katak sei lakon kreditu wainhira ita taru osan ou partisipa iha jogu sira ne'e.
                        <br/>8.&nbsp;&nbsp;Kada aplikasaun ou sistema ne'ebe failha atu halimar sei selu valor/folin makina elektronika e-Games nian. Disponivel husi serbidor jogu nian, tenke kli'ik no baze final husi haksesuk nian mak sai hanesan rezultado, ou ne'ebe relasiona ho jogu no vitória. Ejizensia sira hotu ne'e tenke resolve ona molok selu kreditu foun osan taru nian.
                        <br/>9.&nbsp;&nbsp;Membru sira tenke selu osan restu taru nian molok husik hela fatin joga ou makina kuak. Jestaun sei la responsabilidade ba kreditu osan taru sira ne'ebe lakon. Kada kreditu ne'ebe la husu iha 30 minutus ou liu sei sai propriedade kasino nian.
                        <br/>10.&nbsp;&nbsp;Rezgate ou pagamentu 5.000 dolares ba leten tenke selu kedas. No ho montante ne'ebe bo'ot sei selu ho modelu xeke (cek) iha loron tuir mai.
                        <br/>11.&nbsp;&nbsp;e-Games Management may suspend or cancel certain games without prior notice.
                    </div>
                </div><!-- .content-wrapper-fu -->

            </div><!-- #main-content -->
            
        </div><!-- #page-container -->
        
        <script>
            
            function tegspopup(url, title, w, h) {
                var left = (screen.width/2)-(w/2);
                var top = (screen.height/2)-(h/2);
                return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
            } 
            
        </script>
        
    </body>
    
</html>
<?php endif;?>