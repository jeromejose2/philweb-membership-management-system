/**************************************************
* Author: Jerome Jose
* Date Created: May 7, 2013
*************************************************/
function validateEmail(elementValue)
{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(elementValue);
}
	
function elem(id)
{
    return document.getElementById(id); 
}
function addOrRemoveErrorFieldClass(lblId,inputId)
{
    var lblElem = elem(lblId);
    var email = document.getElementById("txtemail").value;
    email = email.replace(/^\s+|\s+$/, '');
    var regEx=/[a-zA-Z0-9]/;
    email = $.trim(email);
    if(inputId == '' || inputId == 0) {  
        addClass(lblElem,"errorField");
    } else {
        removeClass(lblElem,"errorField");
        removeClass(elem('lblemail'),"errorField");
    }
    if ((email.length > 0) || (email != ""))
    {
        if(echeck(email)==false)
        {
            addClass(elem('lblemail'),"errorField");
        }else
        {
            removeClass(elem('lblemail'),"errorField");
        }
    }else
    {
        removeClass(elem('lblemail'),"errorField");
    }
}

function hasClass(ele,cls) 
{
    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

function removeClass(ele,cls) 
{
    if (hasClass(ele,cls)) {
        var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
        ele.className=ele.className.replace(reg,' ');
    }
}
function AlphaNumericAndSpacesOnly(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
        return false;

    return true;
}
function OnlyNumbers(event)
{
    var e = event || evt; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;

}
function checkregistrationinputs()
{
    var lname = document.getElementById("txtln").value;
    var fname = document.getElementById("txtfn").value;
    var email = document.getElementById("txtemail").value;
    //    var month =  document.getElementById("ddlmonth").options[document.getElementById("ddlmonth").selectedIndex].value;
    //    var day =  document.getElementById("ddlday").options[document.getElementById("ddlday").selectedIndex].value;
    //    var year =  document.getElementById("ddlyear").options[document.getElementById("ddlyear").selectedIndex].value;
    var sex =  document.getElementById("ddlsex").options[document.getElementById("ddlsex").selectedIndex].value;
    var terms = document.getElementById('terms').checked;

    var bday = document.getElementById("hidBday").value;
    var date_array = bday.split("-");
    var month = date_array[1];
    var day = date_array[2];
    var year = date_array[0];

    bday = bday.replace(/^\s+|\s+$/, '');
    email = email.replace(/^\s+|\s+$/, '');
    var regEx=/[a-zA-Z0-9]/;

    fname = $.trim(fname);
    lname = $.trim(lname);
    email = $.trim(email);
    
    addOrRemoveErrorFieldClass("lblfn",fname);
    addOrRemoveErrorFieldClass("lblln",lname);
    addOrRemoveErrorFieldClass("lblbday",month);
    addOrRemoveErrorFieldClass("lblbday",day);
    addOrRemoveErrorFieldClass("lblbday",year);
    addOrRemoveErrorFieldClass("lblsex",sex);
    addOrRemoveErrorFieldClass("lblemail",email);
    
    document.getElementById('errorHead').innerHTML = "INCOMPLETE INFORMATION";
    if ((fname.length == 0) || (fname == ""))
    {
        document.getElementById('errorMsg').innerHTML = "Kindly fill out all required fields.";
        $('#light1').fadeIn('slow');
        document.getElementById('fade').style.display='block';
        return false;
    }
    
    if ((lname.length == 0) || (lname == ""))
    {
        document.getElementById('errorMsg').innerHTML = "Kindly fill out all required fields.";
        $('#light1').fadeIn('slow');
        document.getElementById('fade').style.display='block';
        return false;
    }
    
    if (month == 00 || day == 00 || year == 0000)
    {        
        document.getElementById('errorMsg').innerHTML = "Kindly fill out all required fields.";
        $('#light1').fadeIn('slow');
        document.getElementById('fade').style.display='block';
        return false;
    }else if (month != 00 && day != 00 && year != 0000)
    {
        if (calculateAge(new Date(year, month - 1, day)) < 17) {            
            document.getElementById('errorMsg').innerHTML = "Age below 17 years old is prohibited.";
            $('#light1').fadeIn('slow');
            document.getElementById('fade').style.display='block';
            return false;
        }
    }
    
    if (sex == 0)
    {
        document.getElementById('errorMsg').innerHTML = "Kindly fill out all required fields.";
        $('#light1').fadeIn('slow');
        document.getElementById('fade').style.display='block';
        return false;
    }
    
    if ((email.length > 0) || (email != ""))
    {
        if(echeck(email)==false)
        {
            document.getElementById('errorMsg').innerHTML = "Kindly fill out all required fields.";
            $('#light1').fadeIn('slow');
            document.getElementById('fade').style.display='block';
            return false;
        }
    }
    if (terms == false)
    {
        document.getElementById('errorMsg').innerHTML = "Please check the House Rules & Terms and Condition";
        $('#light1').fadeIn('slow');
        document.getElementById('fade').style.display='block';
        return false ;
    }
     
    if (!$('#terms').is(":checked"))
    {
        document.getElementById('errorMsg').innerHTML = "Please check the House Rules & Terms and Condition";
        $('#light1').fadeIn('slow');
        document.getElementById('fade').style.display='block';
        return false ;
    }
    return true;
    //    $('#light30').fadeIn('slow');
    //    $('#fade1').fadeIn('slow');
    return true;
}
function addClass(ele,cls) {
    if (!this.hasClass(ele,cls)) ele.className += " "+cls;
}
function calculateAge(dateString) {
    var birthday = new Date(dateString);
    var datenow = new Date().valueOf();
    return ~~((datenow - birthday) / (31557600000));
}
function AlphaAndSpaces(event)
{
    var charCode = (event.which) ? event.which : event.keyCode;  
    if(/*DisableSpecialCharacters*/ (charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97) || (charCode > 122 && charCode < 128))      
        return false;
    var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
    // returns false if a numeric character has been entered
    return (chCode < 48 /* '0' */ || chCode > 57 /* '9' */ );
    return (chCode==32);

    return true;
}
function echeck(str)
{
    var at="@"
    var dot="."
    var lat=str.indexOf(at)
    var lstr=str.length
    var ldot=str.indexOf(dot)

    if (str.indexOf(at)==-1){
        return false;
    }

    if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
        return false;
    }

    if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
        return false;
    }

    if (str.indexOf(at,(lat+1))!=-1){
        return false;
    }

    if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
        return false;
    }

    if (str.indexOf(dot,(lat+2))==-1){
        return false;
    }

    if (str.indexOf(" ")!=-1){
        return false;
    }

    return true;
}