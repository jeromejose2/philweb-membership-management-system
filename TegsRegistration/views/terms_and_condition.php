<?php include("membershipcard.php");
if(!isset($_SESSION['language']))
{
    app::pr("<script>window.location.href='registration.php';</script>");
}
?>
<?php if($_SESSION['language'] == 'english'):?>
<!DOCTYPE html>
<html>
    
    <head>
        
        <title>TEGS Website Registration</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        
        <link href="css/style.css" rel="stylesheet" media="screen">
        
    </head>
    
    <body>
        
        <div class="container" id="page-container">
            
            <div class="row-fluid" id="header-container">
                
                <div class="span4"><img src="img/e_gameslogo.jpg"></div>
                
                <div class="span4 pull-right"></div>
                
            </div><!-- #header-container-->
            
            <div id="main-content">

                <div id="content-wrapper-full">

                    <h2 class="page-title">TERMS AND CONDITIONS</h2>

                    <div class="row-fluid full-content">
                        <br/>
                        <h6>MEMBERSHIP PROGRAM TERMS AND CONDITIONS</h6>
                        <br/>1.&nbsp;&nbsp;The Membership Program is open to all individuals 17 years of age and above and who are not otherwise disqualified.
                        <br/>2.&nbsp;&nbsp;The Ministry of Tourism Trade and Industry of Timor Leste (MTCI) requires that all members of e-Games must be registered as a member before they can enter and/or play in the e-Games casino.
                        <br/>3.&nbsp;&nbsp;The e-Games Membership Program requires the member to present a valid ID (Passport, Electoral ID, or Driver's License) and accomplish the e-Games Membership Application Form.
                        <br/>4.&nbsp;&nbsp;The Membership Card is non-transferable and may not be used by any person other than the card holder.
                        <br/>5.&nbsp;&nbsp;e-Games management has the right to request registered  members to present their membership card upon entry.
                        <br/>6.&nbsp;&nbsp;Members whose membership cards are lost, damaged or worn out will be allowed to register for another e-Games Membership Card. The damaged or worn out membership card must be surrendered to the Cashier prior to issuance of a new card.
                        <br/>7.&nbsp;&nbsp;The e-Games Membership Card is only valid for transactions within the e-Games casino. Any misuse of the e-Games Membership Card will result to permanent deactivation.
                        <br/>8.&nbsp;&nbsp;Members may revoke their membership at any time by sending a letter addressed to the e-Games Management. Resigned members are required to surrender their e-Games Membership Cards.
                        <br/>9.&nbsp;&nbsp;e-Games reserves the right to cancel or change the membership benefits, rules, or regulations without prior notice.
                    </div>
                </div><!-- .content-wrapper-fu -->

            </div><!-- #main-content -->
            
        </div><!-- #page-container -->
        
        <script>
            
            function tegspopup(url, title, w, h) {
                var left = (screen.width/2)-(w/2);
                var top = (screen.height/2)-(h/2);
                return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
            } 
            
        </script>
        
    </body>
    
</html>
<?php endif;?>


<?php if($_SESSION['language'] == 'bahasa'):?>
<!DOCTYPE html>
<html>
    
    <head>
        
        <title>TEGS Website Registration</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        
        <link href="css/style.css" rel="stylesheet" media="screen">
        
    </head>
    
    <body>
        
        <div class="container" id="page-container">
            
            <div class="row-fluid" id="header-container">
                
                <div class="span4"><img src="img/e_gameslogo.jpg"></div>
                
                <div class="span4 pull-right"></div>
                
            </div><!-- #header-container-->
            
            <div id="main-content">

                 <div id="content-wrapper-full">

                    <h2 class="page-title">TERMUS NO KONDISAUN-KONDISAUN</h2>

                    <div class="row-fluid full-content">
                        <br/>
                        <h6>TERMUS NO KONDISAUN-KONDISAUN PROGRAMA MEMBRU NIAN</h6>
                        <br/>1.&nbsp;&nbsp;programa membru nian sei loke ba ema hotu ne'ebe ho idade 17 ba leten.
                        <br/>2.&nbsp;&nbsp;Ministru Turizmu Komersiu no industria Timor Leste (MTCI) rekere membru e-Games sira hotu atu bele rejistu sai hanesan membru ida molok sira bele tama no/ou halimar iha kasino e-Games nia laran.
                        <br/>3.&nbsp;&nbsp;Programa membru e-Games nian rekere membru ne'e atu aprezenta ID ida ne'ebe validu (Pasaporte, ID Eleitoral, ou karta kondusaun), prenxe/finaliza formulariu aplikasaun membru e-Games nian, no depozitu maizumenus 25 dolares. Inísiu depozito bele utiliza atu halimar jogu ne'e wainhira membru ne'e hetan aprova tihaona.
                        <br/>4.&nbsp;&nbsp;Kartaun membru ne'e ema seluk sei labele halo transfer ou utiliza ekseptu kartaun nain.
                        <br/>5.&nbsp;&nbsp;Jestaun e-Games iha direitu atu husu membru ne'ebe rejistu tihaona atu aprezenta sira nia kartaun membru nian iha laran.
                        <br/>6.&nbsp;&nbsp;Kartaun membru sira nian ne'ebe lakon, a'at ou krehut (bosan) sei permiti atu rejistu fali kartaun membru e-Games seluk ou tenke entrega uluk ba kasir ho objetivu atu halo fali kartaun foun ida.
                        <br/>7.&nbsp;&nbsp;.Kartaun membru e-Games nian so validu de'it ba tranzasaun ho kasino e-Games. Wainhira utiliza kartaun e-Games la los  sei la ativu tan.
                        <br/>8.&nbsp;&nbsp;Membru sira bele foti fila fali sira nia kartaun membru nian bainhira de'it ho manda/haruka karta enderesu ba iha jestaun e-Games nian. Membru sira ne'ebe la servisu ona sei rekere atu hatama fali sira nian kartaun membru e-Games nian.
                        <br/>9.&nbsp;&nbsp;Serbidor e-Games iha direito atu kansela ou troka benefisiu membru ou regulamentu sira maske la fo avizu uluk.</div>
                </div><!-- .content-wrapper-fu -->

            </div><!-- #main-content -->
            
        </div><!-- #page-container -->
        
        <script>
            
            function tegspopup(url, title, w, h) {
                var left = (screen.width/2)-(w/2);
                var top = (screen.height/2)-(h/2);
                return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
            } 
            
        </script>
        
    </body>
    
</html>
<?php endif;?>