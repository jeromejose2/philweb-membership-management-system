<?php
/**
 * This file will generate the Membership Card No. and to be included in several pages.
 * 
 * @author Noel Antonio 04-18-2013 <ndantonio@philweb.com.ph>
 * @copyright c 2013, PhilWeb Corporation
 */

require_once '../init.inc.php';

App::LoadModuleClass("MembershipTimor", "MTMemberInfo");
$members = new MTMemberInfo();

$array = $members->SelectLastMemberID();
$id = ($array[0][0] + 1);
?>