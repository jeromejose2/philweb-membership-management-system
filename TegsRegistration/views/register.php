<?php
/**
 * @author Jerome Jose May 7 ,2013 for OnlineMembership
 */
error_reporting(E_ALL);
ini_set('display_errors',1);
require_once("membershipcard.php");
require_once("tc_calendar.php");
include("../controller/registerprocess.php");
$_SESSION['language'] = 'english';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>  

    <head>

        <title>Timor e-Games</title>

        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />  
        <link href="css/style.css" rel="stylesheet" media="screen" type="text/css">
        <script language="javascript" src="js/jquery-1.5.2.min.js"></script>
        <script src="js/checkinputs.js"></script>
        <script language="javascript" src="js/calendar.js"></script>
        <link href="http://www.egames.com.tl/wp-content/themes/tegs_v2/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="http://www.egames.com.tl/wp-content/themes/tegs_v2/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="http://www.egames.com.tl/wp-content/themes/tegs_v2/css/style.css" rel="stylesheet" media="screen">
        <link href="http://www.egames.com.tl/wp-content/themes/tegs_v2/style.css" rel="stylesheet" media="screen">
        <script type="text/javascript">
            function tegspopup(url, title, w, h) {
                var left = (screen.width/2)-(w/2);
                var top = (screen.height/2)-(h/2);
            
                if($.browser.msie)
                {    
                    return window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
                }else
                    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
            } 
            $(document).ready(function(){
                var $body = $($.browser.msie ? document.body : document);
                    
                if($.browser.msie)
                { 
                    $('#fade').css({
                        height:  $body.height() 
                    });
                    $('#fade1').css({
                        height:  $body.height()
                    });
                    $('#light1').css({
                        top: ($body.height()/2.5)
                    });
                    $('#successful').css({
                        left:  190,
                        top: ($body.height()/2.5)
                    });
                        
                }else
                {
                    $('#fade').css({
                        width:  $body.width(),
                        height:  $body.height() 
                    });
                    $('#fade1').css({
                        width:  $body.width(),
                        height:  $body.height()
                    });
                    $('#light1').css({
                        top: ($body.height()/2.5)
                    });
                    $('#successful').css({
                        top: ($body.height()/2.5)
                    });
                            
                }
                   
            });
        </script> 
    </head>

    <body>
        <?php 
                    echo $hidBday;
                ?>
        <div id="fade" class="black_overlay"></div>
        <div id="fade1" class="black_overlay2"></div>
        <!--for error message-->
        <div id="light1" style="text-align: center;font-size: 16pt;height: auto;background-color: #e6e6e6;" class="white_content">
            <div style="" class="error-title">
                <div>
                    <b id="errorHead"  style=""></b>
                </div>
            </div>
            <br/>
            <p id="errorMsg"><br/></p>
            <br/>
            <p id="button">
                    <input type="button" class="tegs-submit-btn" value="OKAY" onclick ="$('#light1').fadeOut('slow');document.getElementById('fade').style.display='none';"/>
            </p>
        </div>
        <!--for error message-->

        <!--successfull message-->
        <div id="successful" class="" style="background-color: white;">
            <div style="" class="error-title">
                <b id="errorHead">CONGRATULATIONS</b>
            </div>
            <br/><br/>
            <div style="font-style:normal;font-weight: normal;">
                You have successfully sent your registration for the e-Games membership.
                <br/>Present your valid ID at e-Games and enjoy the e-Games experience! 
                <br/><br/>You will now be redirected to the featured games page for continued entertainment.
                <br/><br/>
            </div>
            <button name="subject" id="btnokay" type="button" value="OKAY" class="tegs-submit-btn" onclick="window.location.href = 'register.php';">OKAY</button>
        </div>
        <!--successfull message-->
        <div class="container" id="page-container">

            <div class="row-fluid" id="header-container">

                <div class="span4"><a href="/"><img src="http://www.egames.com.tl/wp-content/themes/tegs_v2/img/e_gameslogo.jpg"></a></div>

                <div class="span5 pull-right">

                    <div class="row-fluid" id="upper-right">

                        <div class="span8"><form method="get" id="searchform" action="http://www.egames.com.tl/">

                                <input type="text" name="s" id="search-box-upper"  placeholder="Search">

                                <input type="submit" name="submit" id="search-button-upper" value="SEARCH">

                            </form></div>

                        <div class="span4" id="language-flag-selection">
                            <a href="#"><img src="http://www.egames.com.tl/wp-content/themes/tegs_v2/img/uk_flag.jpg"></a>
                            <a href="#"><img src="http://www.egames.com.tl/wp-content/themes/tegs_v2/img/timor_flag.jpg"></a>
                            <a href="#"><img src="http://www.egames.com.tl/wp-content/themes/tegs_v2/img/indonesia_flag.jpg"></a>
                        </div>

                    </div>

                </div>

            </div><!-- #header-container-->

            <div id="nav-container">

                <div class="row-fluid">

                    <div class="span10">
                        <div class="menu-main-menu-container">
                            <ul id="main-navigation" class="menu">
                                <li id="menu-item-42" class="menu-item menu-item-type-post_type menu-item-object-page page_item page-item-2 current_page_item menu-item-42"><a href="http://www.egames.com.tl/">e-Games</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-43" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-43"><a href="http://www.egames.com.tl/e-games/about-e-games/">About e-Games</a></li>
                                        <li id="menu-item-161" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-161"><a href="http://www.egames.com.tl/e-games/gallery/">Gallery</a></li>
                                    </ul>
                                </li>

                                <li id="menu-item-46" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-46 current-menu-item"><a href="http://www.egames.com.tl/membership/">Membership</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-47" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47"><a href="http://www.egames.com.tl/membership/registration/">Registration</a></li>
                                        <li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48"><a href="http://www.egames.com.tl/membership/house-rules/">House Rules</a></li>
                                        <li id="menu-item-49" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49"><a href="http://www.egames.com.tl/membership/terms-and-conditions/">Terms and Conditions</a></li>
                                        <li id="menu-item-287" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-287"><a href="http://www.egames.com.tl/membership/faqs/">FAQs</a></li>
                                    </ul>
                                </li>

                                <li id="menu-item-50" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50"><a href="http://www.egames.com.tl/featured-games/">Featured Games</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-568" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-568"><a href="http://www.egames.com.tl/category/featured-games/slots/">Slots Games</a></li>
                                        <li id="menu-item-570" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-570"><a href="http://www.egames.com.tl/category/featured-games/table-games/">Table Games</a></li>
                                        <li id="menu-item-571" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-571"><a href="http://www.egames.com.tl/category/featured-games/specialty-games/">Specialty Games</a></li>
                                        <li id="menu-item-52" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52"><a href="http://www.egames.com.tl/featured-games/play-for-free/">Play for Free</a></li>
                                    </ul>
                                </li>

                                <li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53"><a href="http://www.egames.com.tl/events-and-promotions/">Events and Promotions</a>
                                    <ul class="sub-menu">
                                        <li id="menu-item-54" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54"><a href="http://www.egames.com.tl/events-and-promotions/promotions/">Promotions</a></li>
                                        <li id="menu-item-55" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55"><a href="http://www.egames.com.tl/events-and-promotions/news/">News</a></li>
                                    </ul>
                                </li>

                                <li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56"><a href="http://www.egames.com.tl/responsible-gaming/">Responsible Gaming</a></li>
                                <li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57"><a href="http://www.egames.com.tl/contact-us/">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="span2"><a id="fb-follow" href="https://www.facebook.com/pages/egamescasinotl"><img src="http://www.egames.com.tl/wp-content/themes/tegs_v2/img/like_us.png"></a></div>

                </div>


            </div><!-- #nav-container -->

            <div id="header-image-container">

                <img width="979" height="357" src="http://www.egames.com.tl/wp-content/uploads/2013/05/banner-home-page.jpg" class="attachment-full" alt="banner home page" />                
            </div>

            <div id="main-content">

                <div id="content-wrapper-full">

                    <h2 class="page-title">Membership | Registration</h2>

                    <div class="row-fluid full-content">
                        <form id="frmRegister" name="frmRegister" class="registration-form" method="POST" action="register.php"  enctype="multipart/form-data">

                            <div class="span8">
                                <div class="row-fluid input-wrapper">
                                    <div class="span3 input-label"><div id="lblfn">* First Name</div></div>
                                    <div class="span8 input-form"><?php echo $txtfn; ?></div>
                                </div><!-- .input-wrapper -->

                                <div class="row-fluid input-wrapper">
                                    <div class="span3 input-label"><div id="lblln">* Last Name</div></div>
                                    <div class="span8 input-form"><?php echo $txtln; ?></div>
                                </div><!-- .input-wrapper -->

                                <div class="row-fluid input-wrapper">
                                    <div class="span3 input-label"><div id="lblbday">* Birthdate</div></div>
                                    <div class="span8 input-form">

                                        <div class="row-fluid">
                                            <?php
                                            $myCalendar->writeScript();
                                            ?>
                                        </div>

                                    </div>


                                </div><!-- .input-wrapper -->

                                <div class="row-fluid input-wrapper">
                                    <div class="span3 input-label"><div id="lblsex">* Gender</div></div>
                                    <div class="span8 input-form">                                                    
                                        <?php echo $ddlsex; ?>
                                    </div>
                                </div><!-- .input-wrapper -->                                   


                                <div class="row-fluid input-wrapper">
                                    <div class="span3 input-label">&nbsp; Nationality</div>
                                    <div class="span8 input-form">                                                    
                                        <?php echo $ddlnationality; ?>
                                    </div>
                                </div><!-- .input-wrapper -->                                    

                                <div class="row-fluid input-wrapper">
                                    <div class="span3 input-label">&nbsp; Marital Status</div>
                                    <div class="span8 input-form">                                                    
                                        <?php echo $ddlstatus; ?>
                                    </div>
                                </div><!-- .input-wrapper -->                                   

                                <div class="row-fluid input-wrapper">
                                    <div class="span3 input-label">&nbsp; Address</div>
                                    <div class="span8 input-form"><?php echo $txtaddr; ?></div>
                                </div><!-- .input-wrapper -->                                   

                                <div class="row-fluid input-wrapper">
                                    <div class="span3 input-label">&nbsp; Contact Number</div>
                                    <div class="span8 input-form"><?php echo $txtcn; ?></div>
                                </div><!-- .input-wrapper -->                                   

                                <div class="row-fluid input-wrapper">
                                    <div class="span3 input-label"><div id="lblemail">&nbsp; Email Address</div></div>
                                    <div class="span8 input-form"><?php echo $txtemail; ?></div>
                                </div><!-- .input-wrapper -->                                  

                                <div class="row-fluid input-wrapper">
                                    <div class="span3 input-label"></div>
                                    <div class="span8">
                                        <input type="checkbox" id="terms" name="terms"> I agree to the 
                                        <a href="javascript:void(0);" onclick="tegspopup('house_rules.php', 'House Rules', 500, 500);">House Rules</a>
                                        &
                                        <a href="javascript:void(0);" onclick="tegspopup('terms_and_condition.php', 'Terms and Condition', 500, 500);">Terms and Condition</a>
                                    </div>
                                </div><!-- .input-wrapper -->                                   

                                <br>

                                <div class="row-fluid input-wrapper">
                                    <div class="span6 input-label">&nbsp; (*) Required Fields</div>
                                    <div class="span6 input-form">
                                        <?php echo $btnSubmit; ?>
                                        <!--<input type="submit" id="btnSubmit" name="btnSubmit" class="tegs-submit-btn" value="Submit" onclick="javascript: return checkregistrationinputs();"> &nbsp;-->
                                        <!--<input type="submit" class="tegs-submit-btn" value="Back">-->
                                    </div>
                                </div><!-- .input-wrapper -->                                   



                            </div>

                            <div class="span4" style="background-color:#ede3c8; padding: 10px;" id="registration-sidebar">

                                <strong>Upload Photo</strong>
    <!--<input type="file">-->
                                <div id="tdbrowse"><input type="file" name="filebrowse" id="filebrowse"/></div>

                                <h2 style="font-size:14px; margin:10px 0; text-align:center; font-weight: bold;line-height: 18px;" class="registration-sidebar-header">HOW TO REGISTER</h2>

                                <section style="text-align:justify;">
                                    <ol>
                                        <li>Register by accomplishing the Membership Application Form. The forms are available on e-Games casino, through flyers during events, and online through the form provided on this page.</li>
                                        <li>Upon accomplishing the form, present one (1) valid ID (e.g. Passport, Driver's License, or Electoral ID) at the e-Games casino to finalize the processing of your application. </li>
                                        <!--<li>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</li>-->
                                    </ol>
                                </section>
                            </div>
                        </form>
                    </div>

                </div><!-- .content-wrapper-fu -->

            </div><!-- #main-content -->

            <div id="footer-container">

                <div class="menu-footer-menu-container">
                    <ul id="footer-navigation" class="menu">
                        <li id="menu-item-58" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2 current_page_item menu-item-58"><a href="http://www.egames.com.tl/">e-Games</a></li>
                        <li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59"><a href="http://www.egames.com.tl/membership/">Membership</a></li>
                        <li id="menu-item-60" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-60"><a href="http://www.egames.com.tl/featured-games/">Featured Games</a></li>+
                        <li id="menu-item-61" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-61"><a href="http://www.egames.com.tl/events-and-promotions/">Events and Promotions</a></li>
                        <li id="menu-item-62" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-62"><a href="http://www.egames.com.tl/responsible-gaming/">Responsible Gaming</a></li>
                        <li id="menu-item-63" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-63"><a href="http://www.egames.com.tl/contact-us/">Contact Us</a></li>
                    </ul>
                </div>                

                <div id="footer-logos">
                    <img src="http://www.egames.com.tl/wp-content/themes/tegs_v2/img/no_under17.png">
                    <img src="http://www.egames.com.tl/wp-content/themes/tegs_v2/img/vip_card.png">
                    <img src="http://www.egames.com.tl/wp-content/themes/tegs_v2/img/mtci.png">
                </div>

            </div><!-- #footer-container -->

        </div><!-- #page-container -->

    <link rel='stylesheet' id='tegs-fg-css-css'  href='http://www.egames.com.tl/wp-content/plugins/tegs-featured-games/css/tegs-gf.css?ver=3.5.1' type='text/css' media='all' />
    <script type='text/javascript' src='http://www.egames.com.tl/wp-includes/js/jquery/jquery.js?ver=1.8.3'></script>
    <script type='text/javascript' src='http://www.egames.com.tl/wp-content/plugins/tegs-featured-games/js/tegs-fg.js?ver=3.5.1'></script>
    <script type='text/javascript' src='http://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js?ver=3.5.1'></script>
    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js?ver=3.5.1'></script>
    <script type='text/javascript' src='http://www.egames.com.tl/wp-content/plugins/visual-form-builder/js/visual-form-builder-validate.js?ver=3.5.1'></script>
    <script type='text/javascript' src='http://www.egames.com.tl/wp-content/plugins/visual-form-builder/js/js_quicktags.js?ver=3.5.1'></script>


    <?php if ( isset($houserules) && $houserules === true ) : ?>
        <script>
            $('#successful').fadeIn('slow');
            document.getElementById('fade').style.display='block';
        </script>  
        <?php
    endif;
    unset($houserules);
    ?>

    <?php if ( isset($errorMsg) ) : ?>
        <script>
            document.getElementById('errorHead').innerHTML = "NOTIFICATION";
            document.getElementById('errorMsg').innerHTML = "<?php echo $errorMsg; ?>";
            $('#light1').fadeIn('slow');
            document.getElementById('fade').style.display='block';
        </script>  
        <?php
    endif;
    unset($errorMsg);
    ?>

</body>

</html>